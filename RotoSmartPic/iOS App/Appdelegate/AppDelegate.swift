//
//  AppDelegate.swift
//  RotoSmartPic
//
//  Created by Mac on 13/05/1942 Saka.
//  Copyright © 1942 Bluepixel Technologies. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase

var connectedLEDDevice: BLEDevice?
var connectedBLEDevice: BLEDevice?
var albumManager: AlbumManager?

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        IQKeyboardManager.shared.enable = true
        setDefaultSettings()
        setRootVC(window: window)
        logfile.create(fileName: logfile.logFileName, text: "============== App is Lanunch ============== ")
    
        isschedulestart = false
        
        sleep(2)
    
        return true
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
        if connectedBLEDevice?.isConnected() ?? false {
            
            connectedBLEDevice?.disConnect(myComplition: { (isDisconnect) in
                if isDisconnect {
                    connectedBLEDevice = nil
                }
            })
        }
        
        if connectedLEDDevice?.isConnected() ?? false {
            
            connectedLEDDevice?.disConnect(myComplition: { (isDisconnect) in
                if isDisconnect {
                    connectedLEDDevice = nil
                }
            })
        }
        
        var arrOfGroup = [Group]()

        if let groups = (userdef.value(forKey: USERDEFAULT.kGroups) as? [Any]) {
            for groupdict in groups {
                arrOfGroup.append(Group(dict: groupdict))
            }
            
            var temparrOfGroup = [Group]()
            for item in arrOfGroup{
                item.arrOfIdividualValues = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                temparrOfGroup.append(item)
            }
            let setting = temparrOfGroup.map({$0.toDict()})
            userdef.set(setting, forKey: USERDEFAULT.kGroups)
            userdef.synchronize()
        }
    }
    
    //--------------------------------------------------------
    //                  MARK: - Function -
    //--------------------------------------------------------
    private func setDefaultSettings() {
        
        if userdef.value(forKey: USERDEFAULT.kMicroStepping) == nil {
            userdef.set(3200, forKey: USERDEFAULT.kMicroStepping)
        }
        
        if userdef.value(forKey: USERDEFAULT.kGearRatio) == nil {
            userdef.set(6.0, forKey: USERDEFAULT.kGearRatio)
        }
        makeFoldersInDocumentDirectory()
    }
    //Create folders for Photo and Video in document directory
    private func makeFoldersInDocumentDirectory() {
        
        
        //Make folders in Application level document directory
        storeManager.makeFolder(directoryName: Constant.kRotoPic, folderName: CaptureType.Photo.rawValue, subfolder: AngleTypes.P45.rawValue)
        storeManager.makeFolder(directoryName: Constant.kRotoPic, folderName: CaptureType.Photo.rawValue, subfolder: AngleTypes.P90.rawValue)
        storeManager.makeFolder(directoryName: Constant.kRotoPic, folderName: CaptureType.Photo.rawValue, subfolder: AngleTypes.P180.rawValue)
        storeManager.makeFolder(directoryName: Constant.kRotoPic, folderName: CaptureType.Photo.rawValue, subfolder: AngleTypes.P360.rawValue)
        storeManager.makeFolder(directoryName: Constant.kRotoPic, folderName: CaptureType.Photo.rawValue, subfolder: AngleTypes.Single.rawValue)
        
        storeManager.makeFolder(directoryName: Constant.kRotoPic, folderName: CaptureType.Video.rawValue, subfolder: AngleTypes.P45.rawValue)
        storeManager.makeFolder(directoryName: Constant.kRotoPic, folderName: CaptureType.Video.rawValue, subfolder: AngleTypes.P90.rawValue)
        storeManager.makeFolder(directoryName: Constant.kRotoPic, folderName: CaptureType.Video.rawValue, subfolder: AngleTypes.P180.rawValue)
        storeManager.makeFolder(directoryName: Constant.kRotoPic, folderName: CaptureType.Video.rawValue, subfolder: AngleTypes.P360.rawValue)
        
        makeFoldersInPhotos()
    }
    
    //Create folders for Photo and Video in default Photos application of iPhone
    private func makeFoldersInPhotos() {
        
        albumManager = AlbumManager.instance
        
        albumManager?.checkAuthorization(complition: { (isAutorized) in
            
            if isAutorized {
                
                print("Album permission is authorized")
                
                albumManager?.createFolderWithName(Constant.kPhoto_Single)
                albumManager?.createFolderWithName(Constant.kPhoto_45)
                albumManager?.createFolderWithName(Constant.kPhoto_90)
                albumManager?.createFolderWithName(Constant.kPhoto_180)
                albumManager?.createFolderWithName(Constant.kPhoto_360)
                
                albumManager?.createFolderWithName(Constant.kVideo_45)
                albumManager?.createFolderWithName(Constant.kVideo_90)
                albumManager?.createFolderWithName(Constant.kVideo_180)
                albumManager?.createFolderWithName(Constant.kVideo_360)
                
            } else {
                
                print("Album permission is not authorized")
            }
        })
    }
}
func setRootVC(window: UIWindow?) {
    
    if userdef.bool(forKey: Constant.kIsCompletedIntroduction) {
        
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navigationController = storyboard.instantiateViewController(withIdentifier: "nav") as! UINavigationController
        
        let capturePhotoVideoVC = storyboard.instantiateViewController(withIdentifier: "CapturePhotoVideoVC") as! CapturePhotoVideoVC
        navigationController.pushViewController(capturePhotoVideoVC, animated: false)
        
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
}

