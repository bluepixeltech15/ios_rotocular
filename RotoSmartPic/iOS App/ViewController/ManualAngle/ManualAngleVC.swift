//
//  ManualAngleVC.swift
//  RotoSmartPic
//
//  Created by BAPS on 26/11/20.
//  Copyright © 2020 Bluepixel Technologies. All rights reserved.
//

import UIKit

class ManualAngleVC: UIViewController {

    //MARK:-  Outlets And Variable Declarations
    @IBOutlet weak var txtAngel: UITextField!
    
    typealias okComplitionBlock = (Bool, Int?) -> Void
    private var okCompletion: okComplitionBlock!

    //MARK:- 
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK:-  Buttons Clicked Actions
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.okCompletion(false, nil)
        }
    }
    @IBAction func btnOkClicked(_ sender: UIButton) {
        
        if txtAngel.text!.trime().isEmpty {
            self.showAlertWithOKButton(message: "Please enter a angle.")
        } else {
            
            self.dismiss(animated: true) {
                self.okCompletion(true, Int(self.txtAngel.text!.trime()) ?? 0)
            }
        }
    }
    
    //MARK:-  Functions
    func isOkClicked(mycompletion: @escaping okComplitionBlock) {
        self.okCompletion = mycompletion
    }
}

//MARK:-  UITextFieldDelegate Methods
extension ManualAngleVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            
            let updatedText = text.replacingCharacters(in: textRange, with: string)
        
            let value = Int(updatedText) ?? 0
            if value > 360 {
                return false
            }
        }
        return true
    }
}
