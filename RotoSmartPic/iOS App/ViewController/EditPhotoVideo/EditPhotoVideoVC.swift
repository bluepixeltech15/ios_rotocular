//
//  EditPhotoVideoVC.swift
//  RotoSmartPic
//
//  Created by Mac on 14/05/1942 Saka.
//  Copyright © 1942 Bluepixel Technologies. All rights reserved.
//

import UIKit
import Mantis
import MobileCoreServices
import AVKit
import AVFoundation

var Selectedimgae: image?

class EditPhotoVideoVC: UIViewController {
    
    //--------------------------------------------------------
    //                  MARK: - Outlet -
    //--------------------------------------------------------
    @IBOutlet weak var btnConnectDisconnect : UIButton!
    @IBOutlet weak var btnConnectDisconnectTitle : UIButton!
    @IBOutlet weak var btnTurnTable : UIButton!
    @IBOutlet weak var btnSelectedPhotoVide : UIButton!

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet var btnAdjustSetting: [UIButton]!
    @IBOutlet weak var sliderRotate: UISlider!
    @IBOutlet weak var sliderBrightness: UISlider!
    @IBOutlet weak var sliderCotrast: UISlider!
    @IBOutlet weak var sliderPurity: UISlider!
    
    @IBOutlet var btnOptions: [UIButton]!
    
    @IBOutlet weak var viewMainEditing: UIView!
    @IBOutlet weak var viewImgEdit: UIView!
    
    @IBOutlet weak var viewSettings: UIView!
    @IBOutlet weak var viewVideoEdit: UIView!
    @IBOutlet weak var imgVideo: UIImageView!
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var editView: UIView!
    
    @IBOutlet  var btnSingleCollection : [UIButton]!
    
    @IBOutlet weak var viewColorPicker: UIView!
    
    @IBOutlet weak var viewAdjustmentSettings: UIView!
    @IBOutlet weak var btnSaveSettings: UIButton!
    
    //--------------------------------------------------------
    //                  MARK: -Property -
    //--------------------------------------------------------
    private var selectedTextfield = UITextField()
    private var audioVideoMergeURL: URL?
    
    private var filter: CIFilter?
    private var filteredImage: CIImage?
    
    private var isCroped = false
    
    
    private struct Edited {
        
        enum EditingType {
        
            case CropChange
            case RotateChange
            case IconAdded
            case TextAdded
            case BrightNessChange
            case ContrastChange
            case PurityChange
        }
    
        let lastEditing: EditingType
        let lastImage: UIImage
        var sliderValue: Float?
    }
    
    private var arrOfEdited = [Edited]()
    
    
    private enum CurrentAdjustment: Int {
        case one = 1, two, three, custom, None
    }

    private var adjustment: CurrentAdjustment = .None
    
    //--------------------------------------------------------
    //                  MARK: - View Life Cycle -
    //--------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Selectedimgae = nil
        scrollView.maximumZoomScale = 4
        scrollView.minimumZoomScale = 1
        
//        addGestureFor(zoomView: self.viewImgEdit)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !isCroped {
            
            setupPhotoVideoUI()
            self.setConnectDisConnect()
            
        } else {
            isCroped = false
        }
    }
    
    //--------------------------------------------------------
    //                  MARK: -  Button Action -
    //--------------------------------------------------------
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAdjustSettingClicked(_ sender: UIButton) {
        
        for btn in btnAdjustSetting {
            
            if sender.tag == btn.tag {
                btn.isSelected = true
                btn.backgroundColor = #colorLiteral(red: 0, green: 0.6524586678, blue: 0.6893625855, alpha: 1)
                
            } else {
                
                btn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                btn.isSelected = false
            }
        }
        
        adjustment = CurrentAdjustment(rawValue: sender.tag) ?? .one
        setAdjustmentsSliders(adj: adjustment)
        
    }
    @IBAction func btnSetting(_ sender:UIButton) {
        let settingsVC = storyboard?.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        self.navigationController?.pushViewController(settingsVC, animated: true)
    }
    
    @IBAction func btnLogoClicked(_ sender: UIButton) {
        
        self.arrOfEdited.append(Edited(lastEditing: .IconAdded, lastImage: UIImage(view: viewImgEdit)))
        if !sender.isSelected {
            return
        }
        if imgView.image == nil {
            return
        }
        self.openImagePiker(mediaTypes: [(kUTTypePNG as String)])
    }
    
    @IBAction func btnImportClicked(_ sender: UIButton) {
        
        self.arrOfEdited.append(Edited(lastEditing: .IconAdded, lastImage: UIImage(view: viewImgEdit)))
        
        if !sender.isSelected {
            return
        }
        
        if imgView.image == nil {
            return
        }
        self.openImagePiker(mediaTypes: [(kUTTypeJPEG as String), (kUTTypeJPEG2000 as String), (kUTTypeBMP as String)])
    }
    
    @IBAction func btnOptionClicked(_ sender: UIButton) {
        
        if imgView.image == nil {
            return
        }
        
        for btn in btnOptions {
            if sender.tag == btn.tag {
                btn.isSelected = true
            } else {
                btn.isSelected = false
            }
        }
        if sender.tag == 0 {
            sliderRotate.isHidden = true
            imgCropped(image: UIImage(view: viewMainEditing))
        } else {
            sliderRotate.isHidden = false
        }
    }
    
    @IBAction func btnConnectDisConnectClicked(_ sender: UIButton) {
        
        if sender.isSelected {
            connectedBLEDevice?.disConnect(myComplition: { (isDisconnect) in
                if isDisconnect {
                    connectedBLEDevice = nil
                    self.setConnectDisConnect()
                }
            })
        } else {
            let deviceScanVC = storyboard?.instantiateViewController(withIdentifier: "DeviceScanVC") as! DeviceScanVC
            self.navigationController?.pushViewController(deviceScanVC, animated: true)
        }
    }
    
    @IBAction func sliderRotateTouchDown(_ sender: UISlider) {
        arrOfEdited.append(Edited(lastEditing: .RotateChange, lastImage: UIImage(view: viewImgEdit), sliderValue: sender.value))
    }
    @IBAction func sliderRotateValueChanged(_ sender: UISlider) {
        
        scrollView.transform = CGAffineTransform(rotationAngle: CGFloat(Double(sender.value) * 2 * Double.pi / Double(sender.maximumValue)))
    }
    @IBAction func sliderRotateTouchUpInside(_ sender: UISlider) {
        arrOfEdited.append(Edited(lastEditing: .RotateChange, lastImage: UIImage(view: viewImgEdit), sliderValue: sender.value))
    }
    
    
    @IBAction func sliderBrTouchDown(_ sender: UISlider) {
        arrOfEdited.append(Edited(lastEditing: .BrightNessChange, lastImage: UIImage(view: viewImgEdit), sliderValue: sender.value))
    }
    @IBAction func sliderBrValueChanged(_ sender: UISlider) {
        adjustmentBrightness(byVal: sliderBrightness.value)
        self.saveAdjustmentsSettings(slider: sender)
    }
    @IBAction func sliderBrTouchUpInside(_ sender: UISlider) {
        arrOfEdited.append(Edited(lastEditing: .BrightNessChange, lastImage: UIImage(view: viewImgEdit), sliderValue: sender.value))
    }
    
    
    @IBAction func sliderCtrTouchDown(_ sender: UISlider) {
        arrOfEdited.append(Edited(lastEditing: .ContrastChange, lastImage: UIImage(view: viewImgEdit), sliderValue: sender.value))
    }
    @IBAction func sliderCtrValueChanged(_ sender: UISlider) {
        adjustmentContrast(byVal: sliderCotrast.value)
        self.saveAdjustmentsSettings(slider: sender)
    }
    @IBAction func sliderCtrTouchUpInside(_ sender: UISlider) {
        arrOfEdited.append(Edited(lastEditing: .ContrastChange, lastImage: UIImage(view: viewImgEdit), sliderValue: sender.value))
    }
    
    
    @IBAction func sliderPurityTouchDown(_ sender: UISlider) {
        arrOfEdited.append(Edited(lastEditing: .PurityChange, lastImage: UIImage(view: viewImgEdit), sliderValue: sender.value))
    }
    @IBAction func sliderPurityValueChanged(_ sender: UISlider) {
        adjustmentSaturation(byVal: sliderPurity.value)
        self.saveAdjustmentsSettings(slider: sender)
    }
    @IBAction func sliderPurityTouchUpInside(_ sender: UISlider) {
        arrOfEdited.append(Edited(lastEditing: .PurityChange, lastImage: UIImage(view: viewImgEdit), sliderValue: sender.value))
    }
    
    
    @IBAction func btnTextClicked(_ sender: UIButton) {
        
        self.arrOfEdited.append(Edited(lastEditing: .TextAdded, lastImage: UIImage(view: viewImgEdit)))
        if imgView.image == nil || !sender.isSelected {
            return
        }
        
        let textField = UITextField()
        textField.backgroundColor = UIColor.darkGray.withAlphaComponent(0.8)
        textField.placeholder = "Type here"
        textField.textAlignment = .center
        textField.frame.size = CGSize(width: 100, height: 50)
        textField.center = viewImgEdit.center
        textField.isUserInteractionEnabled = true
        textField.delegate = self
        textField.returnKeyType = .done
        textField.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        textField.textColor = .white
        viewImgEdit.addSubview(textField)
        
        setColorPickerOnKeyboard(textfield: textField)
        selectedTextfield = textField
    }
    @IBAction func btnPlayVideoClicked(_ sender:UIButton) {
        
        if let url = self.audioVideoMergeURL {
            
            self.playVideo(url: url)
            return
        }
    
        if let s = Selectedimgae {
            self.playVideo(url: s.url!)
        }
    }
    @IBAction func btnSaveSetting(_ sender:UIButton) {
        
        btnSaveSettings.isHidden = true
        viewAdjustmentSettings.isHidden = true
    }
    @IBAction func btnImageSettingClicked(_ sender:UIButton) {
        
        viewAdjustmentSettings.isHidden = !viewAdjustmentSettings.isHidden
        btnSaveSettings.isHidden = viewAdjustmentSettings.isHidden
    }
    
    @IBAction func btnAudioClicked(_ sender: UIButton) {
        
        if !sender.isSelected {
            return
        }
        
        let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypeMP3), String(kUTTypeWaveformAudio), String(kUTTypeAudio)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
    
    @IBAction func btnSaveClicked(_ sender: UIButton) {
        
        if let s = Selectedimgae {
            
            let folderName = getFolderNameOfPhotos(folder: s.folder, subFolder: s.subfolder)
            
            if s.folder == .some(.Video), let saveURL = audioVideoMergeURL {
                
                albumManager?.saveVideoIn(albumName: folderName, videoURL: saveURL)
                self.showAlertWithOKButton(message: "Video saved Successfully")
                
            } else if s.folder == .some(.Photo) {
                
                albumManager?.saveImageIn(albumName: folderName, image: UIImage(view: viewMainEditing))
                self.showAlertWithOKButton(message: "Image saved Successfully")
            }
        }
    }
    
    @IBAction func btnGalleryClicked(_ sender: UIButton) {
        
        let folderVC = storyboard?.instantiateViewController(withIdentifier: "FolderVC") as! FolderVC
        folderVC.gallaryOpenType = .Gallery
        self.navigationController?.pushViewController(folderVC, animated: true)
    }
    
    @IBAction func btnSelectImageClicked(_ sender: UIButton) {
        
        let folderVC = storyboard?.instantiateViewController(withIdentifier: "FolderVC") as! FolderVC
        folderVC.gallaryOpenType = .Selection
        self.navigationController?.pushViewController(folderVC, animated: true)
    }
    @IBAction func btnUndoClicked(_ sender: UIButton) {
        
        for subview in viewImgEdit.subviews {
            if subview != imgView && subview != btnSelectedPhotoVide {
                subview.removeFromSuperview()
            }
        }
        
        if let editing = arrOfEdited.last {
            
            imgView.image = editing.lastImage
            switch editing.lastEditing {
            case .CropChange, .TextAdded, .IconAdded:
            
                break
                
            case .RotateChange:
                
                sliderRotate.value = editing.sliderValue ?? sliderRotate.value
                
                
                if arrOfEdited.count > 2, let previousLast = arrOfEdited.suffix(2).first {
                    if previousLast.lastEditing != .RotateChange {
                        sliderRotate.value = 0.0
                    }
                } else {
            
                    sliderRotate.value = 0.0
                }
                
                scrollView.transform = CGAffineTransform(rotationAngle: CGFloat(Double(sliderRotate.value) * 2 * Double.pi / Double(sliderRotate.maximumValue)))
                
                
            case .BrightNessChange:
                sliderBrightness.value = editing.sliderValue ?? sliderBrightness.value
                adjustmentBrightness(byVal: editing.sliderValue!)
        
            case .ContrastChange:
                sliderCotrast.value = editing.sliderValue ?? sliderCotrast.value
               
            case .PurityChange:
                sliderPurity.value = editing.sliderValue ?? sliderPurity.value
                
            break
                
            default:
                break
            }
            arrOfEdited.removeLast()
        }
    }
    
    
    //--------------------------------------------------------
    //                  MARK: - Function -
    //--------------------------------------------------------
    private func setupPhotoVideoUI() {
        
        if let sImg = Selectedimgae {
        
            if sImg.folder == .some(.Photo) {
                
                if btnSelectedPhotoVide != nil {
                    btnSelectedPhotoVide?.isHidden = true
                }
                
                if self.imgVideo.image != sImg.img { // its Selected Another Image so we need to clear filters
                    
                    sliderRotate.value = 0
                    scrollView.transform = CGAffineTransform(rotationAngle: CGFloat(Double(sliderRotate.value) * 2 * Double.pi / Double(sliderRotate.maximumValue)))
                }
                
                imgView.image = sImg.img
                
                viewSettings.isHidden = false
                viewVideoEdit.isHidden = true
                
                for btn in btnSingleCollection {
                    btn.isHidden = false
                    if btn.tag == 3 { // means its audio button
                        btn.isSelected = false
                    } else {
                        btn.isSelected = true
                    }
                }
                
            } else {
                
                viewSettings.isHidden = true
                viewVideoEdit.isHidden = false
                
                if self.imgVideo.image != sImg.img { // its Selected Another Image so we need to clear filters
                    self.audioVideoMergeURL = nil
                }
                
                self.imgVideo.image = sImg.img
                
                for btn in btnSingleCollection {
                    
                    if btn.tag == 3 { // means its audio button
                        
                        btn.isHidden = false
                        btn.isSelected = true
                        
                    } else {
                        btn.isHidden = true
                    }
                }
            }
        }
    }
    private func setAdjustmentsSliders(adj: CurrentAdjustment) {
        
        switch adjustment {
        case .one:
            
            sliderBrightness.value = userdef.float(forKey: USERDEFAULT.kAdjust1SettingBrightness)
            sliderCotrast.value = userdef.float(forKey: USERDEFAULT.kAdjust1SettingContrass)
            sliderPurity.value = userdef.float(forKey: USERDEFAULT.kAdjust1SettingPurity)
            
            break
            
        case .two:
            
            sliderBrightness.value = userdef.float(forKey: USERDEFAULT.kAdjust2SettingBrightness)
            sliderCotrast.value = userdef.float(forKey: USERDEFAULT.kAdjust2SettingContrass)
            sliderPurity.value = userdef.float(forKey: USERDEFAULT.kAdjust2SettingPurity)
            
            break
    
        case .three:
            
            sliderBrightness.value = userdef.float(forKey: USERDEFAULT.kAdjust3SettingBrightness)
            sliderCotrast.value = userdef.float(forKey: USERDEFAULT.kAdjust3SettingContrass)
            sliderPurity.value = userdef.float(forKey: USERDEFAULT.kAdjust3SettingPurity)
            
            break
            
        case .custom:
            
            sliderBrightness.value = userdef.float(forKey: USERDEFAULT.kAdjustCustomSettingBrightness)
            sliderCotrast.value = userdef.float(forKey: USERDEFAULT.kAdjustCustomSettingContrass)
            sliderPurity.value = userdef.float(forKey: USERDEFAULT.kAdjustCustomSettingPurity)
            break
            
        default:
            break
        }
        
        adjustmentBrightness(byVal: sliderBrightness.value)
        adjustmentContrast(byVal: sliderCotrast.value)
        adjustmentSaturation(byVal: sliderPurity.value)
        
    }
    private func saveAdjustmentsSettings(slider: UISlider) {
        
        switch adjustment {
        case .one:
            
            if slider == sliderBrightness {
                userdef.set(slider.value, forKey: USERDEFAULT.kAdjust1SettingBrightness)
            } else if slider == sliderCotrast {
                userdef.set(slider.value, forKey: USERDEFAULT.kAdjust1SettingContrass)
            } else {
                userdef.set(slider.value, forKey: USERDEFAULT.kAdjust1SettingPurity)
            }
    
            break
            
        case .two:
            
            if slider == sliderBrightness {
                userdef.set(slider.value, forKey: USERDEFAULT.kAdjust2SettingBrightness)
            } else if slider == sliderCotrast {
                userdef.set(slider.value, forKey: USERDEFAULT.kAdjust2SettingContrass)
            } else {
                userdef.set(slider.value, forKey: USERDEFAULT.kAdjust2SettingPurity)
            }
            
            break
    
        case .three:
            
            if slider == sliderBrightness {
                userdef.set(slider.value, forKey: USERDEFAULT.kAdjust3SettingBrightness)
            } else if slider == sliderCotrast {
                userdef.set(slider.value, forKey: USERDEFAULT.kAdjust3SettingContrass)
            } else {
                userdef.set(slider.value, forKey: USERDEFAULT.kAdjust3SettingPurity)
            }
            
            break
            
        case .custom:
            
            if slider == sliderBrightness {
                userdef.set(slider.value, forKey: USERDEFAULT.kAdjustCustomSettingBrightness)
            } else if slider == sliderCotrast {
                userdef.set(slider.value, forKey: USERDEFAULT.kAdjustCustomSettingContrass)
            } else {
                userdef.set(slider.value, forKey: USERDEFAULT.kAdjustCustomSettingPurity)
            }
            break
            
        default:
            break
        }
    }
    func openImagePiker(mediaTypes: [String]) {
        
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        //        imagePicker.mediaTypes = mediaTypes
        
        imagePicker.allowsEditing = false
        imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    private func addImageInView(image: UIImage) {
        
        let imageView = UIImageView()
        imageView.image = image
        imageView.frame.size = CGSize(width: 150, height: 150)
        imageView.center = imgView.center
        imageView.isUserInteractionEnabled = true
        viewImgEdit.addSubview(imageView)
        addGestureFor(moveView: imageView)
        addGestureFor(zoomView: imageView)
    }
    
    private func setConnectDisConnect() {
        
        DispatchQueue.main.async {
            
            if connectedBLEDevice != nil && connectedBLEDevice?.isConnected() ?? false {
                
                self.btnTurnTable.isSelected = true
                self.btnConnectDisconnect.isSelected = true
                self.btnConnectDisconnectTitle.isSelected = true
                
            } else {
                
                self.btnTurnTable.isSelected = false
                self.btnConnectDisconnect.isSelected = false
                self.btnConnectDisconnectTitle.isSelected = false
            }
        }
    }
    
    private func setColorPickerOnKeyboard(textfield: UITextField) {
        
        var colorPickerframe = viewColorPicker.bounds
        
        colorPickerframe.origin.x = 25
        let colorPicker = ColorPickerView(frame: colorPickerframe)
        
        colorPicker.didChangeColor = { [unowned self] color in
            textfield.textColor = color
        }
        
        //        self.viewColorPicker.addSubview(colorPicker)
        
        let colorToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        colorToolbar.barStyle = .default
        colorToolbar.items = [UIBarButtonItem(customView: colorPicker)]
        colorToolbar.sizeToFit()
        textfield.inputAccessoryView = colorToolbar
        
    }
    
    private func addGestureFor(zoomView: UIView) {
        
        let pinGesture = UIPinchGestureRecognizer()
        
        zoomView.addGestureRecognizer(pinGesture)
        pinGesture.addTarget(self, action: #selector(zoomView(_:)))
    }
    
    private func addGestureFor(moveView: UIView) {
        
        let panGesture = UIPanGestureRecognizer()
        moveView.addGestureRecognizer(panGesture)
        panGesture.addTarget(self, action: #selector(moveView(gesture:)))
    }
    
    @IBAction func zoomView(_ gesture: UIPinchGestureRecognizer) {
        
        if let pinchView = gesture.view {
            
            //v.transform = CGAffineTransform(scaleX: pinch.scale, y: pinch.scale)
            pinchView.transform = pinchView.transform.scaledBy(x: gesture.scale, y: gesture.scale)
            gesture.scale = 1
        }
    }
    
    @objc private func moveView(gesture: UIPanGestureRecognizer) {
        
        let translation = gesture.translation(in: self.imgView)
        
        guard let gestview = gesture.view else {
            return
        }
        
        let point = gesture.location(in: self.imgView)
        
        //        if (point.x > self.viewImgEdit.frame.origin.x) && (point.x < self.viewImgEdit.frame.width) && (point.y > self.viewImgEdit.frame.origin.y) && (point.y < (self.viewImgEdit.frame.height - gestview.frame.height)) {
        
        gestview.center = CGPoint(x: point.x, y: point.y)
        
        //        } else {
        //            gesture.state = .ended
        //        }
    }
    func adjustmentSaturation(byVal: Float) {
        
        if let s = Selectedimgae {
        
            let beginImage = CIImage(image: s.img)
            self.filter = CIFilter(name: "CIColorControls")!
            self.filter?.setValue(beginImage, forKey: kCIInputImageKey)
            self.filter?.setValue(byVal, forKey: kCIInputSaturationKey)
            if let ciimage = self.filter?.outputImage {
                filteredImage = ciimage
                print("scale \(s.img.scale)")
                print("imageOrientation \(s.img.imageOrientation)")
                self.imgView.image = UIImage(ciImage: filteredImage!, scale: self.imgView.image!.scale, orientation: .left)
            }
        }
    }
    
    func adjustmentContrast(byVal: Float) {
        
        if let s = Selectedimgae {
        
            let beginImage = CIImage(image: s.img)
            self.filter = CIFilter(name: "CIColorControls")!
            self.filter?.setValue(beginImage, forKey: kCIInputImageKey)
            self.filter?.setValue(byVal, forKey: kCIInputContrastKey)
            if let ciimage = self.filter?.outputImage {
                filteredImage = ciimage
                self.imgView.image = UIImage(ciImage: filteredImage!)
            }
        }
    }
    func adjustmentBrightness(byVal: Float) {
        
        if let s = Selectedimgae {
        
            let beginImage = CIImage(image: s.img)
            self.filter = CIFilter(name: "CIColorControls")!
            self.filter?.setValue(beginImage, forKey: kCIInputImageKey)
            self.filter?.setValue(byVal, forKey: kCIInputBrightnessKey)
            if let ciimage = self.filter?.outputImage {
                filteredImage = ciimage
                self.imgView.image = UIImage(ciImage: filteredImage!)
            }
        }
    }
}
//MARK:-  UITextFieldDelegate Methods
extension EditPhotoVideoVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
//        textField.backgroundColor = .clear
        textField.removeFromSuperview()
        
        let lblsettextlabel : UILabel = UILabel()
        
        lblsettextlabel.text = selectedTextfield.text
        lblsettextlabel.textColor = selectedTextfield.textColor
        lblsettextlabel.frame = selectedTextfield.frame
        lblsettextlabel.textAlignment = .center
        lblsettextlabel.numberOfLines = 0
        lblsettextlabel.lineBreakMode = .byCharWrapping
        lblsettextlabel.sizeToFit()
        lblsettextlabel.frame = CGRect(x: selectedTextfield.frame.origin.x - 5, y: selectedTextfield.frame.origin.y - 5 , width: self.view.frame.width, height: lblsettextlabel.frame.size.height + 10)
        lblsettextlabel.isUserInteractionEnabled = true
        lblsettextlabel.isMultipleTouchEnabled = true
        
        selectedTextfield.removeFromSuperview()
        viewImgEdit.addSubview(lblsettextlabel)
        
        addGestureFor(moveView: lblsettextlabel)
        addGestureFor(zoomView: lblsettextlabel)
        
        return false
    }
}
// ---------------------------------------------------------------------------
//                          MARK:-  UIImagePickerControllerDelegate  -
// ---------------------------------------------------------------------------

extension EditPhotoVideoVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true) {
            if let image = info[.originalImage] as? UIImage {
                self.addImageInView(image: image)
            }
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true) {
            if self.arrOfEdited.count > 0 {
                self.arrOfEdited.removeLast()
            }
        }
    }
}

// ---------------------------------------------------------------------------
//                          MARK:-  imgCropped  -
// ---------------------------------------------------------------------------
extension EditPhotoVideoVC : CropViewControllerDelegate {
    
    func imgCropped(image: UIImage?) {
        
        self.arrOfEdited.append(Edited(lastEditing: .CropChange, lastImage: UIImage(view: viewImgEdit)))
        
        guard let image = image else {
            return
        }
        
        var config = Mantis.Config()
        config.showRotationDial = false
        let cropViewController = Mantis.cropViewController(image: image, config: config)
        cropViewController.modalPresentationStyle = .fullScreen
        cropViewController.delegate = self
        cropViewController.config.ratioOptions = .all
        
        present(cropViewController, animated: true)
    }
    
    func cropViewControllerDidCrop(_ cropViewController: CropViewController, cropped: UIImage, transformation: Transformation) {
        
        //Beacuse we add all view in viewImgEdit like text, logo, import image etc. cause when we crop image that time we make image from viewImgEdit. if we not remove than it will be visible multiple
        for sView in viewImgEdit.subviews {
            if sView != imgView {
                sView.removeFromSuperview()
            }
        }
        isCroped = true
        self.dismiss(animated: true, completion: nil)
        imgView.image = cropped
        if Selectedimgae != nil {
            Selectedimgae?.img = cropped
        }
    }
    
    func cropViewControllerDidCancel(_ cropViewController: CropViewController, original: UIImage) {
        isCroped = true
        self.dismiss(animated: true, completion: nil)
    }
}

extension UIImage {
    
    convenience init(view: UIView) {
        
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        defer { UIGraphicsEndImageContext() }
        let context = UIGraphicsGetCurrentContext()!
        view.layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        self.init(cgImage: image!.cgImage!)
        
        sleep(UInt32(0.5))
    }
}

// ---------------------------------------------------------------------------
//                          MARK:-  UIDocumentMenuDelegate  -
// ---------------------------------------------------------------------------
extension EditPhotoVideoVC : UIDocumentPickerDelegate {
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        
        guard let audiourl = urls.last else {
            return
        }
        
        print("audiourl is: \(audiourl)")
        
        if let s = Selectedimgae {
            
            merge(audioURL: audiourl, videoUrl: s.url)
        }
    }
    private func merge(audioURL: URL, videoUrl: URL) {
        
        showHUD()
        let mixComposition : AVMutableComposition = AVMutableComposition()
        var mutableCompositionVideoTrack : [AVMutableCompositionTrack] = []
        var mutableCompositionAudioTrack : [AVMutableCompositionTrack] = []
        let totalVideoCompositionInstruction : AVMutableVideoCompositionInstruction = AVMutableVideoCompositionInstruction()
        
        
        //start merge
        let aVideoAsset : AVAsset? = AVAsset(url: videoUrl)
        let aAudioAsset : AVAsset? = AVAsset(url: audioURL)
        
        mutableCompositionVideoTrack.append(mixComposition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: kCMPersistentTrackID_Invalid)!)
        mutableCompositionAudioTrack.append(mixComposition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid)!)
        
        let aVideoAssetTrack : AVAssetTrack = aVideoAsset!.tracks(withMediaType: AVMediaType.video)[0]
        let aAudioAssetTrack : AVAssetTrack = aAudioAsset!.tracks(withMediaType: AVMediaType.audio)[0]
        
        if mutableCompositionAudioTrack.count > 0 {
            do {
                try mutableCompositionVideoTrack[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aVideoAssetTrack, at: CMTime.zero)
                
                try mutableCompositionAudioTrack[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aAudioAssetTrack, at: CMTime.zero)
                
            } catch {
                
            }
        
            mutableCompositionVideoTrack.first?.preferredTransform = aVideoAssetTrack.preferredTransform
            totalVideoCompositionInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration )
            
            let mutableVideoComposition : AVMutableVideoComposition = AVMutableVideoComposition()
            mutableVideoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
            
            
            //find your video on this URl
            let filename : String  = "/Documents/newVideo\(Date()).mp4"
            let savePathUrl:URL = URL(fileURLWithPath: NSHomeDirectory() + filename )
            print(savePathUrl)
            
            let assetExport: AVAssetExportSession = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)!
            
            assetExport.outputFileType = .mp4
            assetExport.outputURL = savePathUrl as URL
            assetExport.shouldOptimizeForNetworkUse = true
            
            assetExport.exportAsynchronously { () -> Void in
                switch assetExport.status {
                    
                case AVAssetExportSession.Status.completed:
                    print("success")
                    
                    self.audioVideoMergeURL = savePathUrl

                case  AVAssetExportSession.Status.failed:
                    print("failed \(assetExport.error?.localizedDescription ?? "")")
                case AVAssetExportSession.Status.cancelled:
                    print("cancelled \(assetExport.error?.localizedDescription ?? "")")
                default:
                    print("complete")
                }
                self.hideHUD()
            }
        }
    }
}

//MARK:-  UIScrollViewDelegate Methods
extension EditPhotoVideoVC: UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return viewImgEdit
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        if scrollView.zoomScale > 1 {
            if let image = imgView.image {
                let ratioW = viewImgEdit.frame.width / image.size.width
                let ratioH = viewImgEdit.frame.height / image.size.height
                
                let ratio = ratioW < ratioH ? ratioW : ratioH
                let newWidth = image.size.width * ratio
                let newHeight = image.size.height * ratio
                let conditionLeft = newWidth*scrollView.zoomScale > viewImgEdit.frame.width
                let left = 0.5 * (conditionLeft ? newWidth - viewImgEdit.frame.width : (scrollView.frame.width - scrollView.contentSize.width))
                let conditioTop = newHeight*scrollView.zoomScale > viewImgEdit.frame.height
                
                let top = 0.5 * (conditioTop ? newHeight - viewImgEdit.frame.height : (scrollView.frame.height - scrollView.contentSize.height))
                
                scrollView.contentInset = UIEdgeInsets(top: top, left: left, bottom: top, right: left)
                
            }
        } else {
            scrollView.contentInset = .zero
        }
    }
}

