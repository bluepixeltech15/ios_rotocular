//
//  SettingsVC.swift
//  RotoSmartPic
//
//  Created by Mac on 14/05/1942 Saka.
//  Copyright © 1942 Bluepixel Technologies. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {
    
    
    //--------------------------------------------------------
    //                  MARK: - Outlet -
    //--------------------------------------------------------
    @IBOutlet weak var txtMicroStepping: UITextField!
    @IBOutlet weak var txtGearRatio: UITextField!
    
    
    //--------------------------------------------------------
    //                  MARK: -Property -
    //--------------------------------------------------------
    private var selectedTextField = UITextField()
    
    //--------------------------------------------------------
    //                  MARK: - View Life Cycle -
    //--------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTextFields()
    }
    
    //--------------------------------------------------------
    //                  MARK: -  Button Action -
    //--------------------------------------------------------
    @IBAction func btnSetting(sender:UIButton) {
        
        let resetSettingVC = storyboard?.instantiateViewController(withIdentifier: "ResetSettingVC") as! ResetSettingVC
        resetSettingVC.settingType = sender.tag == 0 ? .Camera:.Gallery
        self.navigationController?.pushViewController(resetSettingVC, animated: true)
    }
    
    @IBAction func btnBack(sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //--------------------------------------------------------
    //                  MARK: - Function -
    //--------------------------------------------------------
    private func setTextFields() {
        
        txtMicroStepping.text = userdef.string(forKey: USERDEFAULT.kMicroStepping)
        txtGearRatio.text = userdef.string(forKey: USERDEFAULT.kGearRatio)
  
        txtMicroStepping.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(btnDoneClicked(sender:)))
        txtGearRatio.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(btnDoneClicked(sender:)))
        
    }
    @objc private func btnDoneClicked(sender: UIButton) {
        
//        if selectedTextField == txtMicroStepping {
//            userdef.set(Int(txtMicroStepping.text!.trime()) ?? 3200, forKey: USERDEFAULT.kMicroStepping)
//            self.writeMicroStepping(value: Int(txtMicroStepping.text!.trime()) ?? 3200) {}
//            
//        } else {
//            userdef.set(Int(txtGearRatio.text!.trime()) ?? 6, forKey: USERDEFAULT.kGearRatio)
//            self.writeGearRatio(value: Int(txtGearRatio.text!.trime()) ?? 6) {}
//        }
    }
}

extension SettingsVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if selectedTextField == txtMicroStepping {
            userdef.set(Int(txtMicroStepping.text!.trime()) ?? 3200, forKey: USERDEFAULT.kMicroStepping)
            self.writeMicroStepping(value: Int(txtMicroStepping.text!.trime()) ?? 3200) {}
            
        } else {
            var setdata = Int(Double((txtGearRatio.text ?? "6" ).trimString()) ?? 6)
            userdef.set(setdata, forKey: USERDEFAULT.kGearRatio)
            self.writeGearRatio(value: (setdata * 10)) {}
        }
    }
}
