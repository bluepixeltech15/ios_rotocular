//
//  CustomSettingsVC.swift
//  RotoSmartPic
//
//  Created by Apple on 05/04/22.
//  Copyright © 2022 Bluepixel Technologies. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

var ispreviewselected = false
var currentSelectedProfile = 1
class CustomSettingsVC: UIViewController {
    
    // ----------------------------------------------------------
    //                       MARK: - Outlet -
    // ----------------------------------------------------------
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    
    @IBOutlet weak var btnCW: UIButton!
    @IBOutlet weak var lblCW: UILabel!
    
    @IBOutlet weak var btnCCW: UIButton!
    @IBOutlet weak var lblCCW: UILabel!
    
    @IBOutlet weak var lblProfile: UILabel!
    @IBOutlet weak var imgViewDropDown: UIImageView!
    @IBOutlet weak var txtDropDown: UITextField!
    
    @IBOutlet weak var lblAngle45: UILabel!
    @IBOutlet weak var lblAngle90: UILabel!
    @IBOutlet weak var lblAngle180: UILabel!
    @IBOutlet weak var lblAngle360: UILabel!
    @IBOutlet weak var txtCustomAngle: UITextField!
    
    @IBOutlet var btnAnglesCollection: [UIButton]!
    
    @IBOutlet weak var lblSelectedProfile: UILabel!
    
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var btnPreview: UIButton!

    // ----------------------------------------------------------
    //                       MARK: - Property -
    // ----------------------------------------------------------
    private var picker = UIPickerView()
    
    private var arrOfDictProfile: [[String: Any]] = [["profile": 1], ["profile": 2], ["profile": 3], ["profile": 4], ["profile": 5], ["profile": 6], ["profile": 7], ["profile": 8], ["profile": 9], ["profile": 10]]
    private var selectedOption1 = ""
    private var selectedOption2 = ""
    
    private var arrOfString = [String]()
    private var cart: [String: String] = [:]
    private var stringFromArray = ""
    private var isFirstTimeAppend = true
    
    private var completion: ((_ isok : Bool) -> ())?
    
    // ----------------------------------------------------------
    //                       MARK: - View Life Cycle -
    // ----------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        self.txtCustomAngle.delegate = self
    }

    // ----------------------------------------------------------
    //                       MARK: - Function -
    // ----------------------------------------------------------
    private func setupUI() {
        self.picker.delegate = self
        self.picker.dataSource = self
        self.txtDropDown.inputView = picker
        self.txtDropDown.text = "\(self.arrOfDictProfile[0]["profile"] ?? 0)"
        
        self.getSelectedProfileFromUserDefaults()
        currentSelectedProfile = Int((userdef.value(forKey: USERDEFAULT.kScheduleProfile) as? String) ?? "") ?? 1
        self.txtDropDown.text = "\(self.arrOfDictProfile[currentSelectedProfile - 1]["profile"] ?? 0)"
        
        self.lblSelectedProfile.text = (self.arrOfDictProfile[currentSelectedProfile - 1]["values"] as? [String])?.joined(separator: ", ")
        
        
        
//      set(self.txtDropDown.text, forKey: USERDEFAULT.kScheduleProfile)
    }
    
    private func addSelectedProfileInUserDefaults() {
        let selectedOptions = self.selectedOption1 + self.selectedOption2
        
        if var values = self.arrOfDictProfile[currentSelectedProfile - 1]["values"] as? [String] {
            if values.count < 15{
                values.append(selectedOptions)
                self.arrOfDictProfile[currentSelectedProfile - 1]["values"] = values
            }
        } else {
            self.arrOfDictProfile[currentSelectedProfile - 1]["values"] = [selectedOptions]
        }
        
        self.lblSelectedProfile.text = (self.arrOfDictProfile[currentSelectedProfile - 1]["values"] as? [String])?.joined(separator: ", ")

        userdef.set(self.arrOfDictProfile, forKey: USERDEFAULT.kSelectedProfile)
        
        self.resetSelection()
    }
    
    private func getSelectedProfileFromUserDefaults() {
        if let selectedProfileFromUserDefaults = userdef.object(forKey: USERDEFAULT.kSelectedProfile) as? [[String: Any]] {
            print("selectedProfileFromUserDefaults :=> \(selectedProfileFromUserDefaults)")
            self.arrOfDictProfile = selectedProfileFromUserDefaults
            self.picker.reloadAllComponents()
        }
    }
    
    private func removeSelectedProfileFromUserDefaults() {
        let values = self.arrOfDictProfile[currentSelectedProfile - 1]["values"] as? [String]
        print("values :=> \(values ?? [])")
        
        self.arrOfDictProfile[currentSelectedProfile - 1]["values"] = []
        print("self.arrOfDictProfile :=> \(self.arrOfDictProfile)")
        
        userdef.set(self.arrOfDictProfile, forKey: USERDEFAULT.kSelectedProfile)
        
        self.lblSelectedProfile.text = (self.arrOfDictProfile[currentSelectedProfile - 1]["values"] as? [String])?.joined(separator: ", ")
        self.picker.reloadAllComponents()
    }
    
    private func resetSelection() {
        self.btnCW.setImage(UIImage(named: "ic_Radio_Unselected_small"), for: .normal)
        self.btnCCW.setImage(UIImage(named: "ic_Radio_Unselected_small"), for: .normal)
        
        for i in self.btnAnglesCollection {
            i.setImage(UIImage(named: "ic_Radio_Unselected_small"), for: .normal)
        }
        
        self.selectedOption1 = ""
        self.selectedOption2 = ""
        self.txtCustomAngle.text = ""
    }
    
    private func isValidCustomAngle() -> Bool {
        let customAngle = Int(self.txtCustomAngle.text ?? "") ?? 0
        print("temp :=> \(customAngle)")
        
        if customAngle < 0 || customAngle > 999 {
            print("NOT ALLOW")
            return false
        } else {
            print("ALLOW")
            return true
        }
    }
    
    private func addCustomAngle() {
        for btn in self.btnAnglesCollection {
            if btn.tag == 4 {
                btn.setImage(UIImage(named: "ic_Radio_Selected_small"), for: .normal)
            }
        }
        
        self.addSelectedProfileInUserDefaults()
    }
    
    private func isValidData() -> Bool {
        
        if self.selectedOption1 == "" || self.selectedOption2 == "" {
            print("NOT VALID DATA...")
            self.resetSelection()
        } else {
            self.view.endEditing(true)
            return true
        }
        return false
    }

    internal func callBack(completion: @escaping ((_ isOkay: Bool) -> ())) {
        self.completion = completion
    }
    
    // ----------------------------------------------------------
    //                       MARK: - Button Action -
    // ----------------------------------------------------------
    @objc func didPressOnDoneButton() {
        self.txtCustomAngle.resignFirstResponder()
        print("Done Tapped...")
        
        self.selectedOption2 = self.txtCustomAngle.text ?? ""
        
        if self.isValidData() {
            if self.isValidCustomAngle() {
                self.addCustomAngle()
            }
        }
    }

    // ----------------------------------------------------------
    //                       MARK: - Action -
    // ----------------------------------------------------------
    @IBAction func onBtnClose(_ sender: UIButton) {
        self.completion!(false)
        self.dismiss(animated: true)
    }
    
    @IBAction func onBtnEdit(_ sender: UIButton) {
        self.removeSelectedProfileFromUserDefaults()
    }
    
    @IBAction func onBtnOk(_ sender: UIButton) {
        userdef.set(String(currentSelectedProfile), forKey: USERDEFAULT.kScheduleProfile)
        
        var values = self.arrOfDictProfile[currentSelectedProfile - 1]["values"] as? [String]
        if (values?.count ?? 0) > 0{
        self.completion!(true)
        self.dismiss(animated: true)
        }else{
            self.showAlertWithOKButton(message: "Please select angle")
        }
    }
    
    @IBAction func onBtnPreview(_ sender: UIButton) {
        userdef.set(String(currentSelectedProfile), forKey: USERDEFAULT.kScheduleProfile)
        
        var values = self.arrOfDictProfile[currentSelectedProfile - 1]["values"] as? [String]
        if (values?.count ?? 0) > 0{
            sender.isSelected = !sender.isSelected
            ispreviewselected = sender.isSelected
            self.completion!(true)
            self.dismiss(animated: true)
        }else{
            self.showAlertWithOKButton(message: "Please select angle")
        }
    }
    
    @IBAction func onBtnUndo(_ sender: UIButton) {
        var values = self.arrOfDictProfile[currentSelectedProfile - 1]["values"] as? [String]
        print("values :=> \(values ?? [])")
        if (values?.count ?? 0) > 0{
            values?.removeLast()
            self.arrOfDictProfile[currentSelectedProfile - 1]["values"] = values
            print("self.arrOfDictProfile :=> \(self.arrOfDictProfile)")
            
            userdef.set(self.arrOfDictProfile, forKey: USERDEFAULT.kSelectedProfile)
            
            self.lblSelectedProfile.text = (self.arrOfDictProfile[currentSelectedProfile - 1]["values"] as? [String])?.joined(separator: ", ")
            self.picker.reloadAllComponents()
        }
//        if (lblSelectedProfile.text?.count ?? 0) > 0 {
//        let chars = lblSelectedProfile.text?.split(separator: ",")
//            var text = lblSelectedProfile.text?.replacingOccurrences(of: (chars?[(chars?.count ?? 1) - 1]) ?? "", with: "")
//            if text?.last == ","
//            {
//                text?.removeLast()
//            }
//            lblSelectedProfile.text = text
//        }
    }
    
    @IBAction func onBtnCW(_ sender: UIButton) {
        self.btnCW.setImage(UIImage(named: "ic_Radio_Selected_small"), for: .normal)
        self.btnCCW.setImage(UIImage(named: "ic_Radio_Unselected_small"), for: .normal)
        
        //self.selectedOption1 = self.lblCW.text ?? ""
        self.selectedOption1 = "c"
    }
    
    @IBAction func onBtnCCW(_ sender: UIButton) {
        self.btnCCW.setImage(UIImage(named: "ic_Radio_Selected_small"), for: .normal)
        self.btnCW.setImage(UIImage(named: "ic_Radio_Unselected_small"), for: .normal)
        
        //self.selectedOption1 = self.lblCCW.text ?? ""
        self.selectedOption1 = "w"
    }
    
    @IBAction func onBtnAngles(_ sender: UIButton) {
        
        for btn in self.btnAnglesCollection {
            if btn.tag == sender.tag {
                
                btn.setImage(UIImage(named: "ic_Radio_Selected_small"), for: .normal)
                
                if btn.tag == 0 {
                    self.selectedOption2 = self.lblAngle45.text ?? ""
                } else if btn.tag == 1 {
                    self.selectedOption2 = self.lblAngle90.text ?? ""
                } else if btn.tag == 2 {
                    self.selectedOption2 = self.lblAngle180.text ?? ""
                } else if btn.tag == 3 {
                    self.selectedOption2 = self.lblAngle360.text ?? ""
                }
                
                if btn.tag == 4 {
                    self.selectedOption2 = self.txtCustomAngle.text ?? ""
                    self.txtCustomAngle.becomeFirstResponder()
                } else {
                    if self.isValidData() {
                        self.addSelectedProfileInUserDefaults()
                    }
                }
                
            } else {
                btn.setImage(UIImage(named: "ic_Radio_Unselected_small"), for: .normal)
            }
        }
    }

    // ----------------------------------------------------------
    //                       MARK: - API Calling -
    // ----------------------------------------------------------

}

// ----------------------------------------------------------
//                       MARK: - UITableView DataSource -
// ----------------------------------------------------------

// ----------------------------------------------------------
//                       MARK: - UITableView Delegate -
// ----------------------------------------------------------


// ---------------------------------------------------------------
//                       MARK: - UIPickerViewDataSource Methods -
// ---------------------------------------------------------------
extension CustomSettingsVC: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.arrOfDictProfile.count
    }
}

// ---------------------------------------------------------------
//                       MARK: - UIPickerViewDelegate Methods -
// ---------------------------------------------------------------
extension CustomSettingsVC: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(self.arrOfDictProfile[row]["profile"] ?? 0)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.txtDropDown.text = "\(self.arrOfDictProfile[row]["profile"] ?? 0)"
        currentSelectedProfile = row + 1
        
        self.lblSelectedProfile.text = (self.arrOfDictProfile[row]["values"] as? [String])?.joined(separator: ", ")
        
        self.resetSelection()
    }
}

// ---------------------------------------------------------------
//                       MARK: - UITextFieldDelegate Methods -
// ---------------------------------------------------------------
extension CustomSettingsVC: UITextFieldDelegate {
    
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        //let updatedStringCount = (textField.text ?? "").count + string.count - range.length
        
        switch textField {
            
//        case self.txtDropDown:
//            if (updatedString?.count ?? 0) > 30 {
//                return false
//            }
//
//            //if updatedString?.isBlank ?? true {
//            //if updatedStringCount == 0 {
//
//            //            if updatedString == "\n" {
//            //                self.isSearchEnable = false
//            //                self.arrSearchList = self.arrUserNewsfeed
//            //                self.tblNewsFeed.reloadData()
//            //
//            //            } else {
//            //                self.isSearchEnable = true
//            //                self.getSearchNewFeed( searchText: updatedString ?? "")
//            //            }
            
        case self.txtCustomAngle:
            let invocation = IQInvocation(self, #selector(didPressOnDoneButton))
            self.txtCustomAngle.keyboardToolbar.doneBarButton.invocation = invocation
            
            print(Int(updatedString ?? "") ?? 0)
            
            let aSet = NSCharacterSet(charactersIn: "1234567890").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            if (string == numberFiltered) == false {
                return false
            }
            if updatedString!.count > 3 {
                //self.btnValidPhone.isHidden = false
                
            } else {
                //self.btnValidPhone.isHidden = true
            }
            
            if let text = textField.text,
               let textRange = Range(range, in: text) {
                let ledValue = Int(text.replacingCharacters(in: textRange, with: string)) ?? 0
                
                if ledValue > 999 {
                    return false
                }
                //setGroupValue(value: ledValue, true)
                
            } else {
                return false
            }
            
        default:
            return true
        }
        return true
    }
}
