//
//  ScanListCell.swift
//  BLE Updater
//
//  Created by Nikhil Jobanputra on 01/04/19.
//  Copyright © 2020 Bluepixel Technology. All rights reserved.
//

import UIKit

class ScanListCell: UITableViewCell {

    @IBOutlet weak var btnConnect: UIButton!
    @IBOutlet weak var lblShowDeviceName: UILabel!
    @IBOutlet weak var lblShowRSSI: UILabel!
    @IBOutlet weak var onButtonConnect: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}
