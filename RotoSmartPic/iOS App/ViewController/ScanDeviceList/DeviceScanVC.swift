//
//  ShowScanListVC.swift
//  BLE Connect
//
//  Created by Nikhil Jobanputra on 01/07/20.
//  Copyright © 2020 Bluepixel Technology. All rights reserved.
//

import UIKit
import CoreBluetooth

class DeviceScanVC: UIViewController {
    
    //----------------------------------------------------------------------------
    //                              MARK: - Outlet -
    //----------------------------------------------------------------------------
    @IBOutlet weak var tblShow: UITableView!
    
    //----------------------------------------------------------------------------
    //                              MARK: - Property -
    //----------------------------------------------------------------------------
    enum scanFor {
        case RotoSmartDevice
        case LEDControllDevice
    }
    
    var scanfor:scanFor = .RotoSmartDevice
    var isfirsttime = true
    var arrOfBleDevices = [BLEDevice]()
    var BLEStateObje : Any?
    var refreshControl = UIRefreshControl()
    
    //----------------------------------------------------------------------------
    //                              MARK: - View Life Cycle -
    //----------------------------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addPullTorefresh()
        self.setNotificationObserver()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.startDevice()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    //----------------------------------------------------------------------------
    //                              MARK: - Buttons Clicked Actions -
    //----------------------------------------------------------------------------
    @IBAction func btnBack(sender:UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //----------------------------------------------------------------------------
    //                              MARK: - Function -
    //----------------------------------------------------------------------------
    func startDevice() {
        
        showHUD()
        
        //        let services = (scanfor == .RotoSmartDevice) ? ["FFE0", "FEE1"]:["6E400001-B5A3-F393-E0A9-E50E24DCCA9E"] //0001 => 6E400001-B5A3-F393-E0A9-E50E24DCCA9E is UART Service
        
        let services = self.scanfor == .RotoSmartDevice ? ["FFE0"] : ["AAA1" , "FEE1"]
        logfile.appendNewText(text: "Scanning for services: \(services)")
        self.arrOfBleDevices.removeAll()
        blemanager.StartScanning(WithServices: services) { (device) in
            
            let index = self.arrOfBleDevices.firstIndex(where: { (d) -> Bool in
                return d.uuid == device.uuid
            })
            
            if index == nil {
                self.arrOfBleDevices.append(device)
            } else {
                self.arrOfBleDevices[index!] = device
            }
            
            DispatchQueue.main.async {
                if self.arrOfBleDevices.count > 0 {
                    if self.isfirsttime {
                        self.isfirsttime = false
                        self.hideHUD()
                    }
                }
                self.tblShow.reloadData()
            }
        }
    }
    func setNotificationObserver() {
        
        self.BLEStateObje = NotificationCenter.default.addObserver(self, selector: #selector(self.BLEState(notification:)), name: Notification.Name("bleState"), object: nil)
    }
    func addPullTorefresh() {
        
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to Refresh", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.refreshControl.addTarget(self, action: #selector(IntialSetup), for: UIControl.Event.valueChanged)
        
        self.tblShow.addSubview(refreshControl)
    }
    @objc func IntialSetup() {
        
        if blemanager.isScanningStart {
            
            self.arrOfBleDevices.removeAll()
            self.tblShow.reloadData()
            self.refreshControl.endRefreshing()
            blemanager.StopScan()
            self.startDevice()
            
        } else {
            
            if !blemanager.IsBluetoothOn {
                
                self.arrOfBleDevices.removeAll()
                self.tblShow.reloadData()
                self.refreshControl.endRefreshing()
            }
        }
    }
    @objc func BLEState(notification: Notification) {
        
        let BLEState = notification.object as? Bool
        DispatchQueue.main.async {
            
            if BLEState! {
                self.startDevice()
            } else {
                self.hideHUD()
            }
            NotificationCenter.default.removeObserver(self.BLEStateObje!)
            return
        }
    }
}


//----------------------------------------------------------------------------
//                              MARK: - UITableViewDelegate and UITableViewDataSource -
//----------------------------------------------------------------------------
extension DeviceScanVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrOfBleDevices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: ScanListCell = tableView.dequeueReusableCell(withIdentifier: "scanListCell") as! ScanListCell
        
        let device = arrOfBleDevices[indexPath.row]
        cell.lblShowDeviceName.text = device.localName ?? device.name ?? "N/A"
        cell.lblShowRSSI.text       = device.uuid
        cell.onButtonConnect.tag    = indexPath.row
        
        cell.onButtonConnect.addTarget(self, action: #selector(self.onConnect(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func onConnect(sender: UIButton) {
        
        let device = arrOfBleDevices[sender.tag]
        self.showHUD()
        
        device.connect(complition: { (isSucess, connectedDevice, error) in
            
            if isSucess {
                
                blemanager.StopScan()
                
                if self.scanfor == .RotoSmartDevice {
                    
                    connectedBLEDevice = connectedDevice
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                        
                        var setdata = Int(Double((userdef.string(forKey: USERDEFAULT.kGearRatio) ?? "6" ).trimString()) ?? 6)
                        
                        self.writeGearRatio(value: (setdata * 10)) {
                            
                            self.writeMicroStepping(value: Int(Double(userdef.string(forKey: USERDEFAULT.kMicroStepping) ?? "3200") ?? 3200)) {
                                
                                self.writeSpeed(value: 10) {
                                    
                                    
                                    DispatchQueue.main.async {
                                        self.hideHUD()
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }
                            }
                        }
                    })
                    
                } else {
                    
                    connectedLEDDevice = connectedDevice
                    logfile.appendNewText(text: "Connect Successfull with LED Device")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                        
                        LEDDevice.writePassword { (isWriten, message) in
                            
                            if isWriten {
                                DispatchQueue.main.async {
                                    self.hideHUD()
                                    self.navigationController?.popViewController(animated: true)
                                }
                            } else {
                                self.hideHUD()
                                self.showAlertWithOKButton(message: message)
                            }
                        }
                    })
                }
                
            } else {
                
                print("ConnectedDevice:->\(connectedDevice)")
                self.onConnect(sender: sender)
                self.hideHUD()
            }
        })
    }
}

