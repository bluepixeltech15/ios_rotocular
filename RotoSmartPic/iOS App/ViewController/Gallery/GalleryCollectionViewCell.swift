//
//  FolderCollectionViewCell.swift
//  RotoSmartPic
//
//  Created by BAPS on 07/11/20.
//  Copyright © 2020 Bluepixel Technologies. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
    
    //MARK:-  Outlets And Variable Declarations
    @IBOutlet weak var imgGallery: UIImageView!
    @IBOutlet weak var btnSelectDeselect: UIButton!
    
}
