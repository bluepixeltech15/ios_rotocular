//
//  GalleryVC.swift
//  RotoSmartPic
//
//  Created by BAPS on 07/11/20.
//  Copyright © 2020 Bluepixel Technologies. All rights reserved.
//

import UIKit
import AVFoundation

class GalleryVC: UIViewController {
    
    //MARK:-  Outlets And Variable Declarations
    @IBOutlet weak var collectionViewGallery: UICollectionView!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    
    public var gallaryOpenType : GallaryOpenType  = .Selection
    
    public var folder: CaptureType = .Photo
    public var subFolder: AngleTypes = .Single
    
    private var arrOfImages = [image]()
    
    //MARK:- 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setButtons()
        getFolderRelatedImagesImages()
    }
    
    //MARK:-  Buttons Clicked Actions
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnDeleteClicked(_ sender: UIButton) {
        
        let selectedImagesOrVideo = self.arrOfImages.filter({$0.isSelected})
        
        if selectedImagesOrVideo.count > 0 {
        
            self.showAlertWith2Buttons(message: "Are you sure want to delete these?", btnOneName: "Yes", btnTwoName: "No") { (btnClickedIndex) in
                
                if btnClickedIndex == 1 {
                    
                    if self.gallaryOpenType == .Preview {
                        
                        for image in selectedImagesOrVideo {
                            storeManager.removeFileFrom(url: image.url)
                        }
                        self.getFolderRelatedImagesImages()
                        
                    } else {
                        
                        if let identifiers = selectedImagesOrVideo.map({$0.photoidentifier}) as? [String] {
                            albumManager?.removeFileFrom(identifier: identifiers, completion: { (isSuccess, message) in
                                
                                if !isSuccess {
//                                    self.showAlertWithOKButton(message: message)
                                }
                                self.getFolderRelatedImagesImages()
                            })
                        }
                    }
                }
            }
        }
    }
    @IBAction func btnShareClicked(_ sender: UIButton) {
        
        let files = arrOfImages.filter({$0.isSelected})
        
        var shareItems = [Any]()
        for f in files {
            if f.isSelected {
                if f.folder == .some(.Photo) {
                    shareItems.append(f.img!)
                } else {
                    shareItems.append(f.url!)
                }
            }
        }
        share(items: shareItems)
    }
    
    //MARK:-  Functions
    private func setButtons() {
        
        switch gallaryOpenType {
        case .Selection:
            
            btnDelete.isHidden = true
            btnShare.isHidden = true
            break
            
        case .Gallery:
            
            btnDelete.isHidden = false
            btnShare.isHidden = false
            break
            
        case .Preview:
            
            btnDelete.isHidden = false
            btnShare.isHidden = true
            break
            
        default:
            break
        }
    }
    private func getFolderRelatedImagesImages() {
        
        arrOfImages.removeAll()
        if gallaryOpenType == .Preview {
            
            let files = storeManager.getAllFiles(directoryName: Constant.kRotoPic, folderName: folder.rawValue, subfolder: subFolder.rawValue) ?? [URL]()
            
            for file in files {
                if folder == .Photo {
                    do {
                        let imageData = try Data(contentsOf: file)
                        arrOfImages.append(image(img: UIImage(data: imageData), isSelected: false, url: file, folder: .Photo, subfolder: subFolder))
                    } catch {
                        print("Not able to load image")
                    }
                } else {
                    arrOfImages.append(image(img: createThumbnail(url: file), isSelected: false, url: file, folder: .Video, subfolder: subFolder))
                }
            }
            
            self.collectionViewGallery.reloadData()
            
        } else {
            
            let albumName = getFolderNameOfPhotos(folder: folder, subFolder: subFolder)
            albumManager?.getFilesFrom(album: albumName, fileType: folder, completion: { (allFiles) in
                
                for file in allFiles ?? [AlbumManager.fileAsset]() {
                    
                    if self.folder == .Photo {
                        self.arrOfImages.append(image(img: file.imgOrVideo as? UIImage, isSelected: false, url: nil, folder: self.folder, subfolder: self.subFolder, photoidentifier: file.localIdentifier))
                    } else {
                        
                        if let fileUrl = file.imgOrVideo as? URL {
                            self.arrOfImages.append(image(img: self.createThumbnail(url: fileUrl), isSelected: false, url: fileUrl, folder: self.folder, subfolder: self.subFolder, photoidentifier: file.localIdentifier))
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.collectionViewGallery.reloadData()
                }
            })
        }
    }
    private func createThumbnail(url: URL) -> UIImage {
        
        let asset = AVURLAsset(url: url, options: nil)
        let imgGenerator = AVAssetImageGenerator(asset: asset)
        imgGenerator.appliesPreferredTrackTransform = true
        let cgImage = try! imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
        // !! check the error before proceeding
        return UIImage(cgImage: cgImage)
    }
}

//MARK:-  UICollectionViewDelegateFlowLayout Methods
extension GalleryVC : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.bounds.width / 3.0
        return CGSize(width: width, height: width)
    }
}

//MARK:-  UICollectionViewDataSource Methods
extension GalleryVC : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrOfImages.count
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCollectionViewCell", for: indexPath) as! GalleryCollectionViewCell
        
        let img = arrOfImages[indexPath.row]
        
        cell.imgGallery.image = img.img
        cell.btnSelectDeselect.isSelected = img.isSelected ?? false
        
        cell.btnSelectDeselect.tag = indexPath.row
        cell.btnSelectDeselect.addTarget(self, action: #selector(btnSelectDeselectClicked(_:)), for: .touchUpInside)
        
        switch gallaryOpenType {
            
        case .Selection:
            cell.btnSelectDeselect.isHidden = true
            break
            
        case .Gallery, .Preview:
            cell.btnSelectDeselect.isHidden = false
            break
    
        default:
            break
        }
        
        return cell
    }
    @objc private func btnSelectDeselectClicked(_ sender: UIButton) {
        arrOfImages[sender.tag].isSelected = !arrOfImages[sender.tag].isSelected
        collectionViewGallery.reloadItems(at: [IndexPath(item: sender.tag, section: 0)])
    }
}

//MARK:-  UICollectionViewDelegate Methods
extension GalleryVC : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if gallaryOpenType == .Selection {
            
            Selectedimgae = arrOfImages[indexPath.row]
            for vc in self.navigationController!.viewControllers {
                
                if (vc is EditPhotoVideoVC) {
                    self.navigationController?.popToViewController(vc, animated: true)
                }
            }
            
        } else if folder == .Photo {
            
            let zoomVC = self.storyboard?.instantiateViewController(withIdentifier: "ZoomImageVC") as! ZoomImageVC
            zoomVC.image = arrOfImages[indexPath.row].img
            self.navigationController?.pushViewController(zoomVC, animated: true)
            
        } else {
            
            self.playVideo(url: arrOfImages[indexPath.row].url!)
        }
    }
}
