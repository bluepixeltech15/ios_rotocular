//
//  CustomAngleVC.swift
//  RotoSmartPic
//
//  Created by BAPS on 13/11/20.
//  Copyright © 2020 Bluepixel Technologies. All rights reserved.
//

import UIKit

class CustomAngleVC: UIViewController {

    //MARK:-  Outlets And Variable Declarations
    @IBOutlet weak var txtAngles: UITextField!
    @IBOutlet weak var viewAngle: AngleView!
    
    typealias okComplitionBlock = (Bool, [Int]?) -> Void
    private var okCompletion: okComplitionBlock!

    //MARK:- 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtAngles.text = userdef.string(forKey: USERDEFAULT.kCustomAngles)
        drawAngles(anglesString: txtAngles.text!)
    }
    
    //MARK:-  Buttons Clicked Actions
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        
        self.dismiss(animated: true) {
            self.okCompletion(false, nil)
        }
    }
    @IBAction func btnOkClicked(_ sender: UIButton) {
        
        let angleText = self.txtAngles.text!.trime()
        
        if angleText.isEmpty {
            self.showAlertWithOKButton(message: "Please enter a angle")
            
        } else {
            
            userdef.set(angleText, forKey: USERDEFAULT.kCustomAngles)
            let angles = self.viewAngle.multiAngles
            
            self.dismiss(animated: true) {
                self.okCompletion(true, angles.sorted())
            }
        }
    }
    
    //MARK:-  Functions
    func isOkClicked(mycompletion: @escaping okComplitionBlock) {
        self.okCompletion = mycompletion
    }
    private func drawAngles(anglesString: String) {
        
        let angles = anglesString.components(separatedBy: ",")
        
        var intAngles = [Int]()
        for a in angles {
            let value = Int(a) ?? 0
            intAngles.append(value)
        }
        intAngles.append(360)
        self.viewAngle.multiAngles = intAngles
    }
}

//MARK:-  UITextFieldDelegate Methods
extension CustomAngleVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let charSet = "0123456789,"
        print("New entered character \(string)")
        
        if charSet.contains(string) || string.count == 0 {
            
            if let text = textField.text,
                let textRange = Range(range, in: text) {
                
                let updatedText = text.replacingCharacters(in: textRange, with: string)
                
                let angles = updatedText.components(separatedBy: ",")
                
                for a in angles {
                    let value = Int(a) ?? 0
                    if value > 360 {
                        return false
                    }
                }
                self.drawAngles(anglesString: updatedText)
            }
            return true
            
        } else {
            return false
        }
    }
}

