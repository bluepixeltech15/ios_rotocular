//
//  ResetSettingVC.swift
//  RotoSmartPic
//
//  Created by Mac on 14/05/1942 Saka.
//  Copyright © 1942 Bluepixel Technologies. All rights reserved.
//

import UIKit

class ResetSettingVC: UIViewController {
    
    
    //--------------------------------------------------------
    //                  MARK: - Outlet -
    //--------------------------------------------------------
    @IBOutlet var btnBoxCollection: [UIButton]!
    @IBOutlet var imgcheckBoxCollection: [UIImageView]!
    
    //--------------------------------------------------------
    //                  MARK: -Property -
    //--------------------------------------------------------
    
    enum SettingType {
        case Camera
        case Gallery
    }
    
    //getting from settings VC when clicked on setting button
    var settingType: SettingType = .Camera
    
    //--------------------------------------------------------
    //                  MARK: - View Life Cycle -
    //--------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setSettings()
    }
    
    //--------------------------------------------------------
    //                  MARK: -  Button Action -
    //--------------------------------------------------------
    @IBAction func btnBack(sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnCheckboxSelecet(sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            self.imgcheckBoxCollection[sender.tag].image = UIImage(named: "ic_checkboxSelected")
        } else {
            self.imgcheckBoxCollection[sender.tag].image = UIImage(named: "ic_checkboxUnselected")
        }
        
        if settingType == .Camera {
            if sender.tag == 0 {
                userdef.set(sender.isSelected, forKey: USERDEFAULT.kCameraSettingMaxResolution)
            } else if sender.tag == 1 {
                userdef.set(sender.isSelected, forKey: USERDEFAULT.kCameraSettingEnablePro)
            } else {
                userdef.set(sender.isSelected, forKey: USERDEFAULT.kCameraSettingSaveToSDCard)
            }
            
        } else {
            
            if sender.tag == 0 {
                userdef.set(sender.isSelected, forKey: USERDEFAULT.kGallerySettingMaxResolution)
            } else if sender.tag == 1 {
                userdef.set(sender.isSelected, forKey: USERDEFAULT.kGallerySettingEnablePro)
            } else {
                userdef.set(sender.isSelected, forKey: USERDEFAULT.kGallerySettingSaveToSDCard)
            }
        }
    }
    
    //--------------------------------------------------------
    //                  MARK: - Function -
    //--------------------------------------------------------
    private func setSettings() {
        
        let button = UIButton()
        
        if settingType == .Camera {
            
            if userdef.bool(forKey: USERDEFAULT.kCameraSettingMaxResolution) {
                button.tag = 0
                setSeletedSettings(sender: button)
            }
            
            if userdef.bool(forKey: USERDEFAULT.kCameraSettingEnablePro) {
                button.tag = 1
                setSeletedSettings(sender: button)
            }
            
            if userdef.bool(forKey: USERDEFAULT.kCameraSettingSaveToSDCard) {
                button.tag = 2
                setSeletedSettings(sender: button)
            }
            
        } else {
            
            if userdef.bool(forKey: USERDEFAULT.kGallerySettingMaxResolution) {
                button.tag = 0
                setSeletedSettings(sender: button)
            }
            
            if userdef.bool(forKey: USERDEFAULT.kGallerySettingEnablePro) {
                button.tag = 1
                setSeletedSettings(sender: button)
            }
            
            if userdef.bool(forKey: USERDEFAULT.kGallerySettingSaveToSDCard) {
                button.tag = 2
                setSeletedSettings(sender: button)
            }
        }
    }
    private func setSeletedSettings(sender: UIButton) {
        
        for btn in btnBoxCollection {
            if btn.tag == sender.tag {
                btn.isSelected = true
            }
        }
        
        for img in imgcheckBoxCollection {
            if img.tag == sender.tag {
                self.imgcheckBoxCollection[sender.tag].image = UIImage(named: "ic_checkboxSelected")
            }
        }
    }
}
