//
//  LEDControllCollectionViewCell.swift
//  RotoSmartPic
//
//  Created by BAPS on 01/02/21.
//  Copyright © 2021 Bluepixel Technologies. All rights reserved.
//

import UIKit

class LEDControllCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblLEDIndex: UILabel!
    @IBOutlet weak var txtAngel: UITextField!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var btnMinus: UIButton!
    
    private var currentValue = 0
    var parent: LEDControllVC!
    var indexPath: IndexPath!
    
    //MARK:- 
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txtAngel.delegate = self
    }
    
    //MARK:-  Buttons Clicked Actions
    @IBAction func btnMinusClicked(_ sender: UIButton) {
        if currentValue > 0 {
            self.indexPath = IndexPath(item: sender.tag, section: 0)
            setLEDValue(ledValue: (currentValue - 1))
        }
    }
    @IBAction func btnPlusClicked(_ sender: UIButton) {
        if currentValue < 245 {
            self.indexPath = IndexPath(item: sender.tag, section: 0)
            setLEDValue(ledValue: (currentValue + 1))
        }
    }

    //MARK:-  Functions
    func configure(ledValue: Int, parentVC: LEDControllVC, cellindexpath: IndexPath, isEnable: Bool) {
        
        self.parent = parentVC
        self.indexPath = cellindexpath
        self.txtAngel.isUserInteractionEnabled  = isEnable
        self.btnPlus.isUserInteractionEnabled   = isEnable
        self.btnMinus.isUserInteractionEnabled  = isEnable
        self.txtAngel.tag = cellindexpath.item
        self.btnPlus.tag = cellindexpath.item
        self.btnMinus.tag = cellindexpath.item
        
       // print("DD \(self.parent.arrOfGroup[parent.selectedGroupIndex].arrOfGroupValues[indexPath.row].groupNumber)")

        
        lblLEDIndex.text = "\((cellindexpath.item) + 1)"
        currentValue = ledValue
        txtAngel.text = "\(ledValue)"
        
        
        
//        self.isUserInteractionEnabled = false
//        if !self.parent.isfirattime{
//        setLEDValue(ledValue: ledValue)
//        }
    }
    private func setLEDValue(ledValue: Int, _ isTextChanged: Bool = false) {
        
        if !isTextChanged {
            txtAngel.text = "\(ledValue)"
        }
        
        currentValue = ledValue
        parent.arrOfGroup[parent.selectedGroupIndex].arrOfIdividualValues[indexPath.item] = ledValue
    
        LEDDevice.shared.startCMDTimer(timeInterval: 0.01)
        LEDDevice.shared.setLEDsValues(LEDIndexAndValues: [LEDIndexAndValue(index: indexPath.row, value: ledValue)]) { (isSuccess, message) in
            
            if isSuccess {
                
            } else {
                self.parent.showAlertWithOKButton(message: message)
            }
        }
    }
}

//MARK:-  UITextFieldDelegate
extension LEDControllCollectionViewCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let ledValue = Int(text.replacingCharacters(in: textRange, with: string)) ?? 0
            
            if ledValue > 255 {
                return false
            }
            self.indexPath = IndexPath(item: textField.tag, section: 0)
            //setLEDValue(ledValue: ledValue, true)
        } else {
            return false
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        print("textFieldDidEndEditing \(textField.text)")
        let ledValue = Int(Double(textField.text ?? "0") ?? 0)
        self.indexPath = IndexPath(item: textField.tag, section: 0)
        setLEDValue(ledValue: ledValue, true)
    }
}
