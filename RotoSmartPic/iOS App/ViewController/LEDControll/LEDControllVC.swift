//
//  LEDControllVC.swift
//  RotoSmartPic
//
//  Created by BAPS on 01/02/21.
//  Copyright © 2021 Bluepixel Technologies. All rights reserved.
//

import UIKit

class LEDControllVC: UIViewController {

    //MARK:-  Outlets And Variable Declarations
    @IBOutlet weak var btnConnectDisconnect : UIButton!
    @IBOutlet weak var btnConnectDisconnectTitle : UIButton!
    @IBOutlet weak var btnLEDControll : UIButton!
    
    @IBOutlet weak var collectionViewLEDControll: UICollectionView!
    @IBOutlet weak var HeightContstraintsOfcollectionView: NSLayoutConstraint!
    
    @IBOutlet weak var tblGroup: UITableView!
    @IBOutlet weak var HeightContstraintsOftblGroup: NSLayoutConstraint!
    
    @IBOutlet weak var txtGroup: UITextField!
    @IBOutlet weak var txtGroupName: UITextField!
    @IBOutlet weak var switchOnOff: UISwitch!
    @IBOutlet weak var viewOnOff : UIView!

    internal var arrOfGroup = [Group]()
    
    private var pickerGroup = UIPickerView()
    internal var selectedGroupIndex = 0
    public var isfirattime = false
    //MARK:- 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getAllLEDValues()
        getGroupSetting()
        setPicker()
        
        if self.isExistUserDefaultKey(USERDEFAULT.kIndividualSwitch) {
            userdef.bool(forKey: USERDEFAULT.kIndividualSwitch) ? self.switchOnOff.setOn(true, animated: true) : self.switchOnOff.setOn(false, animated: true)
            self.viewOnOff.isHidden = userdef.bool(forKey: USERDEFAULT.kIndividualSwitch)
            
        } else {
            
            self.switchOnOff.setOn(true, animated: true)
            self.viewOnOff.isHidden = userdef.bool(forKey: USERDEFAULT.kIndividualSwitch)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setConnectDisConnect()
    }
    //MARK:-  Buttons Clicked Actions
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnShareLogClickedClicked(_ sender: UIButton) {
        self.share(items: [logfile.getLogFile() as Any])
    }
    @IBAction func btnConnectDisConnectClicked(_ sender: UIButton) {
        
        if sender.isSelected {
            connectedLEDDevice?.disConnect(myComplition: { (isDisconnect) in
                if isDisconnect {
                    connectedLEDDevice = nil
                    self.setConnectDisConnect()
                }
            })
        } else {
            let deviceScanVC = storyboard?.instantiateViewController(withIdentifier: "DeviceScanVC") as! DeviceScanVC
            deviceScanVC.scanfor = .LEDControllDevice
            self.navigationController?.pushViewController(deviceScanVC, animated: true)
        }
    }
    @IBAction func btnSaveDataClicked(_ sender: UIButton) {
        
        var temparrOfGroup = [Group]()

        if let groups = (userdef.value(forKey: USERDEFAULT.kGroups) as? [Any]) {
            for groupdict in groups {
                temparrOfGroup.append(Group(dict: groupdict))
            }
        } else {
            
            let arrOfGroupName = ["Group-1", "Group-2", "Group-3", "Group-4", "1", "2", "3", "4", "5", "6"]
            
            for i in 0..<arrOfGroupName.count {
                let gp = Group(dict: [])
                gp.groupindex = i
                gp.groupName = arrOfGroupName[i]
                temparrOfGroup.append(gp)
            }
        }
        
        let selectedIndex = userdef.integer(forKey: USERDEFAULT.klastselectedGroupIndex)
        temparrOfGroup[selectedIndex] = arrOfGroup[selectedIndex]
        
        let setting = temparrOfGroup.map({$0.toDict()})
        logfile.appendNewText(text: "Save Settings: \(setting)")
        userdef.set(setting, forKey: USERDEFAULT.kGroups)
        userdef.synchronize()
//        Old Code
//        let setting = arrOfGroup.map({$0.toDict()})
//        logfile.appendNewText(text: "Save Settings: \(setting)")
//        userdef.set(setting, forKey: USERDEFAULT.kGroups)
    }
    
    @IBAction  func switchValueDidChange(sender: UISwitch!){
        
       // sender.isOn = !sender.isOn
        if sender.isOn {
            
            LEDDevice.LED(isON: true) { (enable, string) in
                
            }
            self.viewOnOff.isHidden = true
            userdef.set(true, forKey: USERDEFAULT.kIndividualSwitch)
            userdef.synchronize()
            print("on b")
        } else {
            
            LEDDevice.LED(isON: false) { (enable, string) in
                
            }
            self.viewOnOff.isHidden = false
            userdef.set(false, forKey: USERDEFAULT.kIndividualSwitch)
            userdef.synchronize()
            print("off m")
        }
    }

    
    //MARK:-  Functions
    private func getGroupSetting() {
        
        selectedGroupIndex = userdef.integer(forKey: USERDEFAULT.klastselectedGroupIndex)
        //Check if group one is aleready saved in userdef so we have get from it otherwise we use default values of group
        arrOfGroup.removeAll()
        if let groups = (userdef.value(forKey: USERDEFAULT.kGroups) as? [Any]) {
            for groupdict in groups {
                arrOfGroup.append(Group(dict: groupdict))
            }
        } else {
            
            let arrOfGroupName = ["Group-1", "Group-2", "Group-3", "Group-4", "1", "2", "3", "4", "5", "6"]
            
            for i in 0..<arrOfGroupName.count {
                let gp = Group(dict: [])
                gp.groupindex = i
                gp.groupName = arrOfGroupName[i]
                arrOfGroup.append(gp)
            }
        }
        tblGroup.layoutIfNeeded()
        tblGroup.reloadData()
        
        HeightContstraintsOftblGroup.constant = CGFloat((110 * arrOfGroup[selectedGroupIndex].arrOfGroupValues.count)) // 110 is fix height of each cell
        
        collectionViewLEDControll.layoutIfNeeded()
        collectionViewLEDControll.reloadData()
        
        HeightContstraintsOfcollectionView.constant = CGFloat((56 * (arrOfGroup[selectedGroupIndex].arrOfIdividualValues.count / 2))) + CGFloat((8 * (arrOfGroup[selectedGroupIndex].arrOfIdividualValues.count / 2))) //56 is height of collectionview cell and 6 is display cell in one side vertically And 8 is the spacing between two row
        
    }
    private func getAllLEDValues() {
    
        LEDDevice.getAllLEDValues { (isSuccess, message, arrOfLEDValues) in
            self.arrOfGroup[self.selectedGroupIndex].arrOfIdividualValues = arrOfLEDValues
            DispatchQueue.main.async {
                self.tblGroup.reloadData()
                self.collectionViewLEDControll.reloadData()
            }
            
        }
    }
    private func setConnectDisConnect() {
        
        DispatchQueue.main.async {
            
            if connectedLEDDevice != nil && connectedLEDDevice?.isConnected() ?? false {
                
                self.getAllLEDValues()
                self.btnLEDControll.isSelected = true
                self.btnConnectDisconnect.isSelected = true
                self.btnConnectDisconnectTitle.isSelected = true
                
            } else {
                
                self.btnLEDControll.isSelected = false
                self.btnConnectDisconnect.isSelected = false
                self.btnConnectDisconnectTitle.isSelected = false
            }
        }
    }
    private func setPicker() {
        
        pickerGroup.dataSource = self
        pickerGroup.delegate = self
        txtGroup.inputView = pickerGroup
        
        let group = arrOfGroup[selectedGroupIndex]
        txtGroup.text = group.groupName
        txtGroupName.text = group.groupName
    }
}

//MARK:-  UIPickerViewDataSource Methods
extension LEDControllVC : UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrOfGroup.count
    }
}

//MARK:-  UIPickerViewDelegate Methods
extension LEDControllVC : UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrOfGroup[row].groupName
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.txtGroup.text = arrOfGroup[row].groupName
        self.txtGroupName.text = self.txtGroup.text
        selectedGroupIndex = row
        userdef.set(selectedGroupIndex, forKey: USERDEFAULT.klastselectedGroupIndex)
        userdef.synchronize()
        self.tblGroup.reloadData()
        //self.collectionViewLEDControll.reloadData()
        
        for index in 0...arrOfGroup[selectedGroupIndex].arrOfIdividualValues.count - 1 {

            (collectionViewLEDControll.cellForItem(at: IndexPath(item: index, section: 0)) as! LEDControllCollectionViewCell).txtAngel.isUserInteractionEnabled = true
        }
        
        if let groups = (userdef.value(forKey: USERDEFAULT.kGroups) as? [Any]) {
                   var temparrOfGroup = [Group]()
       
                   for groupdict in groups {
                       temparrOfGroup.append(Group(dict: groupdict))
                   }
            
            for index in 0...temparrOfGroup[selectedGroupIndex].arrOfGroupValues.count - 1 {
                
                let groupNumber = temparrOfGroup[ selectedGroupIndex].arrOfGroupValues[index].groupNumber
                let groupValue = temparrOfGroup[ selectedGroupIndex].arrOfGroupValues[index].groupValue

                if groupNumber != ""{
                    arrOfGroup[selectedGroupIndex].arrOfGroupValues[index].groupNumber = groupNumber
                    arrOfGroup[selectedGroupIndex].arrOfGroupValues[index].groupValue = groupValue
                    tblGroup.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
                    
                    let IndindexValue = groupNumber.trime().components(separatedBy: ",")

                    for CollectionViewIndex in IndindexValue {
                        if CollectionViewIndex != "" {
                            arrOfGroup[selectedGroupIndex].arrOfIdividualValues[Int(CollectionViewIndex)! - 1] = groupValue
                            LEDDevice.shared.startCMDTimer(timeInterval: 0.01)
                            LEDDevice.shared.setLEDsValues(LEDIndexAndValues: [LEDIndexAndValue(index: Int(CollectionViewIndex)! - 1, value: groupValue)]) { (isSuccess, message) in
                                
                                if isSuccess {
                                    
                                } else {
                                    //self.showAlertWithOKButton(message: message)
                                }
                            }
                            collectionViewLEDControll.reloadItems(at: [IndexPath(row: Int(CollectionViewIndex)! - 1, section: 0)])
                        }
                    }
                    
                }else{
                    arrOfGroup[selectedGroupIndex].arrOfGroupValues[index].groupNumber = ""
                    arrOfGroup[selectedGroupIndex].arrOfGroupValues[index].groupValue = 0
                    tblGroup.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
                    
                    
                }
            }
        }else{
            for index in 0...arrOfGroup[selectedGroupIndex].arrOfGroupValues.count - 1 {

                arrOfGroup[selectedGroupIndex].arrOfGroupValues[index].groupNumber = ""
                arrOfGroup[selectedGroupIndex].arrOfGroupValues[index].groupValue = 0
                tblGroup.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
            }
        }        
    }
}

//MARK:-  UIPickerViewDataSource Methods
extension LEDControllVC : UITextFieldDelegate {
        
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtGroupName, let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            self.arrOfGroup[selectedGroupIndex].groupName = updatedText
            self.txtGroup.text = updatedText
        }
        return true
    }
}

//MARK:-  UICollectionViewDelegateFlowLayout Methods
extension LEDControllVC : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: ((collectionView.frame.width - 16) / 2), height: 56)
    }
}
//MARK:-  UICollectionViewDataSource Methods
extension LEDControllVC : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrOfGroup[selectedGroupIndex].arrOfIdividualValues.count
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LEDControllCollectionViewCell", for: indexPath) as! LEDControllCollectionViewCell
    
        //print("Grp:->\(arrOfGroup[selectedGroupIndex].groupName)")
        print("Grp Value:->\(arrOfGroup[selectedGroupIndex].arrOfIdividualValues[indexPath.row])")
    
        cell.configure(ledValue: arrOfGroup[selectedGroupIndex].arrOfIdividualValues[indexPath.row], parentVC: self, cellindexpath: indexPath, isEnable: arrOfGroup[selectedGroupIndex].arrOfIdividualValuesEnable[indexPath.row])
        
        if indexPath.row == arrOfGroup[selectedGroupIndex].arrOfIdividualValues.count - 1{
            if isfirattime{
                isfirattime = false
            }
        }
        return cell
    }
}

//MARK:-  UITableViewDataSource Methods
extension LEDControllVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfGroup[selectedGroupIndex].arrOfGroupValues.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LEDGroupsTableViewCell", for: indexPath) as! LEDGroupsTableViewCell
    
        cell.configure(parentVC: self, indexpath: indexPath)
        return cell
    }
}
