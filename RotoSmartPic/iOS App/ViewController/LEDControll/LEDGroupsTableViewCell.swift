//
//  LEDGroupsTableViewCell.swift
//  RotoSmartPic
//
//  Created by BAPS on 01/02/21.
//  Copyright © 2021 Bluepixel Technologies. All rights reserved.
//

import UIKit

class LEDGroupsTableViewCell: UITableViewCell {
    
    //MARK:-  Outlets And Variable Declarations
    @IBOutlet weak var lblGroupIndex: UILabel!
    @IBOutlet weak var txtGroupNo: UITextField!
    @IBOutlet weak var txtLEDValue: UITextField!
    
    private var currentValue = 0
    var parent: LEDControllVC!
    var indexPath: IndexPath!

    //MARK:- 
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        txtGroupNo.delegate = self
        txtLEDValue.delegate = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    //MARK:-  Buttons Clicked Actions
    @IBAction func btnMinusClicked(_ sender: UIButton) {
        if currentValue > 0 {
            setGroupValue(value: (currentValue - 1))
        }
    }
    @IBAction func btnPlusClicked(_ sender: UIButton) {
        if currentValue < 245 {
            setGroupValue(value: (currentValue + 1))
        }
    }

    //MARK:-  Functions
    func configure(parentVC: LEDControllVC, indexpath: IndexPath) {
        
        self.indexPath = indexpath
        parent = parentVC
        let group = parent.arrOfGroup[parent.selectedGroupIndex].arrOfGroupValues[indexPath.row]
        
        txtGroupNo.tag = indexpath.row
        txtLEDValue.tag = indexpath.row
        
        lblGroupIndex.text = "\(indexPath.row + 1)"
        print("Index path row + 1\(indexPath.row + 1)")
        txtGroupNo.text = group.groupNumber
        print("groupNumber: \(group.groupNumber)")
        txtLEDValue.text = String(group.groupValue)
        print("groupValue: \(group.groupValue)")
        self.currentValue = group.groupValue
//        setGroupValue(value: group.groupValue)
        self.enableDisableLightsValue(valueText: "\(group.groupNumber)")
        
        txtLEDValue.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(btnDoneClicked(sender:)))
    }
    
    @objc private func btnDoneClicked(sender: UIButton) {
 
    }
    
    private func setGroupValue(value: Int, _ isTextChanged: Bool = false) {
        
        if !isTextChanged {
            txtLEDValue.text = "\(value)"
        }
        parent.arrOfGroup[parent.selectedGroupIndex].arrOfGroupValues[indexPath.row].groupValue = value
        
        if !txtGroupNo.text!.trime().isEmpty {
            txtGroupNo.setNeedsDisplay()
            
            let groupNos = txtGroupNo.text!.trime().components(separatedBy: ",")
            
//            for i in 0..<parent.arrOfGroup[parent.selectedGroupIndex].arrOfIdividualValues.count {
//                if groupNos.contains("\(i + 1)") {
//                    parent.arrOfGroup[parent.selectedGroupIndex].arrOfIdividualValues[i] = value
//                } else {
//                    parent.arrOfGroup[parent.selectedGroupIndex].arrOfIdividualValues[i] = 0
//                }
//            }
        
            var arrOfLEDValues = [LEDIndexAndValue]()
            for groupNo in groupNos {
                let gn = Int(groupNo) ?? 0
                if gn > 0 {
                    parent.arrOfGroup[parent.selectedGroupIndex].arrOfIdividualValues[gn - 1] = value
//                    parent.arrOfGroup[parent.selectedGroupIndex].arrOfIdividualValuesEnable[gn - 1] = false
                    arrOfLEDValues.append(LEDIndexAndValue(index: (gn - 1), value: value))
                    let indexPath = IndexPath(item: gn - 1, section: 0)
                    parent.collectionViewLEDControll.reloadItems(at: [indexPath])
                }
            }
            
            //parent.collectionViewLEDControll.reloadData()
            LEDDevice.shared.startCMDTimer(timeInterval: 0.01)
            LEDDevice.shared.setLEDsValues(LEDIndexAndValues: arrOfLEDValues) { (isSuccess, message) in

                if isSuccess {

                } else {
                    self.parent.showAlertWithOKButton(message: message)
                }
            }
        }
        currentValue = value
    }
    
    fileprivate func enableDisableLightsValue(valueText : String) {
        
        var value = valueText
        
        var arrTotalNumber = self.parent.arrOfGroup[self.parent.selectedGroupIndex].arrTotalIdividualNumber
        
        if value.last == "," { value.removeLast() }
        
        let arrUpdatedText = value.trime().components(separatedBy: ",")
        
        arrTotalNumber = self.parent.arrOfGroup[parent.selectedGroupIndex].arrOfGroupValues.map({$0.groupNumber})//get all
        
        var tempArr:[String] = []
        
        for i in arrTotalNumber {
            if !i.isEmpty {
                let value = i.components(separatedBy: ",")
                tempArr.append(contentsOf: value)
            }
        }
        
        arrTotalNumber.removeAll()
        arrTotalNumber = tempArr
        
        //to add all value to arrTotalNumber
        for i in 0..<arrUpdatedText.count {
            
            if !arrTotalNumber.contains(arrUpdatedText[i]) {
                print("append at \(arrUpdatedText[i])")
                arrTotalNumber.append(arrUpdatedText[i])
            }else {
                
            }
        }
        
        self.parent.arrOfGroup[self.parent.selectedGroupIndex].arrTotalIdividualNumber = arrTotalNumber
        
        //to check arrOfIdividualValuesEnable is enable
        for i in 0..<self.parent.arrOfGroup[parent.selectedGroupIndex].arrOfIdividualValuesEnable.count {
            
            let temp = arrTotalNumber.filter({
                if ((Int($0) ?? 0) - 1) == i {
                    return true
                }else {
                    return false
                }
            }).first ?? ""
            
            if (Int(temp) ?? 0) - 1 == i {
                self.parent.arrOfGroup[parent.selectedGroupIndex].arrOfIdividualValuesEnable[i] = false
                
            }else {
                self.parent.arrOfGroup[parent.selectedGroupIndex].arrOfIdividualValuesEnable[i] = true
            }
        }
        
//        parent.collectionViewLEDControll.reloadData()
    }
}

//MARK:-  UITextFieldDelegate
extension LEDGroupsTableViewCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtGroupNo {
            
            let charSet = "0123456789,"
            print("New entered character \(string)")
            
            if charSet.contains(string) || string.count == 0 {
                
                if let text = textField.text,
                    let textRange = Range(range, in: text) {
                    
                    var updatedText = text.replacingCharacters(in: textRange, with: string)
                    
                    let angles = updatedText.components(separatedBy: ",")
                    
                    for a in angles {
                        let value = Int(a) ?? 0
                        if value > 12 { //because max is LED is 12
                            return false
                        }
                    }
                    self.parent.arrOfGroup[parent.selectedGroupIndex].arrOfGroupValues[textField.tag].groupNumber = updatedText
                    
                    self.enableDisableLightsValue(valueText: updatedText)
                    
                    setGroupValue(value: currentValue, true)
                }
            } else {
                return false
            }
            
        } else if textField == txtLEDValue  {
            
             if let text = textField.text,
                let textRange = Range(range, in: text) {
                let ledValue = Int(text.replacingCharacters(in: textRange, with: string)) ?? 0
                
                if ledValue > 255 {
                    return false
                }
                //setGroupValue(value: ledValue, true)
    
            } else {
                return false
            }
        }
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        print("textFieldDidEndEditing \(textField.text)")
        
        if textField == txtGroupNo {
            var updatedText = textField.text ?? ""
            
            let angles = updatedText.components(separatedBy: ",")
            
            self.parent.arrOfGroup[parent.selectedGroupIndex].arrOfGroupValues[textField.tag].groupNumber = updatedText
            
            self.enableDisableLightsValue(valueText: updatedText)
            
            setGroupValue(value: currentValue, true)
        }else if textField == txtLEDValue{
            let ledValue = Int(Double(textField.text ?? "0") ?? 0)
            setGroupValue(value: ledValue, true)
        }
    }
}
