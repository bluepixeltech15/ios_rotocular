//
//  CapturePhotoVideoVC.swift
//  RotoSmartPic
//
//  Created by Mac on 14/05/1942 Saka.
//  Copyright © 1942 Bluepixel Technologies. All rights reserved.
//

import UIKit
import AVFoundation
import CoreMotion
import AssetsLibrary

var isschedulestart = false

class CapturePhotoVideoVC: UIViewController {
    
    //--------------------------------------------------------
    //                  MARK: - Outlet -
    //--------------------------------------------------------
    @IBOutlet weak var btnConnectDisconnect : UIButton!
    @IBOutlet weak var btnConnectDisconnectTitle : UIButton!
    @IBOutlet weak var btnTurnTable : UIButton!
    @IBOutlet weak var viewAngel: AngleView!
    @IBOutlet weak var viewBrightness: UIView! {
        didSet {
            viewBrightness.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi / 2))
        }
    }
    @IBOutlet weak var sliderBrightness: UISlider!
    
    @IBOutlet weak var viewSpeedSlider: UIView!
//    {
//        didSet {
//            viewSpeedSlider.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi / 2))
//        }
//    }
    
    @IBOutlet weak var sliderSpeed: UISlider!

    
    @IBOutlet weak var viewSpeedSliderLeading: NSLayoutConstraint!
    
    @IBOutlet weak var viewCamera: UIView!
    @IBOutlet weak var lblPicCount : UILabel!
    
    @IBOutlet weak var txtGrids: UITextField!
    @IBOutlet weak var btnSingle : UIButton!
    @IBOutlet weak var bottomConstraintsOfViewCamera : NSLayoutConstraint!
    
    @IBOutlet weak var viewSpeed: UIView!
    @IBOutlet weak var stackViewClicksDelay: UIStackView!
    @IBOutlet weak var txtClicks: UITextField!
    @IBOutlet weak var btnCustom: UIButton!
    @IBOutlet weak var txtDelay: UITextField!
    
    @IBOutlet weak var viewModes: UIView!
    @IBOutlet weak var viewContinueDaley: UIView!
    @IBOutlet weak var viewVideoDelay: UIView!
    @IBOutlet weak var viewSettings: UIView!
    @IBOutlet weak var viewCapture: UIView!
    
    @IBOutlet weak var lblLine : UILabel!
    @IBOutlet weak var segmentPhotoVideo : UISegmentedControl!
    
    @IBOutlet  var btnAngelCollection : [UIButton]!
    @IBOutlet  var btnCWCollection : [UIButton]!
    
    @IBOutlet weak var svVideoFPS : UIStackView!
    @IBOutlet  var imgVideoFPS : [UIImageView]!
    
    @IBOutlet  var imgFocusCollection : [UIImageView]!
    @IBOutlet  var btnModesCollection : [UIButton]!
    @IBOutlet weak var txtVideoDelay: UITextField!
    
    //@IBOutlet weak var txtContinueDelay: UITextField!
    
    @IBOutlet  var btnPlays : [UIButton]!
    
    @IBOutlet weak var txtSpeed: UITextField!
    @IBOutlet weak var btnDiamond: UIButton!

    @IBOutlet weak var viewscheduling: UIView!
    @IBOutlet weak var lblScheduling: UILabel!
    
    @IBOutlet weak var imgDimond : UIImageView!
    
    private var captureSession: AVCaptureSession!
    private var capturePhotoOutput: AVCapturePhotoOutput!
    
    private var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    private var cameraDevice: AVCaptureDevice!
    
    private var viewGrid = UIView()
    private var motionManager = CMMotionManager()
    
    private var pinch = UIPinchGestureRecognizer()
    
    private let xSpacing : CGFloat = 32.0
    private var ySpacing : CGFloat = 50.0
    
    //Zoom
    private var pinchGesture = UIPinchGestureRecognizer()
    
    //Zoom
    private var initialScale: CGFloat = 0
    private var zoomScaleRange: ClosedRange<CGFloat> = 1...10
    private var sessionSetupSucceeds = true // false put default false
    //Put true in will appear and false in disapper
    //    sessionSetupSucceeds = true
    //    sessionSetupSucceeds = false
    
    //Video
    var previewLayer = AVCaptureVideoPreviewLayer()
    var movieOutput = AVCaptureMovieFileOutput()

    private var timerForUpdateDevice: Timer?
    private var captureType: CaptureType = .Photo
    private var angleType: AngleTypes = .Single
    private var angleTypelist : [AngleTypes] = []
    private var mode: cameraMode = .None
    private var playType: [PlayType] = [.None]
//    private var videoFPSSetting: VideoFPS = .Sixty
    
    private var picker = UIPickerView()
    
    private var selectedTextField = UITextField()
    private let kSet = "Set"
    private var arrOfClicks = ["Set", "2", "4", "8", "12", "24", "36", "72"]
    private var arrOfGrids: [GridType] = [.None, .Grid2x2, .Grid3x3, .CircleGrid]
    
    private var isForword = true
    
    private var isStartPhotoAndVideoObserving = false
    private var isCapturePhoto = true
    private var picCount = 0
    
    private var currentObservType:ObserveType = .None
    private var lastRealAngle = 0
    private var previousAngle = 0
    
    private var lastDeviceResponse = ResponseModel()
    private var customAngles = [Int]()
    
    private var stopVideoRunnable : Timer?
    private var isStopCallback : Bool = false

    //private var stopContinueRunnable : Timer?
    //private var isContinue : Bool = false
   // private var isContinuePhoto : Bool = false
    
    //--------------------------------------------------------
    //                  MARK: -Property -
    //--------------------------------------------------------
    
    //--------------------------------------------------------
    //                  MARK: - View Life Cycle -
    //--------------------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSetup()
        self.btnDiamond.isHidden = true
        self.imgDimond.isHidden = true
        self.imgDimond.tintColor = .white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.authorisationStatus(attachmentTypeEnum: .camera) { ispermition in

        }
        self.authorisationStatus(attachmentTypeEnum: .photoLibrary) { ispermition in

        }
        self.authorisationStatus(attachmentTypeEnum: .video) { ispermition in

        }
        
//        self.askForPermision(forType: .camera) { ispermition in
//            //data
//        }
//        self.askForPermision(forType: .photoLibrary) { ispermition in
//            
//        }
//        self.askForPermision(forType: .video) { ispermition in
//            
//        }
        
        self.viewModes.isHidden = true
//        self.viewContinueDaley.isHidden = true
//        self.viewVideoDelay.isHidden = true
        self.setConnectDisConnect()
        self.checkIsableToStart()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.pinch.addTarget(self, action: #selector(handlePinch(_:)))
        viewCamera.addGestureRecognizer(self.pinch)
        
        settingForVideoPhoto()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.stopGettingDeviceUpdate()
        self.captureSession.stopRunning()
        
    }
    //--------------------------------------------------------
    //                  MARK: -  Button Action -
    //--------------------------------------------------------
    @IBAction func btnBackClicked(_ sender: UIButton) {
//        self.navigationController?.popViewController(animated: true)
        self.showAlertWith2Buttons(message: "Are you sure you want to exit?", btnOneName: "CANCEL", btnTwoName: "OK") { btnAction in
            if btnAction == 2 {
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    UIApplication.shared.perform(#selector(NSXPCConnection.suspend))
                }
            }
        }
        
    }
    @IBAction func btnSettingClicked(_ sender: UIButton) {
        
        let settingsVC = storyboard?.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        self.navigationController?.pushViewController(settingsVC, animated: true)
    }
    
    @IBAction func onBtnOpen(_ sender: UIButton) {
        
        let customSettingsVC = storyboard?.instantiateViewController(withIdentifier: "CustomSettingsVC") as! CustomSettingsVC
        self.present(VC: customSettingsVC)
    }
    
    @IBAction func onBtnDiamond(_ sender: UIButton) {
        
        let customSettingsVC = storyboard?.instantiateViewController(withIdentifier: "CustomSettingsVC") as! CustomSettingsVC
        customSettingsVC.callBack { isOkay in
            if isOkay{
                for btn in self.btnAngelCollection {
                    btn.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
                }
                self.btnDiamond.isSelected = true
                self.imgDimond.tintColor = #colorLiteral(red: 0, green: 0.6524586678, blue: 0.6893625855, alpha: 1)
                
                
                self.playType = [.Start]
                self.setSelectedPlay()
            }else{
                self.btnDiamond.isSelected = false
                self.imgDimond.tintColor =  #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
        }
        self.present(VC: customSettingsVC)
    }
    
    @IBAction func btnConnectDisConnectClicked(_ sender: UIButton) {
        
        if sender.isSelected {
            connectedBLEDevice?.disConnect(myComplition: { (isDisconnect) in
                if isDisconnect {
                    connectedBLEDevice = nil
                    self.setConnectDisConnect()
                }
            })
            
        } else {
            let deviceScanVC = storyboard?.instantiateViewController(withIdentifier: "DeviceScanVC") as! DeviceScanVC
            self.navigationController?.pushViewController(deviceScanVC, animated: true)
        }
    }
    @IBAction func btnCaptureClicked(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.4) {
            self.lblLine.frame.origin.x = sender.frame.origin.x
            self.viewCapture.isHidden = false
        }
    }
    
    @IBAction func btnEditClicked(_ sender: UIButton) {
        
        let editPhotoVideoVC = storyboard?.instantiateViewController(withIdentifier: "EditPhotoVideoVC") as! EditPhotoVideoVC
        self.navigationController?.pushViewController(editPhotoVideoVC, animated: true)
    }
    
    @IBAction func sliderBrightnessVaueChange(_ sender: UISlider) {
        setupCameraBrightness(value: sender.value)
    }
    
    @objc func onSliderValChanged(slider: UISlider, event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began:
                // handle drag began
                break
            case .moved:
                // handle drag moved
                break
            case .ended:
                userdef.set("\(slider.value)", forKey: USERDEFAULT.kSpeed)
                break
            default:
                break
            }
        }
    }

    
    @IBAction func btnVideoFPSClicked(_ sender: UIButton) {
        
        for img in imgVideoFPS {
            if img.tag == sender.tag {
                img.image = UIImage(named: "ic_Radio_Selected")
            } else {
                img.image = UIImage(named: "ic_Radio_Unselected")
            }
        }
        
        if sender.tag == 10 {
//            self.videoFPSSetting = .Thirty
            
        } else {
//            self.videoFPSSetting = .Sixty
        }
    }
    
    @IBAction func btnFocusClicked(_ sender: UIButton) {
        
        for img in imgFocusCollection {
            if img.tag == sender.tag {
                img.image = UIImage(named: "ic_Radio_Selected")
            } else {
                img.image = UIImage(named: "ic_Radio_Unselected")
            }
        }
        
        try! cameraDevice.lockForConfiguration()
        if sender.tag == 0 {
            self.viewCamera.gestureRecognizers?.removeAll()
            self.viewCamera.addGestureRecognizer(pinch)
            cameraDevice.focusMode = .continuousAutoFocus
        } else {
            self.setManualFocusGesture()
            cameraDevice.focusMode = .locked
        }
        cameraDevice.unlockForConfiguration()
        
        viewCamera.isUserInteractionEnabled = true
    }
    @IBAction func btnCustomClicked(_ sender: UIButton) {
        
        if txtClicks.text!.trime().lowercased() != kSet.lowercased() {
            self.showAlertWithOKButton(message: "Please unselect no .of Cllicks")
            return
        }
        
        let customVC = storyboard?.instantiateViewController(withIdentifier: "CustomAngleVC") as! CustomAngleVC
        customVC.isOkClicked { (isOkClicked, angles)  in
            
            if isOkClicked {
                
                self.btnCustom.isSelected = true
                self.btnCustom.backgroundColor = #colorLiteral(red: 0, green: 0.6524586678, blue: 0.6893625855, alpha: 1)
                self.btnCustom.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .selected)
                
                self.customAngles = angles!
                
            } else {
                
                self.btnCustom.isSelected = false
                self.btnCustom.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                self.btnCustom.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                self.customAngles = [Int]()
                
            }
            self.checkIsableToStart()
        }
        self.present(VC: customVC)
    }
    @IBAction func btnLEDControlllicked(_ sender:UIButton) {
        
        let lEDControllVC = storyboard?.instantiateViewController(withIdentifier: "LEDControllVC") as! LEDControllVC
        lEDControllVC.isfirattime = true
        self.navigationController?.pushViewController(lEDControllVC, animated: true)
    }
    @IBAction func btnCWCCClicked(_ sender:UIButton) {
        
        for btn in btnCWCollection {
            
            if btn.tag == sender.tag {
                btn.backgroundColor = #colorLiteral(red: 0, green: 0.6524586678, blue: 0.6893625855, alpha: 1)
                btn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
                
            } else {
                btn.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                btn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            }
        }
        isForword = sender.tag == 0
    }
    
    
    @IBAction func btnModesClicked(_ sender: UIButton) {
        
        for btn in btnModesCollection {
            
            if btn.tag == sender.tag {
                btn.backgroundColor = #colorLiteral(red: 0, green: 0.6524586678, blue: 0.6893625855, alpha: 1)
                btn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
                
            } else {
                btn.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                btn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            }
        }
        
        mode = cameraMode(rawValue: sender.tag) ?? cameraMode.None
        
        if mode == .Eaching {
            angleType = .P360
            checkIsableToStart()
            
        } else if mode == .Home {
            
            stopGettingDeviceUpdate()
            var homeAngel = lastDeviceResponse.realAngle
            var isForwordGo = false
            if (homeAngel > 180) {
                homeAngel = 360 - homeAngel
                isForwordGo = true
            }
            
            writeAngel(value: homeAngel) {
                
//                self.makedelay()
                self.writeStart(mode: .ManualMode, isForword: isForwordGo) {
                    self.startGettingDeviceUpdate()
                }
            }
            
            self.playType = [.Stop]
            self.setSelectedPlay()
            self.viewSettings.isHidden = true
            
        } else if mode == .Manual {
            
            let manualAngleVC = storyboard?.instantiateViewController(withIdentifier: "ManualAngleVC") as! ManualAngleVC
            
            manualAngleVC.isOkClicked { (isOkClicked, angle) in
                
                if isOkClicked {
                    
                    self.stopGettingDeviceUpdate()
                    let speed = Int(self.txtSpeed.text!.trime()) ?? 10
//                    let speed = Int(Float(userdef.string(forKey: USERDEFAULT.kSpeed) ?? "10") ?? 10)
                    self.writeSpeed(value: speed) {
                        
                        //self.makedelay()
                        self.writeAngel(value: angle!) {
                            
                            //self.makedelay()
                            self.writeStart(mode: .ManualMode, isForword: self.isForword) {
                                self.startGettingDeviceUpdate()
                            }
                        }
                    }
                    
                    self.playType = [.Stop]
                    self.setSelectedPlay()
                    
                    self.lastRealAngle = self.lastDeviceResponse.realAngle
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.mode = .Manual
                        self.setSelectedMode()
                    }
                    self.viewSettings.isHidden = true
                    
                } else {
                    self.mode = .None
                    self.setSelectedMode()
                }
            }
            
            self.navigationController?.present(manualAngleVC, animated: true, completion: nil)
        } else if mode == .Continue {
            //self.isContinue = true
            checkIsableToStart()
        }
        setSelectedAngle()
    }
    @IBAction func btnPhotoVideoSettingClicked(_ sender: UIButton) {
        
        if captureType == .Photo && angleType != .Single {
            stackViewClicksDelay.isHidden = !stackViewClicksDelay.isHidden
            viewModes.isHidden = !stackViewClicksDelay.isHidden
//            self.viewContinueDaley.isHidden = !stackViewClicksDelay.isHidden
//            self.viewVideoDelay.isHidden = !stackViewClicksDelay.isHidden
            
        } else if captureType == .Photo {
            viewModes.isHidden = !viewModes.isHidden
//            self.viewContinueDaley.isHidden = viewModes.isHidden
//            self.viewVideoDelay.isHidden = viewModes.isHidden
            
        } else if captureType == .Video {
            viewModes.isHidden = !viewModes.isHidden
//            self.viewContinueDaley.isHidden = viewModes.isHidden
//            self.viewVideoDelay.isHidden = viewModes.isHidden
        }
        
    }
    @IBAction func segmentPhotVideoValueChanged(_ sender: UISegmentedControl) {
        
        viewModes.isHidden = true
//        self.viewContinueDaley.isHidden = true
//        self.viewVideoDelay.isHidden = true
        stackViewClicksDelay.isHidden = true
        
        mode = .None
        setSelectedMode()
        
        for btn in btnAngelCollection {
            btn.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        }
        
        if sender.selectedSegmentIndex == 1 {
            
            btnSingle.isHidden = true
            self.btnDiamond.isHidden = false
            self.imgDimond.isHidden = false
            captureType = .Video
            captureSession.sessionPreset = .high
            let diff = viewCamera.frame.height - viewCamera.frame.width
            bottomConstraintsOfViewCamera.constant = diff
            
            for output in captureSession.outputs {
                if output == movieOutput {
                    captureSession.removeOutput(movieOutput)
                }
            }
            angleType = .Single
            movieOutput = AVCaptureMovieFileOutput()
            DispatchQueue.main.async {
                self.captureSession.addOutput(self.movieOutput)
            }
            //60 fpps
//            btnSingle.isHidden = true
//            self.btnDiamond.isHidden = false
//            self.imgDimond.isHidden = false
//            captureType = .Video
//            captureSession.sessionPreset = .high
//            let diff = viewCamera.frame.height - viewCamera.frame.width
//            bottomConstraintsOfViewCamera.constant = diff
//
//            for output in captureSession.outputs {
//                if output == movieOutput {
//                    captureSession.removeOutput(movieOutput)
//                }
//            }
//            angleType = .Single
//            movieOutput = AVCaptureMovieFileOutput()
//
////            let movieFileOutput = movieOutput
////            let connection = movieFileOutput.connection(with: .video)
////            if movieFileOutput.availableVideoCodecTypes.contains(.h264) {
////                // Use the H.264 codec to encode the video.
////                movieFileOutput.setOutputSettings([AVVideoCodecKey: AVVideoCodecType.h264], for: connection!)
////            }
////            movieOutput = movieFileOutput
////            DispatchQueue.main.async {
////                if let device = self.cameraDevice {
////
////                        // 1
////                    for vFormat in self.cameraDevice!.formats {
////
////                            //                        if let vFormat = self.cameraDevice!.formats.last {
////                        let ranges = vFormat.videoSupportedFrameRateRanges as [AVFrameRateRange]
////                        if ranges.indices.contains(0) {
////                            let frameRates = ranges[0]
////
////                                // 3
////                            if frameRates.maxFrameRate == Double(self.videoFPSSetting.rawValue) {
////
////                                    // 4
////                                DispatchQueue.main.async {
////                                    do {
////
////                                        try device.lockForConfiguration()
////                                        device.activeFormat = vFormat as AVCaptureDevice.Format
////                                        device.activeVideoMinFrameDuration = frameRates.minFrameDuration
////                                        device.activeVideoMaxFrameDuration = frameRates.maxFrameDuration
////                                        device.unlockForConfiguration()
////
////                                    } catch {
////                                        print(error.localizedDescription)
////                                    }
////                                }
////                            }
////                                //                        }
////                        } else {
////                        }
////                    }
////                }
//
////            }
//            self.configureCameraForHighestFrameRate(device: self.cameraDevice, maxFramRate: self.videoFPSSetting.rawValue)
//            self.cameraDevice.set(frameRate: Double(self.videoFPSSetting.rawValue))
//
//            DispatchQueue.main.async {
//                self.captureSession.addOutput(self.movieOutput)
//            }
//
        } else {
            captureSession.sessionPreset = .photo
            bottomConstraintsOfViewCamera.constant = 0
            btnSingle.isHidden = false
            self.btnDiamond.isHidden = true
            self.imgDimond.isHidden = true
            captureType = .Photo
        }
        checkIsableToStart()
    }
    
    @IBAction func btnSingleClicked(_ sender: UIButton) {

//        fatalError()
        
        //For giving backgrouncolor effect
        let lastBackgroundColor = sender.backgroundColor
        sender.backgroundColor = .lightGray
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
            sender.backgroundColor = lastBackgroundColor
        }
        
        let lastAngleType = angleType
        angleType = .Single
        takePhoto()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { //Because take photo take some time so its photo store in lastAngleType
            self.angleType = lastAngleType
            self.checkIsableToStart()
        }
    }
    @IBAction func btnAnglesClicked(_ sender: UIButton) {
        
        btnDiamond.isSelected = false
        self.imgDimond.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
 
        if mode == .Eaching {
            return
        }
        
        if captureType == .Photo {
            stackViewClicksDelay.isHidden = false
            viewModes.isHidden = true
//            self.viewContinueDaley.isHidden = false
//            self.viewVideoDelay.isHidden = true
        } else {
            stackViewClicksDelay.isHidden = true
            viewModes.isHidden = false
//            self.viewContinueDaley.isHidden = false
//            self.viewVideoDelay.isHidden = false
        }
        
        for btn in btnAngelCollection {
            if btn.tag == sender.tag {
                btn.setTitleColor(#colorLiteral(red: 0, green: 0.6524586678, blue: 0.6893625855, alpha: 1), for: .normal)
            } else {
                btn.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            }
        }
        angleType = AngleTypes(rawValue: (sender.titleLabel?.text!.trime())!)!
        
        if angleType == .P45 && txtClicks.text!.trime().lowercased() == "2".lowercased() {
            txtClicks.text = "4"
        }
        self.checkIsableToStart()
    }
    @IBAction func btnStartDownClicked(_ sender: UIButton) {
        
        print("btn Start Down Clicked")
        if mode == .Eaching {
            
            self.stopGettingDeviceUpdate()
//            let speed = Int(Float(userdef.string(forKey: USERDEFAULT.kSpeed) ?? "10") ?? 10)
            self.writeSpeed(value: Int(txtSpeed.text!.trime()) ?? 10) {
//            self.writeSpeed(value: speed) {
                self.writeStart(mode: .EachingMode, isForword: self.isForword) {
                    self.startGettingDeviceUpdate()
                }
            }
            self.viewSettings.isHidden = true
        }
    }
    @IBAction func btnPlayClicked(_ sender: UIButton) {
        
        if !sender.isSelected {
            return
        }
        
        playType = [PlayType(rawValue: sender.tag) ?? .None]
        let speed = Int(txtSpeed.text!.trime()) ?? 10
        //let speed = Int(Float(userdef.string(forKey: USERDEFAULT.kSpeed) ?? "1") ?? 1)
        var delay = Int(txtDelay.text!.trime()) ?? 2
        
        switch self.playType.last {
        case .Start:
            print("btn Start Touch UP Clicked")
            delay = delay < 3 ? 3:delay
            self.stopGettingDeviceUpdate()
            if self.mode == .Eaching {
                
                print("btn Start UP Clicked with mode Eaching")
                self.writeStop(mode: .EachingMode, isForword: self.isForword) {
                    self.startGettingDeviceUpdate()
                }
                stopUIForModeAndBtnPlays()
                self.viewSettings.isHidden = false
               
                return
                
            } else if mode == .Continue {
                
               // self.isContinuePhoto = true
                self.stopGettingDeviceUpdate()
                //let speed = Int(Float(userdef.string(forKey: USERDEFAULT.kSpeed) ?? "1") ?? 1)
                self.writeDelay(value: Int(userdef.string(forKey: USERDEFAULT.kContinueDelay) ?? "1") ?? 1 , commandType: .delayincontmode) {
                    self.writeSpeed(value: Int(self.txtSpeed.text!.trime()) ?? 10) {
                    //self.writeSpeed(value: speed) {
                        
                        self.writeAngel(value: self.angleType.getIntAngle()) {
                            self.writeStart(mode: .ContinuosDelayMode, isForword: self.isForword) {
                                self.startGettingDeviceUpdate()
                            }
                        }
                    }
                }
                self.playType = [.Stop]
                self.setSelectedPlay()
                //stopUIForModeAndBtnPlays()
                self.viewSettings.isHidden = true
                
            } else if self.btnCustom.isSelected {
                
                self.writeDelay(value: delay) {
                    
                    //                    self.makedelay()
                    self.writeSpeed(value: speed) {
                        
                        //                        self.makedelay()
                        self.writeCustomAngel(values: self.customAngles) {
                            
                            //                            self.makedelay()
                            self.writeStart(mode: .CustomMode, isForword: true) {
                                
                                self.startGettingDeviceUpdate()
                                self.takePhoto()
                                DispatchQueue.main.asyncAfter(deadline: .now() + Double(delay)) {
                                    self.isStartPhotoAndVideoObserving = true
                                }
                            }
                        }
                    }
                }
                
                
                self.playType = [.Stop]
                self.setSelectedPlay()
                self.lastRealAngle = self.lastDeviceResponse.realAngle
                
            } else if btnDiamond.isSelected {
                
                isschedulestart = true
                
                self.viewscheduling.isHidden = false
                
                let arrOfDictProfile = userdef.value(forKey: USERDEFAULT.kSelectedProfile) as! [[String: Any]]
                var values = arrOfDictProfile[currentSelectedProfile - 1]["values"] as! [String]
                let scheduletext = (arrOfDictProfile[currentSelectedProfile - 1]["values"] as? [String])?.joined(separator: ", ")
                self.lblScheduling.text = "Sch: " + (scheduletext ?? "")
                self.angleTypelist = []
                var angleValue = [UInt8]()
                
//                for item in values  {
//                    //                    arrOfDictProfile[currentSelectedProfile - 1]["values"] = values
//                    var data = item ?? ""
//
//                    var strstart = (data.first == "c" ? "1" : "0") + "\(data.first!)"
////                    angleTypelist.append(AngleTypes(rawValue: (strstart))!)
//
//                    angleValue.append(contentsOf: (AngleTypes(rawValue: (strstart))!).getIntAngle().intToByteArray(length: 1))
//                    data.removeFirst()
//                    angleValue.append(contentsOf: (Int(data) ?? 0)!.intToByteArray(length: 2))
//
//                }
//                angleValue.insert(contentsOf: (values.count).intToByteArray(length: 1), at: 0)
                
                
                
                let clicks = Int(self.txtClicks.text!.trime()) ?? 2
                
                self.angleType = .P360
                
                self.writeDelay(value: delay) {
                    
                    self.writeSpeed(value: speed) {
                        
                        //self.makedelay()
                        
                        angleValue.append(contentsOf: 0.intToByteArray(length: 1 ))
                        angleValue.append(contentsOf: (values.count).intToByteArray(length: 1))
                        print("Mode = SchedulingAngel , dataValue = \(Date().timeIntervalSince1970)")
                        
                        self.writeAngelSchedule(value: 0 , commandtype : .SchedulingAngel , valuearr : angleValue ) {

//                            self.startGettingDeviceUpdate()
//                            if !ispreviewselected {
//                                self.startVideoTaking()
//                            }
//
//                            DispatchQueue.main.asyncAfter(deadline: .now() + Double(delay)) {
//                                self.isStartPhotoAndVideoObserving = true
//                            }
                        }
                            
                        var itemcount = 0
                        
                        fnctimecount()
                        
                        func fnctimecount() {
                            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                              
                                print("Mode = SchedulingAngel , dataValue = \(Date().timeIntervalSince1970)")
                                    var data = values[itemcount] ?? ""
                                    
                                    angleValue.removeAll()
                                    
                                    angleValue.append(contentsOf: (itemcount).intToByteArray(length: 1))
                                    var strstart = (data.first == "c" ? "1" : "0") + "\(data.first!)"
                                    //                    angleTypelist.append(AngleTypes(rawValue: (strstart))!)
                                    
                                    angleValue.append(contentsOf: (AngleTypes(rawValue: (strstart))!).getIntAngle().intToByteArray(length: 1))
                                    
                                    self.writeAngelSchedule (value: 0 , commandtype : .SchedulingAngel , valuearr : angleValue ) {
//                                        self.startGettingDeviceUpdate()
//                                        if !ispreviewselected {
//                                            self.startVideoTaking()
//                                        }
//
//                                        DispatchQueue.main.asyncAfter(deadline: .now() + Double(delay)) {
//                                            self.isStartPhotoAndVideoObserving = true
//                                        }
                                    }
                                    
                                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                                    
                                    print("Mode = SchedulingAngel , dataValue = \(Date().timeIntervalSince1970)")
                                    angleValue.removeAll()
                                    data.removeFirst()
                                    angleValue.append(contentsOf: (Int(data) ?? 0)!.intToByteArray(length: 2))
                                    self.writeAngelSchedule (value: 0 , commandtype : .SchedulingAngel , valuearr : angleValue ) {
//                                        self.startGettingDeviceUpdate()
//                                        if !ispreviewselected {
//                                            self.startVideoTaking()
//                                        }
//
//                                        DispatchQueue.main.asyncAfter(deadline: .now() + Double(delay)) {
//                                            self.isStartPhotoAndVideoObserving = true
//                                        }
                                    }
                                }
                                    
                                itemcount = itemcount + 1
                                
                                if itemcount != values.count {
                                        fnctimecount()
                                
                                }else{
                                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
                                        print("Mode = SchedulingAngel , dataValue = \(Date().timeIntervalSince1970)")
                                        angleValue.removeAll()
                                        angleValue.append(contentsOf: (0).intToByteArray(length: 1))
                                        angleValue.append(CommandType.start.rawValue)
                                        self.writeAngelSchedule (value: 0 , commandtype : .SchedulingAngel , valuearr : angleValue ) {
                                            
                                            isschedulestart = false
                                            
                                            self.startGettingDeviceUpdate()
                                            if !ispreviewselected {
                                                self.startVideoTaking()
                                            }
                                            
                                            DispatchQueue.main.asyncAfter(deadline: .now() + Double(delay)) {
                                                self.isStartPhotoAndVideoObserving = true
                                            }
                                        }
                                    }
                                }
                            }
                        }
                            
                            
                            //old
//                        var itemcount = 0
//
//                        for item in values  {
//
//                            var data = item ?? ""
//
//                            angleValue.removeAll()
//
//                            angleValue.append(contentsOf: (itemcount).intToByteArray(length: 1))
//                            var strstart = (data.first == "c" ? "1" : "0") + "\(data.first!)"
//                            //                    angleTypelist.append(AngleTypes(rawValue: (strstart))!)
//
//                            angleValue.append(contentsOf: (AngleTypes(rawValue: (strstart))!).getIntAngle().intToByteArray(length: 1))
//
//                            self.writeAngelSchedule (value: 0 , commandtype : .SchedulingAngel , valuearr : angleValue ) {
//                                self.startGettingDeviceUpdate()
//                                if !ispreviewselected {
//                                    self.startVideoTaking()
//                                }
//
//                                DispatchQueue.main.asyncAfter(deadline: .now() + Double(delay)) {
//                                    self.isStartPhotoAndVideoObserving = true
//                                }
//                            }
//
//                            angleValue.removeAll()
//                            data.removeFirst()
//                            angleValue.append(contentsOf: (Int(data) ?? 0)!.intToByteArray(length: 2))
//                            self.writeAngelSchedule (value: 0 , commandtype : .SchedulingAngel , valuearr : angleValue ) {
//                                self.startGettingDeviceUpdate()
//                                if !ispreviewselected {
//                                    self.startVideoTaking()
//                                }
//
//                                DispatchQueue.main.asyncAfter(deadline: .now() + Double(delay)) {
//                                    self.isStartPhotoAndVideoObserving = true
//                                }
//                            }
//
//                            itemcount = itemcount + 1
//                            //                    //                    arrOfDictProfile[currentSelectedProfile - 1]["values"] = values
//                            //                    var data = item ?? ""
//                            //
//                            //                    var strstart = (data.first == "c" ? "1" : "0") + "\(data.first!)"
//                            ////                    angleTypelist.append(AngleTypes(rawValue: (strstart))!)
//                            //
//                            //                    angleValue.append(contentsOf: (AngleTypes(rawValue: (strstart))!).getIntAngle().intToByteArray(length: 1))
//                            //                    data.removeFirst()
//                            //                    angleValue.append(contentsOf: (Int(data) ?? 0)!.intToByteArray(length: 2))
//
//                        }
                        
//                        angleValue.removeAll()
//                        angleValue.append(contentsOf: (0).intToByteArray(length: 1))
//                        angleValue.append(CommandType.start.rawValue)
//                        self.writeAngelSchedule (value: 0 , commandtype : .SchedulingAngel , valuearr : angleValue ) {
//
//                            self.startGettingDeviceUpdate()
//                            if !ispreviewselected {
//                                self.startVideoTaking()
//                            }
//
//                            DispatchQueue.main.asyncAfter(deadline: .now() + Double(delay)) {
//                                self.isStartPhotoAndVideoObserving = true
//                            }
//                        }

                    
//                        self.writeAngelSchedule (value: 0 , commandtype : .SchedulingAngel , valuearr : angleValue ) {
//
//                            self.startGettingDeviceUpdate()
//                            if !ispreviewselected {
//                                self.startVideoTaking()
//                            }
//
//                            DispatchQueue.main.asyncAfter(deadline: .now() + Double(delay)) {
//                                self.isStartPhotoAndVideoObserving = true
//                            }
////                        }
                    }
                }
                self.playType = [.Stop]
                self.setSelectedPlay()
                self.lastRealAngle = self.lastDeviceResponse.realAngle
            } else {
                
                let angleValue = self.angleType.getIntAngle()
               
                let clicks = Int(self.txtClicks.text!.trime()) ?? 2
                
                self.writeDelay(value: delay) {
                    
                    self.writeSpeed(value: speed) {
                        
                        //self.makedelay()
                        self.writeAngel(value: angleValue) {
                            
                            //                        makedelay()
                            if angleValue <= 90 {
                                
                                if self.captureType == .Photo {
                                    
                                    self.writeNoOfStop(value: clicks / 2) {
                                        
                                        //self.makedelay()
                                        self.writeStart(mode: .SingleDelayMode, isForword: false) {
                                            self.startGettingDeviceUpdate()
                                            //self.takePhoto()//Do comment this line bz its take twise photo on initial stage
                                            DispatchQueue.main.asyncAfter(deadline: .now() + Double(delay)) {
                                                self.isStartPhotoAndVideoObserving = true
                                                self.currentObservType = .first
                                            }
                                        }
                                        
                                    }
                                } else {
                                    
                                    self.writeNoOfStop(value: clicks / 2) {
                                        
                                        //self.makedelay()
                                        self.writeDelay(value: 1) {
                                            
                                            //                                      self.makedelay()
                                            self.writeStartVideoP(angle: self.angleType) {
                                                
                                                self.startGettingDeviceUpdate()
                                                self.startVideoTaking()
                                    
                                                DispatchQueue.main.asyncAfter(deadline: .now() + Double(delay)) {
                                                    self.isStartPhotoAndVideoObserving = true
                                                    
                                                }
                                            }
                                        }
                                    }
                                }
                                
                            } else {
                                
                                if self.captureType == .Photo {
                                    
                                    self.writeNoOfStop(value: clicks) {
                                        
                                        //self.makedelay()
                                        self.writeStart(mode: .SingleDelayMode, isForword: self.isForword) {
                                            
                                            self.startGettingDeviceUpdate()
                                            self.takePhoto()
                                            DispatchQueue.main.asyncAfter(deadline: .now() + Double(delay)) {
                                                self.isStartPhotoAndVideoObserving = true
                                            }
                                        }
                                    }
                                } else {
                                    
                                    self.writeNoOfStop(value: clicks) {
                                        //                                    self.makedelay()
                                        self.writeDelay(value: 1) {
                                            
                                            //                                     self.makedelay()
                                            self.writeStart(mode: .ManualMode, isForword: self.isForword) {
                                                
                                                self.startGettingDeviceUpdate()
                                                self.startVideoTaking()
                                                
                                                DispatchQueue.main.asyncAfter(deadline: .now() + Double(delay)) {
                                                    self.isStartPhotoAndVideoObserving = true
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                self.playType = [.Stop]
                self.setSelectedPlay()
                self.lastRealAngle = self.lastDeviceResponse.realAngle
            }
            self.viewSettings.isHidden = true
            break
            
        case .Stop:
            
            if self.mode == .Eaching {
                return
                
            } else if self.mode == .Manual || self.mode == .Home || self.mode == .Continue {
                //self.isContinue = false
                self.writeStop(mode: .ManualMode, isForword: self.isForword) {
                    
                }
                
            } else if self.btnCustom.isSelected {
                
                self.writeStop(mode: .CustomMode, isForword: false) {
                    
                }
                
            } else {
                
                if self.angleType.getIntAngle() <= 90 { // For 45 and 90 angle
                    if self.captureType == .Photo {
                        
                        self.writeStop(mode: .SingleDelayMode, isForword: false) {
                            
                        }
                        
                    } else {
                        self.writeStop(mode: .ManualMode, isForword: self.isForword) {
                            
                        }
                    }
                } else {  // For 180 and 360 angle
                    
                    if self.captureType == .Photo {
                        self.writeStop(mode: .SingleDelayMode, isForword: self.isForword) {
                            
                        }
                    } else {
                        self.writeStop(mode: .SingleDelayMode, isForword: self.isForword) {
                            
                        }
                    }
                }
                self.isStartPhotoAndVideoObserving = false
            }
            self.stopUIForModeAndBtnPlays()
            
            break
            
        case .Preview:
            
            let folderVC = storyboard?.instantiateViewController(withIdentifier: "FolderVC") as! FolderVC
            folderVC.gallaryOpenType = .Preview
            self.navigationController?.pushViewController(folderVC, animated: true)
            
            break
        case .Save:
            
            let photoSingle = storeManager.getAllFiles(directoryName: Constant.kRotoPic, folderName: CaptureType.Photo.rawValue, subfolder: AngleTypes.Single.rawValue)
            
            self.showHUD()
            albumManager?.moveImagesIn(albumName: Constant.kPhoto_Single, imageURLs: photoSingle ?? [URL](), completion: {
                
                let photo45 = storeManager.getAllFiles(directoryName: Constant.kRotoPic, folderName: CaptureType.Photo.rawValue, subfolder: AngleTypes.P45.rawValue)
                
                albumManager?.moveImagesIn(albumName: Constant.kPhoto_45, imageURLs: photo45 ?? [URL](), completion: {
                    
                    let photo90 = storeManager.getAllFiles(directoryName: Constant.kRotoPic, folderName: CaptureType.Photo.rawValue, subfolder: AngleTypes.P90.rawValue)
                    
                    
                    albumManager?.moveImagesIn(albumName: Constant.kPhoto_90, imageURLs: photo90 ?? [URL](), completion: {
                        
                        
                        let photo180 = storeManager.getAllFiles(directoryName: Constant.kRotoPic, folderName: CaptureType.Photo.rawValue, subfolder: AngleTypes.P180.rawValue)
                        
                        albumManager?.moveImagesIn(albumName: Constant.kPhoto_180, imageURLs: photo180 ?? [URL](), completion: {
                            
                            let photo360 = storeManager.getAllFiles(directoryName: Constant.kRotoPic, folderName: CaptureType.Photo.rawValue, subfolder: AngleTypes.P360.rawValue)
                            
                            albumManager?.moveImagesIn(albumName: Constant.kPhoto_360, imageURLs: photo360 ?? [URL](), completion: {
                                
                                let video45 = storeManager.getAllFiles(directoryName: Constant.kRotoPic, folderName: CaptureType.Video.rawValue, subfolder: AngleTypes.P45.rawValue)
                                
                                albumManager?.moveVideoIn(albumName: Constant.kVideo_45, videoURLs: video45 ?? [URL](), completion: {
                                    
                                    let video90 = storeManager.getAllFiles(directoryName: Constant.kRotoPic, folderName: CaptureType.Video.rawValue, subfolder: AngleTypes.P90.rawValue)
                                    
                                    albumManager?.moveVideoIn(albumName: Constant.kVideo_90, videoURLs: video90 ?? [URL](), completion: {
                                        
                                        let video180 = storeManager.getAllFiles(directoryName: Constant.kRotoPic, folderName: CaptureType.Video.rawValue, subfolder: AngleTypes.P180.rawValue)
                                        
                                        albumManager?.moveVideoIn(albumName: Constant.kVideo_180, videoURLs: video180 ?? [URL](), completion: {
                                            
                                            let video360 = storeManager.getAllFiles(directoryName: Constant.kRotoPic, folderName: CaptureType.Video.rawValue, subfolder: AngleTypes.P360.rawValue)
                                            
                                            albumManager?.moveVideoIn(albumName: Constant.kVideo_360, videoURLs: video360 ?? [URL](), completion: {
                                                
                                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                                    self.hideHUD()
                                                    self.showToast(message: "Photos and Videos saved")
                                                    self.checkIsableToStart()
                                                }
                                            })
                                            
                                        })
                                        
                                    })
                                    
                                })
                                
                            })
                            
                        })
                        
                    })
                })
                
            })
            break
            
        default:
            break
        }
    }
    
    //--------------------------------------------------------
    //                  MARK: - Function -
    //--------------------------------------------------------
    private func initialSetup() {
        
        viewSpeedSlider.sizeToFit()
        viewSpeedSliderLeading.constant = CGFloat(-(viewSpeedSlider.frame.width / 2))
        viewSpeedSlider.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi / 2))
        sliderSpeed.addTarget(self, action: #selector(onSliderValChanged(slider:event:)), for: .valueChanged)

        
        userdef.set(true, forKey: Constant.kIsCompletedIntroduction)
        let unselectedtitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: 0/255.0, green: 151.0/255.0, blue: 160.0/255.0, alpha: 1)]
        
        let selectedtitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        segmentPhotoVideo.setTitleTextAttributes(unselectedtitleTextAttributes, for: .normal)
        segmentPhotoVideo.setTitleTextAttributes(selectedtitleTextAttributes, for: .selected)
        
        picker.delegate = self
        picker.dataSource = self
        txtClicks.inputView = picker
        txtGrids.inputView = picker
        
        txtDelay.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(btnDonebarButtonClicked(_:)))
        txtSpeed.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(btnDonebarButtonClicked(_:)))

//        txtVideoDelay.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(btnDonebarButtonClicked(_:)))
//        txtContinueDelay.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(btnDonebarButtonClicked(_:)))
        
        txtDelay.text = userdef.string(forKey: USERDEFAULT.kDelay) ?? "0"
        txtVideoDelay.text = userdef.string(forKey: USERDEFAULT.kVideoDelay) ?? "5"
        txtSpeed.text = userdef.string(forKey: USERDEFAULT.kSpeed) ?? "1"

        //txtContinueDelay.text = userdef.string(forKey: USERDEFAULT.kContinueDelay) ?? "1"
        if cameraDevice != nil{
        cameraDevice.set(frameRate: 60)
        }
    }
    
    @objc private func btnDonebarButtonClicked(_ sender: UIButton) {
        if !self.viewSettings.isHidden {
        checkIsableToStart()
        }
    }
    
    @objc private func handlePinch(_ pinch: UIPinchGestureRecognizer) {
        
        guard sessionSetupSucceeds,  let device = cameraDevice else {
            print("return")
            return }
        
        switch pinch.state {
        case .began:
            initialScale = device.videoZoomFactor
        case .changed:
            
            try? device.lockForConfiguration()
            
            let minAvailableZoomScale = device.minAvailableVideoZoomFactor
            let maxAvailableZoomScale = device.maxAvailableVideoZoomFactor
            let availableZoomScaleRange = minAvailableZoomScale...maxAvailableZoomScale
            let resolvedZoomScaleRange = zoomScaleRange.clamped(to: availableZoomScaleRange)
            
            let resolvedScale = max(resolvedZoomScaleRange.lowerBound, min(pinch.scale * initialScale, resolvedZoomScaleRange.upperBound))
            
            device.videoZoomFactor = resolvedScale
            device.unlockForConfiguration()
            
        default:
            return
        }
        
    }
    private func settingForVideoPhoto() {
        
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = .photo
        
        guard let backCamera = AVCaptureDevice.default(for: .video)
            else {
                print("Unable to access back camera!")
                return
        }
//        guard let backCamera = AVCaptureDevice.default(.builtInDualCamera, for: .video, position: .back)
//        else {
//            print("Unable to access back camera!")
//            return
//        }
        cameraDevice = backCamera
        
//        if let device = cameraDevice {
//
//                // 1
//            for vFormat in cameraDevice!.formats {
//
//                    // 2
//                let ranges = vFormat.videoSupportedFrameRateRanges as [AVFrameRateRange]
//                let frameRates = ranges[0]
//
//                    // 3
//                if frameRates.maxFrameRate == Double(VideoFPS.Sixty.rawValue) {
//
//                        // 4
//                    do {
//                        try device.lockForConfiguration()
//                        device.activeFormat = vFormat as AVCaptureDevice.Format
//                        device.activeVideoMinFrameDuration = frameRates.minFrameDuration
//                        device.activeVideoMaxFrameDuration = frameRates.maxFrameDuration
//                        device.unlockForConfiguration()
//
//                    } catch {
//                        print(error.localizedDescription)
//                    }
//                }
//            }
//        }
        //fps
//        self.configureCameraForHighestFrameRate(device: self.cameraDevice, maxFramRate: self.videoFPSSetting.rawValue)
//        cameraDevice.set(frameRate: Double(self.videoFPSSetting.rawValue))
       
        cameraDevice.set(frameRate: 60)
        if cameraDevice.isFocusModeSupported(.continuousAutoFocus) {
            
            try! cameraDevice.lockForConfiguration()
            cameraDevice.focusMode = .continuousAutoFocus
            cameraDevice.unlockForConfiguration()
        }
        
        sliderSpeed.minimumValue = Float(1)
        sliderSpeed.maximumValue = Float(100)
        userdef.set(userdef.string(forKey: USERDEFAULT.kSpeed) ?? "1", forKey:  USERDEFAULT.kSpeed)
        sliderSpeed.value = Float(userdef.string(forKey: USERDEFAULT.kSpeed) ?? "1") ?? 1
        
        let minISO = cameraDevice.activeFormat.minISO
        let maxISO = cameraDevice.activeFormat.maxISO
        
        let clampedISO = ((maxISO - minISO) / 2) + minISO
        
        sliderBrightness.minimumValue = Float(minISO)
        sliderBrightness.maximumValue = Float(maxISO)
        sliderBrightness.value = Float(clampedISO)
        setupCameraBrightness(value: clampedISO)
        
        do {
            let input = try AVCaptureDeviceInput(device: backCamera)
            
            capturePhotoOutput = AVCapturePhotoOutput()
            if captureSession.canAddInput(input) && captureSession.canAddOutput(capturePhotoOutput) {
                
                captureSession.addInput(input)
                captureSession.addOutput(capturePhotoOutput)
                
                setupLivePreview()
            }
        } catch let error  {
            print("Error Unable to initialize back camera:  \(error.localizedDescription)")
        }
        
        if captureType == .Video {
            
            captureSession.sessionPreset = .high
            for output in captureSession.outputs {
                if output == movieOutput {
                    captureSession.removeOutput(movieOutput)
                }
            }
            
            movieOutput = AVCaptureMovieFileOutput()
            
            
            captureSession.addOutput(movieOutput)
        }
    }
    
    func configureCameraForHighestFrameRate(device: AVCaptureDevice, maxFramRate: Int) {
        
        var bestFormat: AVCaptureDevice.Format?
        var bestFrameRateRange: AVFrameRateRange?

        for format in device.formats {
            for range in format.videoSupportedFrameRateRanges {
                if range.maxFrameRate > bestFrameRateRange?.maxFrameRate ?? 0 {
                    bestFormat = format
                    bestFrameRateRange = range
                }
            }
        }
        
        if let bestFormat = bestFormat,
           let bestFrameRateRange = bestFrameRateRange {
            if Int(bestFrameRateRange.maxFrameRate) >= maxFramRate {
                do {
                    try device.lockForConfiguration()
                    
                        // Set the device's active format.
                    device.activeFormat = bestFormat
                    
                        // Set the device's min/max frame duration.
                    let duration = bestFrameRateRange.minFrameDuration
                    device.activeVideoMinFrameDuration = duration
                    device.activeVideoMaxFrameDuration = CMTimeMake(value: 1, timescale: Int32(maxFramRate))
                    
                    device.unlockForConfiguration()
                } catch {
                        // Handle error.
                }
            }
        }
    }
    
    private func setupLivePreview() {
        
        for lay in viewCamera.layer.sublayers ?? [CALayer]() {
            if lay is AVCaptureVideoPreviewLayer {
                lay.removeFromSuperlayer()
            }
        }
    
        viewCamera.isUserInteractionEnabled = true
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        
        videoPreviewLayer.videoGravity = .resizeAspectFill
        videoPreviewLayer.connection?.videoOrientation = .portrait
        self.videoPreviewLayer.frame = self.viewCamera.bounds
        viewCamera.layer.addSublayer(videoPreviewLayer)
        
        DispatchQueue.global(qos: .userInitiated).async { //[weak self] in
            self.captureSession.automaticallyConfiguresApplicationAudioSession = false
            self.captureSession.usesApplicationAudioSession = true
            self.captureSession.startRunning()
        }
    }
    private func setManualFocusGesture() {
        
        let tapGesture = UITapGestureRecognizer()
        tapGesture.numberOfTapsRequired = 1
        tapGesture.addTarget(self, action: #selector(setManualFocus(_:)))
        viewCamera.addGestureRecognizer(tapGesture)
        
    }
    @objc private func setManualFocus(_ gesture: UITapGestureRecognizer) {
        
        let location = gesture.location(in: self.viewCamera)
        
        let screenSize = viewCamera.bounds.size
        let focusPoint = CGPoint(x: (location.y / screenSize.height), y: 1.0 - (location.x / screenSize.width))
        
        try! cameraDevice.lockForConfiguration()
        cameraDevice.focusPointOfInterest = focusPoint
        
        cameraDevice.focusMode = .continuousAutoFocus
        cameraDevice.exposurePointOfInterest = focusPoint
        cameraDevice.exposureMode = .continuousAutoExposure
        cameraDevice.unlockForConfiguration()
        
    }
    private func setupCameraBrightness(value: Float) {
        
        if value >= cameraDevice.activeFormat.minISO && value <= cameraDevice.activeFormat.maxISO {
            
            do {
                try! cameraDevice.lockForConfiguration()
                cameraDevice.exposureMode = .custom
                
                cameraDevice.setExposureModeCustom(duration: AVCaptureDevice.currentExposureDuration, iso: value) { (t) in
                    print("Epoch Time: \(t.epoch)")
                }
                cameraDevice.unlockForConfiguration()
            } catch  {
                
            }
        }
    }
    private func makeGridLines(noOfColumn: Int, noOfRows: Int, gridview : GridType) {
        
        let startIndex = gridview == .Grid3x3 ? 0 : 1
        
        viewGrid = UIView()
        viewGrid.frame = viewCamera.frame
        
        let size  = viewCamera.frame.size
        
        let viewwidth = ((size.width) - CGFloat((xSpacing * 2)))
        ySpacing = size.height / 2  - (viewwidth / 2)
        
        viewGrid.frame = CGRect(x: xSpacing, y: ySpacing, width: viewwidth , height: viewwidth)
        
        if gridview == .CircleGrid {
            
            viewGrid.layer.cornerRadius = viewwidth / 2
            viewGrid.layer.borderColor = UIColor.white.cgColor
            viewGrid.layer.borderWidth = 1
        }
        
        viewGrid.backgroundColor = UIColor.clear
        
        for layer in self.viewGrid.layer.sublayers ?? [CALayer]() {
            if layer is CAShapeLayer {
                layer.removeFromSuperlayer()
            }
        }
        
        var startYPosition : CGFloat = 0 //self.viewGrid.frame.origin.y
        let height = viewwidth//self.viewGrid.frame.size.height
        let spaceBetweenTwoRow = height / (gridview == .Grid3x3 ? CGFloat(noOfRows - 1) : 2)
        
        var startXPosition : CGFloat = 0 //self.viewGrid.frame.origin.x
        let width = viewwidth//self.viewGrid.frame.size.width
        let spaceBetweenTwoColumn = width / (gridview == .Grid3x3 ? CGFloat(noOfColumn - 1) : 2)
        
        //For x Axis
        for _ in startIndex..<noOfRows {
            
            startYPosition += gridview == .Grid3x3 ? 0 : spaceBetweenTwoRow
            
            let dashlineColor = UIColor.white
            drawDashLine(start: CGPoint(x: 0, y: startYPosition), end: CGPoint(x: width , y: startYPosition), view: self.viewGrid, color: dashlineColor)
            startYPosition += spaceBetweenTwoRow
        }
        
        //For y Axis
        for _ in startIndex..<noOfColumn {
            
            startXPosition += gridview == .Grid3x3 ? 0 : spaceBetweenTwoColumn
            
            let dashlineColor = UIColor.white
            drawDashLine(start: CGPoint(x: startXPosition, y: 0), end: CGPoint(x: startXPosition, y: height), view: self.viewGrid, color: dashlineColor)
            
            startXPosition += spaceBetweenTwoColumn
        }
        
        //viewCamera.layer.addSublayer(viewGrid)
        viewCamera.translatesAutoresizingMaskIntoConstraints = false
        viewCamera.addSubview(viewGrid)
    }
    
    private func drawDashLine(start p0: CGPoint, end p1: CGPoint, view: UIView, color: UIColor) {
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = 2
        shapeLayer.lineDashPattern = [5, 0] // 2 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    private func setMotionForGrid(grid : GridType) {
        
        motionManager.stopDeviceMotionUpdates()
        if grid != .Grid3x3 {
            
            if motionManager.isDeviceMotionAvailable {
                motionManager.deviceMotionUpdateInterval = 0.01
                motionManager.startDeviceMotionUpdates(to: OperationQueue.main) {
                    (data, error) in
                    if let data = data {
                        
                        print("data.gravity.x \(data.gravity.x)")
                        print("data.gravity.x \(data.gravity.y)")
                        
                        let rotation = atan2(data.gravity.x, data.gravity.y) - .pi
                        self.viewGrid.transform = CGAffineTransform(rotationAngle: CGFloat(rotation))
                    }
                }
            } else {
                showAlertWithOKButton(message: "Motion not supported in your device")
            }
        }
    }
    private func setSelectedAngle() {
        
        for btn in btnAngelCollection {
            
            if btn.titleLabel?.text == angleType.rawValue {
                btn.setTitleColor(#colorLiteral(red: 0, green: 0.6524586678, blue: 0.6893625855, alpha: 1), for: .normal)
            } else {
                btn.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            }
        }
    }
    private func setSelectedMode() {
        
        for btn in btnModesCollection {
            if btn.tag == mode.rawValue {
                btn.backgroundColor = #colorLiteral(red: 0, green: 0.6524586678, blue: 0.6893625855, alpha: 1)
                btn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            } else {
                btn.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                btn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            }
        }
    }
    private func setSelectedPlay() {
        
        var count = 0
        
        count += storeManager.getAllFiles(directoryName: Constant.kRotoPic, folderName: CaptureType.Photo.rawValue, subfolder: AngleTypes.Single.rawValue)?.count ?? 0
        
        count += storeManager.getAllFiles(directoryName: Constant.kRotoPic, folderName: CaptureType.Photo.rawValue, subfolder: AngleTypes.P45.rawValue)?.count ?? 0
        
        count += storeManager.getAllFiles(directoryName: Constant.kRotoPic, folderName: CaptureType.Photo.rawValue, subfolder: AngleTypes.P90.rawValue)?.count ?? 0
        
        count += storeManager.getAllFiles(directoryName: Constant.kRotoPic, folderName: CaptureType.Photo.rawValue, subfolder: AngleTypes.P180.rawValue)?.count ?? 0
        
        count += storeManager.getAllFiles(directoryName: Constant.kRotoPic, folderName: CaptureType.Photo.rawValue, subfolder: AngleTypes.P360.rawValue)?.count ?? 0
        
        count += storeManager.getAllFiles(directoryName: Constant.kRotoPic, folderName: CaptureType.Video.rawValue, subfolder: AngleTypes.P45.rawValue)?.count ?? 0
        
        count += storeManager.getAllFiles(directoryName: Constant.kRotoPic, folderName: CaptureType.Video.rawValue, subfolder: AngleTypes.P90.rawValue)?.count ?? 0
        
        count += storeManager.getAllFiles(directoryName: Constant.kRotoPic, folderName: CaptureType.Video.rawValue, subfolder: AngleTypes.P180.rawValue)?.count ?? 0
        
        count += storeManager.getAllFiles(directoryName: Constant.kRotoPic, folderName: CaptureType.Video.rawValue, subfolder: AngleTypes.P360.rawValue)?.count ?? 0
        
        var newPlayTypes = playType
        if count > 0 {
            newPlayTypes += [PlayType.Preview, PlayType.Save]
        }
        
        for btn in btnPlays {
            btn.isSelected = newPlayTypes.contains(PlayType(rawValue: btn.tag)!)
        }
    }
    private func checkIsableToStart() {
        
        
        if mode == .Eaching || btnCustom.isSelected {
            
            if !txtSpeed.text!.trime().isEmpty && !txtDelay.text!.trime().isEmpty {
            //if  userdef.string(forKey: USERDEFAULT.kSpeed) != nil && !txtDelay.text!.trime().isEmpty {
                playType = [.Start]
            } else {
                playType = [.None]
            }
        
        }else if mode == .Continue || btnCustom.isSelected {
            
            if !txtSpeed.text!.trime().isEmpty && !txtDelay.text!.trime().isEmpty {
            //if  userdef.string(forKey: USERDEFAULT.kSpeed) != nil { //&& !txtDelay.text!.trime().isEmpty {
                playType = [.Start]
            } else {
                playType = [.None]
            }
        
        } else if captureType == .Video {
            
//            if !txtSpeed.text!.trime().isEmpty && angleType != .Single && !txtDelay.text!.trime().isEmpty && Int(txtDelay.text ?? "0")! > 0 {
 //         if userdef.string(forKey: USERDEFAULT.kSpeed) != nil && angleType != .Single {
                if !txtSpeed.text!.trime().isEmpty && angleType != .Single {
                playType = [.Start]
                
            } else {
                playType = [.None]
            }
            
        } else {
            
            if !txtDelay.text!.trime().isEmpty && !txtSpeed.text!.trime().isEmpty && (txtClicks.text!.trime().lowercased() != kSet.lowercased() || btnCustom.isSelected) && angleType != .Single && Int(txtDelay.text ?? "0")! > 0 {
            //if !txtDelay.text!.trime().isEmpty && userdef.string(forKey: USERDEFAULT.kSpeed) != nil && (txtClicks.text!.trime().lowercased() != kSet.lowercased() || btnCustom.isSelected) && angleType != .Single {
                playType = [.Start]
            } else {
                playType = [.None]
            }
        }
        
        setSelectedPlay()
    }
    private func setConnectDisConnect() {
        
        DispatchQueue.main.async {
            
            if connectedBLEDevice != nil && connectedBLEDevice?.isConnected() ?? false {
                
                self.btnTurnTable.isSelected = true
                self.btnConnectDisconnect.isSelected = true
                self.btnConnectDisconnectTitle.isSelected = true
                self.startGettingDeviceUpdate()
                
            } else {
                
                self.btnTurnTable.isSelected = false
                self.btnConnectDisconnect.isSelected = false
                self.btnConnectDisconnectTitle.isSelected = false
                self.stopGettingDeviceUpdate()
            }
        }
    }
    
    private func stopGettingDeviceUpdate() {
        
        if self.timerForUpdateDevice?.isValid ?? true {
            self.timerForUpdateDevice?.invalidate()
        }
    }
    
    private func startGettingDeviceUpdate() {
        
        var bytes = [UInt8]()
        var count = 0
        
        DispatchQueue.main.async {
            
            self.timerForUpdateDevice?.invalidate()
            self.timerForUpdateDevice = Timer.scheduledTimer(withTimeInterval: 0.2, repeats: true, block: { (t) in

                print("timer call")
                if let d = connectedBLEDevice {

                    bytes = [UInt8]()
                    count = 0
                    d.writeValueIn(characteristicUUID: Command.CHAR_UUID, value: Command.getCommand(commandType: .readdatafromuC, Command.defaultBytes)) { (isSuccess, message, char, device) in

                        if isSuccess {

                        } else {
                            self.showAlertWithOKButton(message: message)
                            self.setConnectDisConnect()
                            self.timerForUpdateDevice?.invalidate()
                        }
                    }
                }
            })
        }
        
        if let d = connectedBLEDevice {
            d.getUpdatedCharacteristic { (char, bleDevice) in

                print("Getting Updates")
                connectedBLEDevice = bleDevice

                if let charValue = char.byteArray {

                    if charValue.first == 0x40 && charValue.count == 20 {

                        bytes = charValue
                        count += 1

                    } else if count == 1 {
                        bytes += charValue
                        
                        DispatchQueue.main.async {
                            self.parseData(bytes: bytes)
                        }
                    }
                }
            }
        }
    }
    let response = ResponseModel()
    private func parseData(bytes: [UInt8]) {
        
        if bytes.count > 23 {
            
            response.mode = Mode(rawValue: bytes[2])
            response.microStepping = byteToInt(bytes: [bytes[3], bytes[4]])
            response.speed = byteToInt(bytes: [bytes[5], bytes[6]])
            response.direction = Int(bytes[8])
            response.delay = Int(bytes[10])
            response.angle = byteToInt(bytes: [bytes[11], bytes[12]])
            response.stepAngle = byteToInt(bytes: [bytes[13], bytes[14]])
            response.machineBit = byteToInt(bytes: [bytes[15], bytes[16]])
            response.gearValue = Int(Float(byteToInt(bytes: [bytes[17], bytes[18]])) / 10.0)
            response.realAngle = Int(ceil(Double(byteToInt(bytes: [bytes[19], bytes[20]])) / 10.0))
            response.captureBit = byteToInt(bytes: [bytes[21], bytes[22]])
            
            print("parseData: mode = \(response.mode)  byte = \(bytes[2])")
            print("parseData: speed = \(response.speed)")
            print("parseData: direction = \(response.direction)")
            print("parseData: delay = \(response.delay)")
            print("parseData: microStepping = \(response.microStepping)")
            print("parseData: angle = \(response.angle)")
            print("parseData: stepAngle = \(response.stepAngle)")
            print("parseData: machineBit = \(response.machineBit)")
            print("parseData: captureBit = \(response.captureBit)")
            print("parseData: gearValue = \(response.gearValue)")
            print("parseData: realAngle = \(response.realAngle)")
            print("parseData: --------------- ")
            
            
            print("Anglesssss realAngle: \(response.realAngle)")
            print("Anglesssss lastRealAngle: \(self.lastRealAngle)")
            var newAngle = response.realAngle
            if self.lastRealAngle != 0 || self.lastRealAngle != 360 {
                
                if response.direction == 1 {
                    newAngle -= self.lastRealAngle
                } else {
                    newAngle += self.lastRealAngle
                }
            }
        
            if self.previousAngle != newAngle {
                self.previousAngle = newAngle
                self.viewAngel.angle = newAngle
            }
            print("Anglesssss newAngle: \(newAngle)")
            
            if self.isStartPhotoAndVideoObserving {
                
                if response.machineBit == 1 {
                    
                    if response.captureBit == 1 {
                        if self.isCapturePhoto {
                            self.isCapturePhoto = false
                            self.takePhoto()
                            
                        }
                    } else {
                        if !self.isCapturePhoto {
                            self.isCapturePhoto = true
                        }
                    }
                } else {
                    
                    self.isStartPhotoAndVideoObserving = false
                    
                    if self.currentObservType == .first {
                        
                        self.stopGettingDeviceUpdate()
//                        DispatchQueue.main.async {
                            
                            let delay = Int(self.txtDelay.text!.trime()) ?? 2
                            let angleValue = self.angleType.getIntAngle()
                            let clicks = Int(self.txtClicks.text!.trime()) ?? 2
                            
                            self.writeDelay(value: delay) {
                                
                                //self.makedelay()
                                self.writeAngel(value: angleValue * 2) {
                                    
                                    //self.makedelay()
                                    self.writeNoOfStop(value: clicks) {
                                        
                                        //self.makedelay()
                                        self.writeStart(mode: .SingleDelayMode, isForword: true) {
                                            
                                            self.startGettingDeviceUpdate()
                                            self.takePhoto()
                                            
                                            DispatchQueue.main.asyncAfter(deadline: .now() + Double(delay)) {
                                                self.isStartPhotoAndVideoObserving = true
                                                self.currentObservType = .second
                                            }
                                        }
                                    }
                                }
                            }
//                        }
                        
                    } else if self.currentObservType == .second {
                        
//                        DispatchQueue.main.async {
                            self.stopGettingDeviceUpdate()
                            let delay = Int(self.txtDelay.text!.trime()) ?? 2
                            let angleValue = self.angleType.getIntAngle()
                            let clicks = Int(self.txtClicks.text!.trime()) ?? 2
                            
                            self.writeDelay(value: delay) {
                                
                                //self.makedelay()
                                self.writeAngel(value: angleValue) {
                                    
                                    //self.makedelay()
                                    self.writeNoOfStop(value: clicks / 2) {
                                        
                                        //self.makedelay()
                                        self.writeStart(mode: .SingleDelayMode, isForword: false) {
                                            
                                            self.startGettingDeviceUpdate()
                                            self.takePhoto()
                                            
                                            DispatchQueue.main.asyncAfter(deadline: .now() + Double(delay)) {
                                                self.isStartPhotoAndVideoObserving = true
                                                self.currentObservType = .last
                                            }
                                        }
                                    }
                                }
                            }
//                        }
                    } else {
                        
                        if self.captureType == .Video{
                        self.stopVideoTaking()
                        }
                        self.currentObservType = .None
                        self.stopUIForModeAndBtnPlays()
                        
                    }
                }
            } else if self.mode == .Home {
                
                if response.realAngle < 1 || response.realAngle > 359 {
                    self.stopUIForModeAndBtnPlays()
                }
                
            } else if self.mode == .Eaching {
                //                    self.viewAngel.angle = response.realAngle
                
            } else if self.mode == .Manual {
                
                if response.machineBit == 0 {
                    self.stopUIForModeAndBtnPlays()
                }
            }else if self.mode == .Continue{
//                if !(self.stopContinueRunnable?.isValid ?? false) {
                if self.captureType == .Video{
                self.stopVideoTaking()
                }
//                }
            }
            self.lastDeviceResponse = response
        }
    }
    
    private func byteToInt(bytes: [UInt8]) -> Int {
        
        var value : Int = 0
        for byte in bytes {
            value = value << 8
            value = value | Int(byte)
        }
        return value
    }
    
    private func takePhoto() {
        
        if self.captureType == .Photo {
            
            let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
            capturePhotoOutput.capturePhoto(with: settings, delegate: self)
        
            if angleType != .Single {
                
                DispatchQueue.main.async {
                    self.picCount += 1
                    self.lblPicCount.isHidden = false
                    self.lblPicCount.text = "\(self.picCount)"
                }
            }
        }
    }
    
    private func startVideoTaking() {
        
        if isStopCallback {
            self.stopVideoRunnable?.invalidate()
        } else {
            if self.captureType == .Video {
                
                let videoStoreURL = storeManager.getVideoStorePath(directoryName: Constant.kRotoPic, folderName: captureType.rawValue, subfolder: angleType.rawValue, videoName: getCurrentDateWithMilisecond())
                
                self.showToast(message: "Start Recording")
                self.movieOutput.startRecording(to: videoStoreURL, recordingDelegate: self)
            }
        }
    }
    private func stopVideoTaking() {
        
//        if  self.isContinue {
//            DispatchQueue.main.async {
//                self.stopContinueRunnable?.invalidate()
//                let delaytime = Double(userdef.string(forKey: USERDEFAULT.kContinueDelay) ?? "1") ?? 1
//                self.stopContinueRunnable = Timer.scheduledTimer(withTimeInterval: delaytime , repeats: true, block: { (t) in
//                    if self.captureType == .Video || self.isContinuePhoto {
//                        self.stopContinueRunnable?.invalidate()
//                        self.stopGettingDeviceUpdate()
//                        let speed = Int(Float(userdef.string(forKey: USERDEFAULT.kSpeed) ?? "1") ?? 1)
//                        //            self.writeSpeed(value: Int(txtSpeed.text!.trime()) ?? 10) {
//                        self.writeSpeed(value: speed) {
//                            //self.writeDelay(value: Int(userdef.string(forKey: USERDEFAULT.kContinueDelay) ?? "1") ?? 1) {
//                            self.writeAngel(value: self.angleType.getIntAngle()) {
//                                self.writeStart(mode: .ContinuosDelayMode, isForword: self.isForword) {
//                                    self.startGettingDeviceUpdate()
//                                }
//                            }
//                            //}
//                        }
////                        self.playType = [.Stop]
////                        self.setSelectedPlay()
////                        self.stopUIForModeAndBtnPlays()
////                        self.viewSettings.isHidden = true
//                    }
//                })
//            }
//        } else {
            //self.isContinue = false
            self.isStopCallback = true
            DispatchQueue.main.async {
                self.stopVideoRunnable?.invalidate()
                let delaytime = Double(userdef.string(forKey: USERDEFAULT.kVideoDelay) ?? "5") ?? 5
//                self.stopVideoRunnable = Timer.scheduledTimer(withTimeInterval: delaytime , repeats: true, block: { (t) in
                    if self.captureType == .Video { //}|| self.isContinuePhoto {
                        //self.isContinuePhoto = false
                        self.isStopCallback = false
                        self.showToast(message: "Stop Recording")
                        self.stopVideoRunnable?.invalidate()
                        self.captureSession.stopRunning()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            //            self.settingForVideoPhoto()
                            self.captureSession.automaticallyConfiguresApplicationAudioSession = false
                            self.captureSession.usesApplicationAudioSession = true
                            self.captureSession.startRunning()
                        }
                        //add for new issue
                        self.playType = [.Stop]
                        self.setSelectedPlay()
                        self.stopUIForModeAndBtnPlays()
                    }
//                })
            }
//        }
    }
    
    private func stopUIForModeAndBtnPlays() {
        
        DispatchQueue.main.async {
            
            self.viewscheduling.isHidden = true
            self.viewSettings.isHidden = false
            self.btnCustom.isSelected = false
            self.btnCustom.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.btnCustom.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            
            self.picCount = 0
            self.lblPicCount.isHidden = true
            
            self.mode = .None
            self.setSelectedMode()
            
            self.playType = [.None]
            self.setSelectedPlay()
            self.checkIsableToStart()
        }
    }
}

//MARK:-  UITextFieldDelegate Methods
extension CapturePhotoVideoVC : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTextField = textField
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            if textField == txtDelay {
                
                let value = Int(updatedText) ?? 0
                if value > 24 {
                    return false
                }
                userdef.set("\(value)", forKey: USERDEFAULT.kDelay)
            } else if textField == txtVideoDelay {
                
                let value = Int(updatedText) ?? 0
                if value > 10 {
                    return false
                }
                else if updatedText.count > 0 && value < 0 {
                    return false
                }
                userdef.set("\(value)", forKey: USERDEFAULT.kVideoDelay)
            } else if textField == txtSpeed {
                let value = Int(updatedText) ?? 10
                
                if value > 600 {
                    return false
                }else {
                    userdef.set("\(value)", forKey: USERDEFAULT.kSpeed)
                    self.writeSpeed(value: value) {
                    }
                }
                
            }

        }
        
        return true
    }
}

//MARK:-  UIPickerViewDataSource Methods
extension CapturePhotoVideoVC : UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return selectedTextField == txtClicks ? arrOfClicks.count:arrOfGrids.count
    }
}

//MARK:-  UIPickerViewDelegate Methods
extension CapturePhotoVideoVC : UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return selectedTextField == txtClicks ? arrOfClicks[row]:arrOfGrids[row].rawValue
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if selectedTextField == txtClicks {
            
            if btnCustom.isSelected && (row != 0) {
                
                selectedTextField.resignFirstResponder()
                self.showAlertWithOKButton(message: "Please unselect custom options")
                
                return
            }
            
            selectedTextField.text = arrOfClicks[row]
            
        } else if selectedTextField == txtGrids {
            
            let selectedGrid = arrOfGrids[row]
            selectedTextField.text = selectedGrid.rawValue
            
            motionManager.stopDeviceMotionUpdates()
            viewGrid.removeFromSuperview()
            viewGrid = UIView()
            
            switch selectedGrid {
            case .None:
                viewGrid = UIView()
                
            case .Grid2x2:
                makeGridLines(noOfColumn: 2, noOfRows: 2, gridview: selectedGrid)
                setMotionForGrid(grid: selectedGrid)
                
            case .Grid3x3:
                makeGridLines(noOfColumn: 4, noOfRows: 4, gridview: selectedGrid)
                
            default:
                makeGridLines(noOfColumn: 2, noOfRows: 2, gridview: selectedGrid)
                setMotionForGrid(grid: selectedGrid)
            }
        }
        self.checkIsableToStart()
    }
}

extension CapturePhotoVideoVC: AVCaptureFileOutputRecordingDelegate {
    
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        
        if (error != nil) {
            print("Unable to save video to the iPhone  \(error?.localizedDescription ?? "")")
            
        } else {
            // save video to photo album
//            library.writeVideoAtPath(toSavedPhotosAlbum: outputFileURL, completionBlock: nil)
//            captureSession.removeOutput(movieOutput)
            print("Video captured")
            
        }
    }
}

//MARK:-  AVCapturePhotoCaptureDelegate Methods
extension CapturePhotoVideoVC: AVCapturePhotoCaptureDelegate {
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        guard let imageData = photo.fileDataRepresentation()
            else { return }
        
        print("image captured")
        if let image = UIImage(data: imageData) {
            
            storeManager.saveJpg(directoryName: Constant.kRotoPic, folderName: captureType.rawValue, subfolder: angleType.rawValue, imageName: getCurrentDateWithMilisecond(), image: image)
        }
        //        imgCapture.image = image
    }
}
