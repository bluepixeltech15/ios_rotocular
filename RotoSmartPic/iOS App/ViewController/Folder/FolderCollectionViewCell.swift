//
//  FolderCollectionViewCell.swift
//  RotoSmartPic
//
//  Created by BAPS on 07/11/20.
//  Copyright © 2020 Bluepixel Technologies. All rights reserved.
//

import UIKit

class FolderCollectionViewCell: UICollectionViewCell {
    
    //MARK:-  Outlets And Variable Declarations
    @IBOutlet weak var lblFolderName: UILabel!
    
}
