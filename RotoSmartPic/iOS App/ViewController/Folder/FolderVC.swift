//
//  FolderVC.swift
//  RotoSmartPic
//
//  Created by BAPS on 07/11/20.
//  Copyright © 2020 Bluepixel Technologies. All rights reserved.
//

import UIKit

class FolderVC: UIViewController {
    
    //MARK:-  Outlets And Variable Declarations
    @IBOutlet weak var collectionViewPhoto: UICollectionView!
    @IBOutlet weak var collectionViewVideo: UICollectionView!
    
    public var gallaryOpenType : GallaryOpenType  = .Selection
    
    private var arrOfPhotoFolders = [AngleTypes.P45, AngleTypes.P90, AngleTypes.P180, AngleTypes.P360, AngleTypes.Single]
    private var arrOfVideoFolders = [AngleTypes.P45, AngleTypes.P90, AngleTypes.P180, AngleTypes.P360]

    //MARK:- 
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK:-  Buttons Clicked Actions
    @IBAction func btnBackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:-  Functions

}

//MARK:-  UICollectionViewDelegateFlowLayout Methods
extension FolderVC : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewPhoto {
            
            let width = Int(collectionView.bounds.width) / arrOfPhotoFolders.count
            return CGSize(width: CGFloat(width), height: collectionView.bounds.height)
            
        } else {
            
            let width = Int(collectionView.bounds.width) / arrOfVideoFolders.count
            return CGSize(width: CGFloat(width), height: collectionView.bounds.height)
        }
    }
}

//MARK:-  UICollectionViewDataSource Methods
extension FolderVC : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView == collectionViewPhoto ? arrOfPhotoFolders.count:arrOfVideoFolders.count
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FolderCollectionViewCell", for: indexPath) as! FolderCollectionViewCell
        
        cell.lblFolderName.text = collectionView == collectionViewPhoto ? arrOfPhotoFolders[indexPath.row].rawValue:arrOfVideoFolders[indexPath.row].rawValue
        
        return cell
    }
}

//MARK:-  UICollectionViewDelegate Methods
extension FolderVC : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let galleryVC = storyboard?.instantiateViewController(withIdentifier: "GalleryVC") as! GalleryVC
        
        galleryVC.gallaryOpenType = self.gallaryOpenType
        
        if collectionView == collectionViewPhoto {
            galleryVC.folder = .Photo
            galleryVC.subFolder = arrOfPhotoFolders[indexPath.row]
        } else {
            galleryVC.folder = .Video
            galleryVC.subFolder = arrOfVideoFolders[indexPath.row]
        }
        self.navigationController?.pushViewController(galleryVC, animated: true)
    }
}
