//
//  WelcomeVC.swift
//  RotoSmartPic
//
//  Created by Mac on 13/05/1942 Saka.
//  Copyright © 1942 Bluepixel Technologies. All rights reserved.
//

import UIKit

class WelcomeVC: UIViewController {
    
    //--------------------------------------------------------
    //                  MARK: - Outlet -
    //--------------------------------------------------------
    
    //--------------------------------------------------------
    //                  MARK: -Property -
    //--------------------------------------------------------
    
    //--------------------------------------------------------
    //                  MARK: - View Life Cycle -
    //--------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        // Do any additional setup after loading the view.
    }
    
    
    //--------------------------------------------------------
    //                  MARK: - Function -
    //--------------------------------------------------------

    //--------------------------------------------------------
    //                  MARK: -  Button Action -
    //--------------------------------------------------------
    
    @IBAction func btnNext(sender:UIButton) {
        
        let brandingVC = storyboard?.instantiateViewController(withIdentifier: "BrandingVC") as! BrandingVC
        
        self.navigationController?.pushViewController(brandingVC, animated: true)
    }
    
}
