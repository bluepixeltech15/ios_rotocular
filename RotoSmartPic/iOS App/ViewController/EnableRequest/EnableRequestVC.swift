//
//  EnableRequestVC.swift
//  RotoSmartPic
//
//  Created by Mac on 14/05/1942 Saka.
//  Copyright © 1942 Bluepixel Technologies. All rights reserved.
//

import UIKit

class EnableRequestVC: UIViewController {
    
    
    //--------------------------------------------------------
    //                  MARK: - Outlet -
    //--------------------------------------------------------
    
     @IBOutlet  var imgcheckBoxCollection : [UIImageView] = []
    
    //--------------------------------------------------------
    //                  MARK: -Property -
    //--------------------------------------------------------
    
    //--------------------------------------------------------
    //                  MARK: - View Life Cycle -
    //--------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    //--------------------------------------------------------
    //                  MARK: - Function -
    //--------------------------------------------------------
    
    //--------------------------------------------------------
    //                  MARK: -  Button Action -
    //--------------------------------------------------------
    
    @IBAction func btnCheckboxSelecet(sender:UIButton) {
        
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            self.imgcheckBoxCollection[sender.tag].image = UIImage(named: "ic_checkboxSelected")
        } else {
            self.imgcheckBoxCollection[sender.tag].image = UIImage(named: "ic_checkboxUnselected")
        }
    }
    
    @IBAction func btnNext(sender:UIButton) {
        let deviceConnectVC = storyboard?.instantiateViewController(withIdentifier: "CapturePhotoVideoVC") as! CapturePhotoVideoVC
        self.navigationController?.pushViewController(deviceConnectVC, animated: true)
    }
    
    @IBAction func btnBack(sender:UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}
