//
//  ResponseModel.swift
//  RotoSmartPic
//
//  Created by BAPS on 09/11/20.
//  Copyright © 2020 Bluepixel Technologies. All rights reserved.
//

import UIKit

class ResponseModel: NSObject {
    
    var mode:             Mode?
    var speed           = Int()
    var microStepping   = Int()
    var direction       = Int()
    var delay           = Int()
    var angle           = Int()
    var stepAngle       = Int()
    var machineBit      = Int()
    var gearValue       = Int()
    var realAngle       = Int()
    var captureBit      = Int()
}

class Group: NSObject {

    var groupindex = 0
    var groupName  = "1"
    var arrOfIdividualValues = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    var arrOfIdividualValuesEnable = [true, true, true, true, true, true, true, true, true, true, true, true]
    var arrTotalIdividualNumber:[String] = []
    var arrOfGroupValues = [GroupValue]()
    
    init(dict: Any) {
        super.init()
        
        if let d = dict as? [String: Any] {
            
            self.groupindex = (d[Constant.kgroupindex] as? Int) ?? 0
            self.groupName = (d[Constant.kgroupName] as? String) ?? "1"
            if let indiValues = (d[Constant.kindividualValues] as? [Int]) {
                self.arrOfIdividualValues = indiValues
            }
             
            if let GroupValues = (d[Constant.kgroupValues] as? [Any]) {
                arrOfGroupValues.removeAll()
                for value in GroupValues {
                    arrOfGroupValues.append(GroupValue(dict: value))
                }
            }
            
        } else {
            arrOfGroupValues.removeAll()
            for _ in 0...3 {
                arrOfGroupValues.append(GroupValue(dict: [:]))
            }
        }
    }
    func toDict() -> [String: Any] {
        return [Constant.kgroupindex: self.groupindex,
                Constant.kgroupName: self.groupName,
                Constant.kindividualValues: self.arrOfIdividualValues,
                Constant.kgroupValues: arrOfGroupValues.map({$0.toDict()})]
    }
}

class GroupValue: NSObject {

    var groupNumber   = ""
    var groupValue    = 0
    
    init(dict: Any) {
        super.init()
        
        if let d = dict as? [String: Any] {
            self.groupNumber = (d[Constant.kgroupText] as? String ?? "")
            self.groupValue = (d[Constant.kgroupValue] as? Int ?? 0)
        }
    }
    func toDict() -> [String: Any] {
        return [Constant.kgroupText: self.groupNumber,
                Constant.kgroupValue: self.groupValue]
    }
}
