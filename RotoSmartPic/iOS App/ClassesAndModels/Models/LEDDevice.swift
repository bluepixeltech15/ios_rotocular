//
//  LEDDevice.swift
//  RotoSmartPic
//
//  Created by BAPS on 10/02/21.
//  Copyright © 2021 Bluepixel Technologies. All rights reserved.
//

import UIKit


public class LEDDevice: NSObject {
    
    static let shared = LEDDevice()
    
    public var arrOfLEDIndexAndValues = [LEDIndexAndValue]()
    
    public typealias GetLEDIndexValue = ((_ isSuccess: Bool, _ message: String) -> Void)
    private var getLEDIndexValue: GetLEDIndexValue?
    
    public var commandQueue: [Data] = [Data]()
    private var commandTimer:Timer?
    
    override init() {
        super.init()
    }
    
    class func writePassword(completion: @escaping ((_ isSuccess: Bool, _ message: String) -> Void)) {
        
        var password = "BLS@1233"
        let pass = Data([0x01] + password.toBytes())
        logfile.appendNewText(text: "Writing password 0x\(pass.hexEncodedString())")
        
        connectedLEDDevice?.writeValueIn(characteristicUUID: Command.CHAR_LED_WRITE_UUID, value: pass, complition: { (isWriten, message, charstic, device) in
            
            logfile.appendNewText(text: "Writing password \(isWriten ? "Success":"Fail"), error: \(message)")
            connectedLEDDevice = device
            completion(isWriten, message)
        })
    }
    class func getAllLEDValues(completion: @escaping ((_ isSuccess: Bool, _ message: String, _ arrOfLEDValues: [Int]) -> Void)) {
        
        logfile.appendNewText(text: "Writing getAllLEDValues 0x\(Command.allLEDsValues)")
        connectedLEDDevice?.writeValueIn(characteristicUUID: Command.CHAR_LED_WRITE_UUID, value: [Command.allLEDsValues].toData(), complition: { (isWriten, message, charstic, device) in
            
            logfile.appendNewText(text: "allLEDsValues Write \(isWriten ? "Success":"Fail"), Error: \(message)")
            print("allLEDsValues Write \(isWriten ? "Success":"Fail"), Message: \(message)")
        })
        
        connectedLEDDevice?.getUpdatedCharacteristic({ (charstic, device) in
            
            logfile.appendNewText(text: "getUpdatedCharacteristic charstic: \(charstic.uuid ?? "UUID") hexValue: 0x\(charstic.hexValue ?? "0x00")")
            if let bytes = charstic.byteArray, bytes.count > 13 {
                
                if bytes.first == Command.responseSuccess && bytes[1] == Command.allLEDsValues {
                    
                    logfile.appendNewText(text: "All LED Value matched hexValue: 0x\(charstic.hexValue ?? "0x00")")
                    print("LED 1  => \(bytes[2].toInt)")
                    print("LED 2  => \(bytes[3].toInt)")
                    print("LED 3  => \(bytes[4].toInt)")
                    print("LED 4  => \(bytes[5].toInt)")
                    print("LED 5  => \(bytes[6].toInt)")
                    print("LED 6  => \(bytes[7].toInt)")
                    print("LED 7  => \(bytes[8].toInt)")
                    print("LED 8  => \(bytes[9].toInt)")
                    print("LED 9  => \(bytes[10].toInt)")
                    print("LED 10 => \(bytes[11].toInt)")
                    print("LED 11 => \(bytes[12].toInt)")
                    print("LED 11 => \(bytes[13].toInt)")
                    
                    let arrOfLEDValues = [bytes[2].toInt, bytes[3].toInt, bytes[4].toInt, bytes[5].toInt,
                                          bytes[6].toInt, bytes[7].toInt, bytes[8].toInt, bytes[9].toInt,
                                          bytes[10].toInt, bytes[11].toInt, bytes[12].toInt, bytes[13].toInt]
                    
                    logfile.appendNewText(text: "All LED Values: \(arrOfLEDValues)")
                    
                    completion(true, "allLEDsValues got successfully", arrOfLEDValues)
                }
            }
        })
    }
    class func LED(isON: Bool, completion: @escaping ((_ isSuccess: Bool, _ message: String) -> Void)) {
        
        let ledValue = isON ? [Command.LEDsOn]:[Command.LEDsOff]
        logfile.appendNewText(text: "LED is ON => \(isON) Writing")
        connectedLEDDevice?.writeValueIn(characteristicUUID: Command.CHAR_LED_WRITE_UUID, value: ledValue.toData(), complition: { (isWriten, message, charstic, device) in
            
            print("LED is ON => \(isON) \(isWriten ? "Success":"Fail"), Message: \(message)")
            logfile.appendNewText(text: "LED is ON => \(isON) \(isWriten ? "Success":"Fail"), message: \(message)")
        })
    }
    
    //
    //    class func setLEDsValues(LEDIndexAndValues: [LEDIndexAndValue], completion: @escaping ((_ isSuccess: Bool, _ message: String) -> Void)) {
    //
    //        var arrOfLEDIndexAndValues = LEDIndexAndValues
    //
    //        func setLEDValue(LEDIndexAndValue: LEDIndexAndValue) {
    //
    //            let firstByte = (LEDIndexAndValue.index + 3) // Add 3 beacause of 1st LED byte is 0x03 and the array LED index start with 0
    //            let secByte = LEDIndexAndValue.value
    //            let ledValue = (firstByte.intToByteArray(length: 1) + secByte.intToByteArray(length: 1)).toData() // first byte is indicate LED no which want to change value and sec byte is value of LED
    //
    //            logfile.appendNewText(text: "LED Value writing LEDNo: \(firstByte), LEDValue: \(secByte), Complete Hex: 0x\(ledValue.hexEncodedString())")
    //
    //            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
    //                connectedLEDDevice?.writeValueIn(characteristicUUID: Command.CHAR_LED_WRITE_UUID, value: ledValue, complition: { (isWriten, message, charstic, device) in
    //
    //                    print("setLEDValue Write \(isWriten ? "Success":"Fail"), Message: \(message)")
    //                    logfile.appendNewText(text: "setLEDValue Write \(isWriten ? "Success":"Fail"), Message: \(message)")
    //
    //                    //Just added
    //                    if arrOfLEDIndexAndValues.count > 0 {
    //                    arrOfLEDIndexAndValues.removeFirst()
    //                    }
    //
    //                    if arrOfLEDIndexAndValues.count > 0 {
    //                        setLEDValue(LEDIndexAndValue: arrOfLEDIndexAndValues.first!)
    //                    } else {
    //                        completion(true, message)
    //                    }
    //                })
    //            }
    //        }
    //        if arrOfLEDIndexAndValues.count > 0 {
    //            setLEDValue(LEDIndexAndValue: arrOfLEDIndexAndValues.first!)
    //        }
    //    }
    
    
    func setLEDsValues(LEDIndexAndValues: [LEDIndexAndValue], completion: @escaping GetLEDIndexValue) {
   
        self.getLEDIndexValue = completion
        arrOfLEDIndexAndValues += LEDIndexAndValues
        if arrOfLEDIndexAndValues.count > 0 {
            print("setLEDsValues")
            self.setLEDValue(LEDIndexAndValue: arrOfLEDIndexAndValues.first!)
        }
    }
    
    func setLEDValue(LEDIndexAndValue: LEDIndexAndValue) {
        print(Date().timeIntervalSince1970)
        let firstByte = (LEDIndexAndValue.index + 3) // Add 3 beacause of 1st LED byte is 0x03 and the array LED index start with 0
        let secByte = LEDIndexAndValue.value
        let ledValue = (firstByte.intToByteArray(length: 1) + secByte.intToByteArray(length: 1)).toData() // first byte is indicate LED no which want to change value and sec byte is value of LED
        print("Index: \(LEDIndexAndValue.index): \(LEDIndexAndValue.value)")
        self.commandQueue.append(ledValue)
        logfile.appendNewText(text: "LED Value writing LEDNo: \(firstByte), LEDValue: \(secByte), Complete Hex: 0x\(ledValue.hexEncodedString())")
        
    }
    
    public func startCMDTimer(timeInterval: Double){
        
        if commandTimer == nil {
            self.commandTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(self.checkCMDTimeFinish), userInfo: nil, repeats: true)
        }
    }
    public func stopCMDTimer(){
        
        self.commandQueue.removeAll()
        self.commandTimer?.invalidate()
    }
    @objc func checkCMDTimeFinish(){
        
        self.writeOneByOneCommand()
    }
    
    func writeOneByOneCommand() {
        
        if self.commandQueue.count > 0 {
            
            if let param = self.commandQueue.first {
            //print("Date: \(Date())")
                print("LedCommand fire : \(Command.CHAR_LED_WRITE_UUID) value:\(param)")
                connectedLEDDevice?.writeValueIn(characteristicUUID: Command.CHAR_LED_WRITE_UUID, value: param, complition: { (isWriten, message, charstic, device) in
                    
                    print("setLEDValue Write \(isWriten ? "Success":"Fail"), Message: \(message)")
                    logfile.appendNewText(text: "setLEDValue Write \(isWriten ? "Success":"Fail"), Message: \(message)")
                    
                    //Just added
                    if self.arrOfLEDIndexAndValues.count > 0 {
                        self.arrOfLEDIndexAndValues.removeFirst()
                    }
                    if self.commandQueue.count > 0 {
                        print("Remove Process: \(self.commandQueue.count)")
                        self.commandQueue.removeFirst()
                    }
                    if self.arrOfLEDIndexAndValues.count > 0 {
                        self.setLEDValue(LEDIndexAndValue: self.arrOfLEDIndexAndValues.first!)
                    } else {
                        self.getLEDIndexValue!(true, message)
                    }
                })
            }
        }
    }
}
