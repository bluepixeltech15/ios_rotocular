//
//  CustomView.swift
//  CustomClasses
//
//  Created by bluepixel on 4/20/18.
//  Copyright © 2018 bluepixel. All rights reserved.
//

import UIKit
import Foundation

@IBDesignable
class AngleView: UIView {

    @IBInspectable var BordeHeight: CGFloat = 0.0 {
        didSet {
            layer.borderWidth = BordeHeight
        }
    }
    @IBInspectable var BordeColor: UIColor = .clear {
        didSet {
            layer.borderColor = BordeColor.cgColor
        }
    }
    
    @IBInspectable var angle: Int = 0 {
        didSet {
            
            for lay in layer.sublayers ?? [CALayer]() {
                if lay is CAShapeLayer {
                    lay.removeFromSuperlayer()
                }
            }
            drawAngleLine(degree: angle)
        }
    }
    
    @IBInspectable var isSingleAngle: Bool = true
    
    @IBInspectable var multiAngles: [Int] = [0] {
        didSet {
            for lay in layer.sublayers ?? [CALayer]() {
                if lay is CAShapeLayer {
                    lay.removeFromSuperlayer()
                }
            }
            for ang in multiAngles {
                drawAngleLine(degree: ang)
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = (layer.frame.size.width) / 2
        layer.masksToBounds = true
        
        print("self Frame \(frame)")
        
        if isSingleAngle {
            
            let angle0 = UILabel(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
            angle0.text = "0"
            angle0.textAlignment = .center
            angle0.font = UIFont.systemFont(ofSize: 5, weight: .regular)
            angle0.center.x = frame.width / 2
            angle0.frame.origin.y = self.frame.height - (BordeHeight + angle0.frame.height)
            self.addSubview(angle0)
            print("angle0 Frame \(angle0.frame)")
            
            let angle90 = UILabel(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
            angle90.text = "90"
            angle90.font = UIFont.systemFont(ofSize: 5, weight: .regular)
            angle90.center.y = (frame.height / 2) + 2.5
            angle90.frame.origin.x = BordeHeight + 2
            angle90.sizeToFit()
            self.addSubview(angle90)
            print("angle90 Frame \(angle90.frame)")
            
            let angle180 = UILabel(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
            angle180.text = "180"
            angle180.textAlignment = .center
            angle180.font = UIFont.systemFont(ofSize: 5, weight: .regular)
            angle180.center.x = angle0.center.x
            angle180.frame.origin.y = BordeHeight
            self.addSubview(angle180)
            print("angle180 Frame \(angle180.frame)")
            
            
            let angle270 = UILabel(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
            angle270.text = "270"
            angle270.font = UIFont.systemFont(ofSize: 5, weight: .regular)
            angle270.center.y = (frame.height / 2) + 2.5
            angle270.frame.origin.x = self.frame.width - (BordeHeight + 1 + angle270.frame.width)
            angle270.sizeToFit()
            self.addSubview(angle270)
            print("angle270 Frame \(angle270.frame)")
        }
    }
    private func drawAngleLine(degree: Int) {
        
        let radius = Double(frame.height / 2)
        
        let mX = Double(frame.width / 2)
        let mY = Double(frame.height / 2)
        
        let mAngle = degree - 270
        print("newAngel \(mAngle)")
        let radians = Double(mAngle) * Double.pi / 180
        
        let pX = (mX + radius * cos(radians))
        let pY = (mY + radius * sin(radians))
        
        let start = CGPoint(x: mX, y: mY)
        let endPoint = CGPoint(x: pX, y: pY)
        
        drawDashLine(start: start, end: endPoint, view: self, color: BordeColor)
    }
    private func drawDashLine(start p0: CGPoint, end p1: CGPoint, view: UIView, color: UIColor) {
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = 1.5
        shapeLayer.lineDashPattern = [5, 0] // 2 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
}
