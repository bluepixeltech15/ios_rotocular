//
//  CustomView.swift
//  CustomClasses
//
//  Created by bluepixel on 4/20/18.
//  Copyright © 2018 bluepixel. All rights reserved.
//

import UIKit

@IBDesignable
class CustomSlider: UISlider {
    
    @IBInspectable var thumbImage: UIImage? = nil {
        didSet {
            if let thumb = thumbImage {
                setThumbImage(thumb, for: .normal)
            }
        }
    }
}
