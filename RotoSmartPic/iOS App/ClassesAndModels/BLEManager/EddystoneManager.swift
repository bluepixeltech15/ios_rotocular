//
//  BleMaster.swift
//  PromoBlingFramework
//
//  Created by bluepixel on 23/11/18.
//  Copyright © 2018 bluepixel. All rights reserved.
//

import UIKit
import CoreBluetooth

private enum FrameType {
    
    case FrameTypeEID
    case FrameTypeUID
    case FrameTypeTLM
    case FrameTypeURL
    case FrameTypeUNKNOWN

}

public class EddystoneManager: NSObject {
    
    //MARK:-  EddystoneFrameType
    private let EddystoneFrameTypeUID: UInt8 = 0x00
    private let EddystoneFrameTypeURL: UInt8 = 0x10
    private let EddystoneFrameTypeTLM: UInt8 = 0x20
    private let EddystoneFrameTypeEID: UInt8 = 0x30
    
    private var advData = [String: Any]()
    private var frameData = NSData()
    
    public var EddystoneURL: EddystoneURLModel?
    public var EddystoneEID: EddystoneEIDModel?
    public var EddystoneTLM: EddystoneTLMModel?
    public var EddystoneUID: EddystoneUIDModel?
    
    //MARK:- Usable Variables
    private var frameType: FrameType?
    
    //MARK:-  Functions
    public class func isEddystone(advertisementData: [String: Any]) -> Bool {
        if (advertisementData[CBAdvertisementDataServiceDataKey] != nil) {
            if (advertisementData[CBAdvertisementDataServiceDataKey] as! [CBUUID: Any])[CBUUID(string: "FEAA")] != nil {
                return true
            }
        }
        return false
    }
    init(advertisementData: [String: Any]) {
        super.init()
        
        if (advertisementData[CBAdvertisementDataServiceDataKey] != nil) {
            if (advertisementData[CBAdvertisementDataServiceDataKey] as! [CBUUID: Any])[CBUUID(string: "FEAA")] != nil {
                
                advData = advertisementData
                setFrameType()
            }
        }
    }
    
    //set Frame type of eddystone
    private func setFrameType() {
        
        if let frameData = (advData[CBAdvertisementDataServiceDataKey] as! [CBUUID: Any])[CBUUID(string: "FEAA")] as? NSData {
            
            self.frameData = frameData
            if frameData.length > 1 {
                
                let count = frameData.length
                var frameBytes = [UInt8](repeating: 0, count: count)
                frameData.getBytes(&frameBytes, length: count)
                
                if frameBytes[0] == EddystoneFrameTypeUID {
                    frameType = .FrameTypeEID
                    SetEddystoneEIDData()
                } else if frameBytes[0] == EddystoneFrameTypeTLM {
                    frameType = .FrameTypeTLM
                    setEddystoneTLMData()
                } else if frameBytes[0] == EddystoneFrameTypeEID {
                    frameType = .FrameTypeEID
                    SetEddystoneEIDData()
                } else if frameBytes[0] == EddystoneFrameTypeURL {
                    frameType = .FrameTypeURL
                    SetEddystoneURLData()
                } else {
                    frameType = .FrameTypeUNKNOWN
                }
            }
        } else {
            
            if let manufactureData = advData[CBAdvertisementDataManufacturerDataKey] as? Data {
                
                print("Manufacturer Data : \(manufactureData)")
            }
        }
    }
    //MARK:-  Eddystone URL Data
    private func SetEddystoneURLData() {
        
        let count = frameData.count
        
        var frameBytes = [UInt8](repeating: 0, count: count)
        
        let txPower = Int(Int8(bitPattern: frameData[1]))
        var url = ""
        
        if let URLPrefix = URLPrefixFromByte(schemeID: frameBytes[2]) {
            
            url = URLPrefix
            for i in 3..<frameBytes.count {
                if let encoded = encodedStringFromByte(charVal: frameBytes[i]) {
                    url.append(encoded)
                }
            }
        }
        EddystoneURL = EddystoneURLModel(txPower: txPower, url: url)
        
    }
    //get url prefix string
    private func URLPrefixFromByte(schemeID: UInt8) -> String? {
        
        switch schemeID {
            
        case 0x00:
            return "http://www."
        case 0x01:
            return "https://www."
        case 0x02:
            return "http://"
        case 0x03:
            return "https://"
        default:
            return nil
        }
    }
    //get url last extension
    private func encodedStringFromByte(charVal: UInt8) -> String? {
        switch charVal {
        case 0x00:
            return ".com/"
        case 0x01:
            return ".org/"
        case 0x02:
            return ".edu/"
        case 0x03:
            return ".net/"
        case 0x04:
            return ".info/"
        case 0x05:
            return ".biz/"
        case 0x06:
            return ".gov/"
        case 0x07:
            return ".com"
        case 0x08:
            return ".org"
        case 0x09:
            return ".edu"
        case 0x0a:
            return ".net"
        case 0x0b:
            return ".info"
        case 0x0c:
            return ".biz"
        case 0x0d:
            return ".gov"
        default:
            return String(data: Data(bytes: [ charVal ] as [UInt8], count: 1), encoding: .utf8)
        }
    }
    //MARK:-  Eddystone EID Data
    private func SetEddystoneEIDData() {
    
        if frameData.count > 0 {
            
            let count = frameData.length
            var frameBytes = [UInt8](repeating: 0, count: count)
            frameData.getBytes(&frameBytes, length: count)
            let TXPower = Int(Int8(bitPattern:frameBytes[1]))
            let beaconID: [UInt8] = Array(frameBytes[2..<10])
            EddystoneEID = EddystoneEIDModel(txPower: TXPower, beaconID: hexBeaconID(beaconID: beaconID))
        }
    }
    //MARK:-  setEddystone TLM Data
    private func setEddystoneTLMData() {
        
        if frameData.length == 14 {
            
            var batteryVoltage: UInt16 = 0
            var beaconTemperature: UInt16 = 0
            var advPDUCount: UInt32 = 0
            var timeSinceReboot: UInt32 = 0
            
            frameData.getBytes(&batteryVoltage, range: NSMakeRange(2, 2))
            frameData.getBytes(&beaconTemperature, range: NSMakeRange(4, 2))
            frameData.getBytes(&advPDUCount, range: NSMakeRange(6, 4))
            frameData.getBytes(&timeSinceReboot, range: NSMakeRange(10, 4))
            
            let count = frameData.length
            var frameBytes = [UInt8](repeating: 0, count: count)
            let TXPower = Int(Int8(bitPattern:frameBytes[1]))
            batteryVoltage = CFSwapInt16BigToHost(batteryVoltage)
            beaconTemperature = CFSwapInt16BigToHost(beaconTemperature)
            
            /// Convert temperature from 8.8 fixed point notation.
            let realBeaconTemperature: Double = Double(beaconTemperature) / 256.0
            advPDUCount = CFSwapInt32BigToHost(advPDUCount)
            timeSinceReboot = CFSwapInt32BigToHost(timeSinceReboot)
            
            EddystoneTLM = EddystoneTLMModel(batteryVoltage: Int(batteryVoltage), txPower: TXPower, temperature: realBeaconTemperature, packetCount: Int(advPDUCount), time: Int(timeSinceReboot))
            
        }
    }
    //MARK:-  setEddystone UID Data
    private func setEddystoneUIDData() {
        
        if frameData.length == 14 {
            
            let count = frameData.length
            
            var frameBytes = [UInt8](repeating: 0, count: count)
            frameData.getBytes(&frameBytes, length: count)
            
            let TXPower = Int(Int8(bitPattern:frameBytes[1]))
            let namespaceID = hexBeaconID(beaconID: Array(frameBytes[2..<12]))
            let instanceID = hexBeaconID(beaconID: Array(frameBytes[12..<18]))
            
            EddystoneUID = EddystoneUIDModel(txPower: TXPower, nameSpaceID: namespaceID, instanceID: instanceID)
        }
    }
    private func hexBeaconID(beaconID: [UInt8]) -> String {
        var retval = ""
        for byte in beaconID {
            var s = String(byte, radix:16, uppercase: false)
            if s.count == 1 {
                s = "0" + s
            }
            retval += s
        }
        return retval
    }
    
    public func isEddystoneURL() -> Bool {
        return frameType == FrameType.FrameTypeURL
    }
    public func isEddystoneEID() -> Bool {
        return frameType == FrameType.FrameTypeEID
    }
    public func isEddystoneUID() -> Bool {
        return frameType == FrameType.FrameTypeUID
    }
    public func isEddystoneTLM() -> Bool {
        return frameType == FrameType.FrameTypeTLM
    }
    
}
public class EddystoneURLModel {
    
    var txPower: Int = -59
    var url: String = ""
    
    init(txPower: Int?, url: String?) {
        
        self.txPower = txPower ?? -59
        self.url = url ?? ""
    }
}

public class EddystoneEIDModel {
    
    var txPower: Int = -59
    var beaconID: String = ""
    
    init(txPower: Int?, beaconID: String?) {
        
        self.txPower = txPower ?? -59
        self.beaconID = beaconID ?? ""
    }
}

public class EddystoneTLMModel {
    
    var batteryVoltage: Int = 0
    var txPower: Int = -59
    var temperature: Double = 0.0
    var advPacketCount: Int = 0
    var time: Int = 0
    
    init(batteryVoltage: Int?, txPower: Int?, temperature: Double?, packetCount: Int?, time: Int?) {
        
        self.batteryVoltage = batteryVoltage ?? 0
        self.txPower = txPower ?? -59
        self.temperature = temperature ?? 0.0
        self.advPacketCount = packetCount ?? 0
        self.time = time ?? 0
        
    }
}

public class EddystoneUIDModel {
    
    var txPower: Int = -59
    var nameSpaceID: String = ""
    var instanceID: String = ""

    init(txPower: Int?, nameSpaceID: String?, instanceID: String?) {
        
        self.txPower = txPower ?? -59
        self.nameSpaceID = nameSpaceID ?? ""
        self.instanceID = instanceID ?? ""
    }
}

