//
//  GlobalKeys.swift
//  BLEUArtDemo
//
//  Created by bluepixel on 21/08/18.
//  Copyright © 2018 bluepixel. All rights reserved.
//

import Foundation

public let kadvertisementData  = "advetisementdata"
public  let krssi               = "rssi"
public let kperipheral         = "peripheral"

public let kstoredbeacon = "storebeacon"

public let DictOfservices =   [ "1811" : "ALERT NOTIFICATION SERVICE",
                                "1815" : "AUTOMATION IO",
                                "180F" : "BATTERY SERVICE",
                                "1810" : "BLOOD PRESSURE",
                                "181B" : "BODY COMPOSITION",
                                "181E" : "BOND MANAGEMENT",
                                "181F" : "CONTINUOUS GLUCOSE MONITORING",
                                "1805" : "CURRENT TIME SERVICE",
                                "1818" : "CYCLING POWER",
                                "1816" : "CYCLING SPEED AND CADENCE",
                                "180A" : "DEVICE INFORMATION",
                                "181A" : "ENVIRONMENTAL SENSING",
                                "1800" : "GENERIC ACCESS",
                                "1801" : "GENERIC ATTRIBUTE",
                                "1808" : "GLUCOSE",
                                "1809" : "HEALTH THERMOMETER",
                                "180D" : "HEART RATE",
                                "1823" : "HTTP PROXY",
                                "1812" : "HUMAN INTERFACE DEVICE",
                                "1802" : "IMMEDIATE ALERT",
                                "1821" : "INDOOR POSITIONING",
                                "1820" : "INTERNET PROTOCOL SUPPORT",
                                "1803" : "LINK LOSS",
                                "1819" : "LOCATION AND NAVIGATION",
                                "1807" : "NEXT DST CHANGE SERVICE",
                                "1825" : "OBJECT TRANSFER",
                                "180E" : "PHONE ALERT STATUS SERVICE",
                                "1822" : "PULSE OXIMETER",
                                "1806" : "REFERENCE TIME UPDATE SERVICE",
                                "1814" : "RUNNING SPEED AND CADENCE",
                                "1813" : "SCAN PARAMETERS",
                                "1824" : "TRANSPORT DISCOVERY",
                                "1804" : "TX POWER",
                                "181C" : "USER DATA",
                                "181D" : "WEIGHT SCALE",
                                "00001530-1212-EFDE-1523-785FEABCD123" : "LEGACY DFU SERVICE",
                                "6E400001-B5A3-F393-E0A9-E50E24DCCA9E" : "NORDIC UART SERVICE"]


public let DictOfCharacteristic =  ["1811" : "ALERT NOTIFICATION SERVICE",
                                    "1815" : "AUTOMATION IO",
                                    "180F" : "BATTERY SERVICE",
                                    "1810" : "BLOOD PRESSURE",
                                    "181B" : "BODY COMPOSITION",
                                    "181E" : "BOND MANAGEMENT",
                                    "181F" : "CONTINUOUS GLUCOSE MONITORING",
                                    "1805" : "CURRENT TIME SERVICE",
                                    "1818" : "CYCLING POWER",
                                    "1816" : "CYCLING SPEED AND CADENCE",
                                    "180A" : "DEVICE INFORMATION",
                                    "181A" : "ENVIRONMENTAL SENSING",
                                    "1800" : "GENERIC ACCESS",
                                    "1801" : "GENERIC ATTRIBUTE",
                                    "1808" : "GLUCOSE",
                                    "1809" : "HEALTH THERMOMETER",
                                    "180D" : "HEART RATE",
                                    "1823" : "HTTP PROXY",
                                    "1812" : "HUMAN INTERFACE DEVICE",
                                    "1802" : "IMMEDIATE ALERT",
                                    "1821" : "INDOOR POSITIONING",
                                    "1820" : "INTERNET PROTOCOL SUPPORT",
                                    "1803" : "LINK LOSS",
                                    "1819" : "LOCATION AND NAVIGATION",
                                    "1807" : "NEXT DST CHANGE SERVICE",
                                    "1825" : "OBJECT TRANSFER",
                                    "180E" : "PHONE ALERT STATUS SERVICE",
                                    "1822" : "PULSE OXIMETER",
                                    "1806" : "REFERENCE TIME UPDATE SERVICE",
                                    "1814" : "RUNNING SPEED AND CADENCE",
                                    "1813" : "SCAN PARAMETERS",
                                    "1824" : "TRANSPORT DISCOVERY",
                                    "1804" : "TX POWER",
                                    "181C" : "USER DATA",
                                    "181D" : "WEIGHT SCALE"]
