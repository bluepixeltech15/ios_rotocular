//
//  BleMaster.swift
//  PromoBlingFramework
//
//  Created by bluepixel on 23/11/18.
//  Copyright © 2018 bluepixel. All rights reserved.
//

import UIKit
import CoreBluetooth
import UserNotifications

let blemanager = BLEManager()
let kAppName = "RotoSmartPic"

public class BLEManager: NSObject {
    
    //MARK:-  Variables
    private var cbManager: CBCentralManager!
    private let beaconOperationsQueue = DispatchQueue(label: "beacon_operations_queue")
    public var isScanningStart = false
    
    public var IsShowAlertWhenBluetoothIsOFF = false
    public var IsBluetoothOn = true
    
    private var arrOfBLEDevices = [BLEDevice]()

    private var timer: Timer!
    private var timerLostBeacon: Timer!
    private var services = [CBUUID]()

    private var seenEddystoneCache = [String : [String : AnyObject]]()
    
    public typealias complitionBLock = ((BLEDevice) -> Void)
    private var complition: complitionBLock!
    
    public typealias connectComplitionBLock = ((Bool, CBPeripheral, Error?) -> Void)
    private var connectComplition: connectComplitionBLock!
    private var connectablePeripheral: CBPeripheral?
    
    typealias disConnectComplitionBlock = (Bool) -> Void
    private var disconnectCompletion: disConnectComplitionBlock!
    
    typealias AutoDisconnect = (BLEDevice) -> Void
    private var autoDisconnect: AutoDisconnect?
    
    public typealias OutRangecomplitionBLock = ((BLEDevice) -> Void)
    private var outRangeComplition: OutRangecomplitionBLock!
    
    override init() {
        super.init()

        //        let option = [CBCentralManagerOptionShowPowerAlertKey: true]
        cbManager = CBCentralManager(delegate: self, queue: beaconOperationsQueue, options: [CBCentralManagerOptionRestoreIdentifierKey: kAppName])
        
        //Calculate RSSIs and give the Update to Front end side
        timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(getAccurateRssiBeacons), userInfo: nil, repeats: true)
        
        //To consider beacon out range call it every 3 seconds
        timerLostBeacon = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(ConsiderBeaconOutRange), userInfo: nil, repeats: true)
        
    }
    
    //MARK:-  Usable Fuctions
    // TO start scanning Ble Devices
    public func StartScanning(WithServices: [String]?, mycomplition: @escaping complitionBLock) {
    
//        arrOfBLEDevices.removeAll()
        self.services.removeAll()
        if let services = WithServices {
            for uuid in services {
                self.services.append(CBUUID(string: uuid))
            }
        }
        
        self.complition = mycomplition
        self.perform(#selector(self.StartScan), with: nil, afterDelay: 1.0)
    }
    
    @objc private func StartScan() {
    
        if isScanningStart == false {
            
            ClearAllArays()
            if IsBluetoothOn {
    
                if cbManager == nil {
                    
                    cbManager = CBCentralManager(delegate: self, queue: beaconOperationsQueue, options: [CBCentralManagerOptionRestoreIdentifierKey: kAppName])
                }
                let option = [CBCentralManagerScanOptionAllowDuplicatesKey: true]
                cbManager.delegate = self
                cbManager.scanForPeripherals(withServices: services, options: option)
                isScanningStart = true
            }
        }
    }
    
    // To Clear all variables beacuse when we rescan Ble devices so its not add duplicate devices
    private func ClearAllArays() {
        
        arrOfBLEDevices.removeAll()
    }
    public func connect(peripheral: CBPeripheral, myComplition: @escaping connectComplitionBLock) {
        
        connectComplition = myComplition
        connectablePeripheral = peripheral
        cbManager.connect(peripheral, options: nil)
    }
    func disConnect(device:CBPeripheral, myComplition: @escaping disConnectComplitionBlock) {
        
        self.cbManager.cancelPeripheralConnection(device)
        disconnectCompletion = myComplition
    }
    func getDisconnectedDevice(myComplition: @escaping AutoDisconnect) {
        
        autoDisconnect = myComplition
    }
    public func retrivePeripheral(device: BLEDevice,complition: ((BLEDevice) -> Void)) {
        
        let peripherals = cbManager.retrievePeripherals(withIdentifiers: [UUID(uuidString: device.uuid)!])
        
        if peripherals.count == 1 {
            device.peripheral = peripherals[0]
            complition(device)
        }
    }
    public func StopScan() {
        
        ClearAllArays()
        cbManager.stopScan()
        isScanningStart = false
    }
    
    //MARK:-  Custom Functions
    // Called when the bluetooth is off to display message to user
    private func ShowMessageDialougue(message: String) {
        
        let alert = UIAlertController(title: kAppName, message: message, preferredStyle: .alert)
    
        let okaction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(okaction)
        
        DispatchQueue.main.async { () -> Void in
            
            if #available(iOS 13.0, *) {
                
                let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
                if var topController = keyWindow?.rootViewController {
                    while let presentedViewController = topController.presentedViewController {
                        
                        topController = presentedViewController
                    }
                    topController.hideHUD()
                    topController.present(alert, animated: true){
                        topController.hideHUD()
                        NotificationCenter.default.post(name: Notification.Name("bleState"), object: false, userInfo: nil)
                        //NotificationCenter.default.addObserver(self, selector: #selector(self.BLEState(notification:)), name: Notification.Name("bleState"), object: nil)
                    }
                }
            } else {
                if var topController = UIApplication.shared.keyWindow?.rootViewController {
                    while let presentedViewController = topController.presentedViewController {
                        topController = presentedViewController
                    }
                    topController.hideHUD()
                    topController.present(alert, animated: true) {
                        topController.hideHUD()
                        NotificationCenter.default.post(name: Notification.Name("bleState"), object: false, userInfo: nil)
                        //blemanager.StopScan()
                    }
                }
            }
        }

//        DispatchQueue.main.async { () -> Void in
//        UIApplication.shared.delegate?.window!?.rootViewController?.present(alert, animated: false, completion: nil)
//        }
    }
    // this method is call every 1 minute to get a accurate rssi value of Ble Device
    @objc private func getAccurateRssiBeacons() {
        
        for device in arrOfBLEDevices {
            device.arrOfRSSI = device.arrOfRSSI.sorted()
           
            if device.arrOfRSSI.count > 15 {
            
                let upperDeleteCount = (device.arrOfRSSI.count * 20) / 100
                let lowerDeleteCount = (device.arrOfRSSI.count * 20) / 100
                
                for _ in 0..<upperDeleteCount {
                    device.arrOfRSSI.removeFirst()
                }
                
                for _ in 0..<lowerDeleteCount {
                    device.arrOfRSSI.removeLast()
                }
            }
            
            let totalRssis = device.arrOfRSSI.reduce(0, +)
            device.avgRSSI = totalRssis / device.arrOfRSSI.count
            let newDevice = calculateDistanceInMeter(device: device)
    
            complition(newDevice)
        }
    }
    public func getOutRangeDevice(mycomplition: @escaping OutRangecomplitionBLock) {
        
        outRangeComplition = mycomplition
    }
    //Check device is out range or not
    @objc private func ConsiderBeaconOutRange() {
        
        for device in arrOfBLEDevices {

            device.count += 1
            device.inRange = false

            if device.count >= 5 && device.inRange == false {
                
                arrOfBLEDevices.removeAll { (d) -> Bool in
                    return device.uuid == d.uuid
                }
                if outRangeComplition != nil {
                    outRangeComplition(device)
                }
            }
        }
    }
    
    //Calculate distance in meter on rssi and txpower
    private func calculateDistanceInMeter(device: BLEDevice) -> BLEDevice {
        
        var txPower = -59
        if device.isEddystone() {
            
            if (device.Eddystone?.isEddystoneEID())! {
                txPower = (device.Eddystone?.EddystoneEID!.txPower)!
            } else if (device.Eddystone?.isEddystoneTLM())! {
                txPower = (device.Eddystone?.EddystoneTLM!.txPower)!
            } else if (device.Eddystone?.isEddystoneUID())! {
                txPower = (device.Eddystone?.EddystoneUID!.txPower)!
            } else if (device.Eddystone?.isEddystoneURL())! {
                txPower = (device.Eddystone?.EddystoneURL!.txPower)!
            }
            
        } else {
            
            //because in another types of beaconse no has txpower so we set it -59 by default
            txPower = -59
        }
        
        //Distance = 10 ^ ((Measured Power – RSSI)/(10 * N))
        let strDistance = calculatorDistance(dividValue: -60, avarageRssi: device.avgRSSI)
        device.distanceInMeter = Float(strDistance)
        
        return device
    }
    private func calculatorDistance(dividValue: Int, avarageRssi: Int) -> String {
        
        var accuracy = 0.0
        
        if (avarageRssi == 0) {
            accuracy = -1.0 // if we cannot determine accuracy, return -1.
        }
        
        let ratio = Double(avarageRssi) * 1.0 / Double(dividValue)
        if (ratio < 1.0) {
            accuracy = pow(ratio, 10)
        } else {
            accuracy = (0.89976) * pow(ratio, 7.7095) + 0.111
        }
        
        var distanceFloatDisplay = "0.0"
        //print("actualyValue:->\(accuracy)")
        if (accuracy < 1) {
            distanceFloatDisplay =  "\(String(format: "%.1f", Double(accuracy)))"
        } else if (accuracy > 1 && accuracy < 4) {
            distanceFloatDisplay = "\(String(format: "%.1f", Double(accuracy)))"
        } else {
            distanceFloatDisplay = "\(String(format: "%.1f", Double(accuracy)))"
        }
        
        
        return distanceFloatDisplay
    }
}

//MARK:-  CBCentralManagerDelegate Methods
extension BLEManager: CBCentralManagerDelegate {
    
    // For checking bluetooth is on or off
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        if central.state != .poweredOff {
            IsBluetoothOn = true
            
        } else {
            
            isScanningStart = false
            IsBluetoothOn = false
            ShowMessageDialougue(message: "Please 'turn on' Bluetooth to scan nearest devices")
        }
    }
    
    // To get nearest bleDevices
    public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
    
        let Device = BLEDevice(peripheral: peripheral, advertisementdata: advertisementData, Rssi: RSSI.intValue)
        
        //check peripheral is already available or not from the identifier
        //get already added Device index from array
        let peripheralindex = self.arrOfBLEDevices.firstIndex { (peri) -> Bool in
           return peri.uuid == Device.uuid
        }

        // Check index is found or not, if found then its update Ble device if not then its a new Ble Device and it add into array
        if (peripheralindex == nil) {
            
            self.arrOfBLEDevices.append(Device)
            self.complition(Device)
            
        } else {
    
            let d = self.arrOfBLEDevices[peripheralindex!]
            
            if d.arrOfRSSI.count > 15 {
               d.arrOfRSSI.remove(at: 0)
            }
            
            d.name = Device.name
            d.localName = Device.localName
            d.arrOfRSSI.append(Device.avgRSSI)
            
            d.count = 0
            
            self.arrOfBLEDevices[peripheralindex!] = d
        }
    }
    public func centralManager(_ central: CBCentralManager, willRestoreState dict: [String : Any]) {
        
//        print(dict)
    }
    public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        
        if peripheral.identifier == connectablePeripheral?.identifier {
            connectComplition(true, peripheral, nil)
        }
    }
    public func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        
        if peripheral.identifier == connectablePeripheral?.identifier {
            connectComplition(false, peripheral, error)
        }
    }
    public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        
        print(" DisconnectPeripheral :->\(peripheral)")
        print("didDisconnectPeripheral", peripheral.state)
        
        if disconnectCompletion != nil {
            disconnectCompletion(true)
        }
        if autoDisconnect != nil {
            
            let index = arrOfBLEDevices.firstIndex(where: { (device) -> Bool in
                return device.uuid.uppercased() == peripheral.identifier.uuidString.uppercased()
            })
            
            if let i = index {
                autoDisconnect?(arrOfBLEDevices[i])
                arrOfBLEDevices.remove(at: i)
            }
        }
    }
}
