//
//  GlobalKeys.swift
//  BLEConnect
//
//  Created by Nikhil Jobanputra on 01/07/20.
//  Copyright © 2020 Bluepixel Technology. All rights reserved.
//

import Foundation
import CoreBluetooth
import UIKit

//MARK:-  For Peripherals
public extension CBPeripheral {
    func getperipheralid() -> String {
        return identifier.uuidString.uppercased()
        
    }
    func Isconnected() -> Bool {
         return state == .connected
    }
}

//MARK:-  For AdvertisementData
public extension Dictionary where Key == String, Value == Any {
    
    func peripheralname() -> String? {

        return self[CBAdvertisementDataLocalNameKey] as? String
    }
    func IsConnectable() -> Bool {
        return (self[CBAdvertisementDataIsConnectable] as! Int == 1) ? true:false
    }
    func IsEddystone() -> Bool {
        if (self[CBAdvertisementDataServiceDataKey] != nil) {
            if (self[CBAdvertisementDataServiceDataKey] as! [CBUUID: Any])[CBUUID(string: "FEAA")] != nil {
                return true
            }
        }
        return false
    }
    func standardServiceName() -> String? {
        
        if let serviceIDS = self[CBAdvertisementDataServiceUUIDsKey] as? NSArray {
            for seviceid in serviceIDS {
                
                let key = DictOfservices.keys.filter({$0 == "\(seviceid)"})
                if key.count > 0 {
                    return DictOfservices[key[0]]
                }
            }
        }
        return nil
    }
}

//MARK:-  For String
public extension String {
    func isValidHexValue() -> Bool {
        
        let chars = CharacterSet(charactersIn: "0123456789ABCDEF").inverted
        guard uppercased().rangeOfCharacter(from: chars) == nil else {
            return false
        }
        return true
    }
    func trimString() -> String {
        return trimmingCharacters(in: .whitespacesAndNewlines)
    }
}

//MARK:-  For Characteristic
public extension CBService {
    
    func getUUIDString() -> String {
        
        return uuid.uuidString.uppercased()
    }
    func servicename() -> String {
        return "\(DictOfservices[uuid.uuidString.uppercased()] ?? "CUSTOM SERVICE")"
    }
}

//MARK:-  For Characteristic
public extension CBCharacteristic {
    
    func getUUIDString() -> String {
        
        return uuid.uuidString.uppercased()
    }
    func isContainReadProperty() -> Bool {
        return properties.contains(.read)
    }
    func isContainWriteProperty() -> Bool {
        return properties.contains(.write)
    }
    func isContainNotifyProperty() -> Bool {
        return properties.contains(.notify)
    }
    func getProperties() -> String {
    
        var strPermission = ""
        if properties.contains(.read) {
            strPermission = "Read "
        }
        if properties.contains(.write) {
            strPermission = strPermission + "Write "
        }
        if properties.contains(.notify) {
            strPermission = strPermission + "Notify"
        }
        return strPermission
    }
}

//MARK:-  For Data
extension Data {
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
    }
    
    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
        return map { String(format: format, $0) }.joined()
    }
}

//MARK:-  For UIViewController
public extension UIViewController {
    
}
