//
//  PromoblingDevice.swift
//  PromoBlingFramework
//
//  Created by bluepixel on 14/12/18.
//  Copyright © 2018 bluepixel. All rights reserved.
//

import UIKit
import CoreBluetooth

private enum DeviceType {
    
    case Eddystone
    case iBeacon
}
public class BLEDevice: NSObject {
    
    //MARK:-  Variables Declarations 
    public var peripheral: CBPeripheral!
    private var AdvData: [String: Any]!
    
    private var deviceType: DeviceType!
    
    //MARK:-  Variables for Eddystone URL
    public var Eddystone: EddystoneManager?
    
    public var inRange: Bool!
    public var count: Int!
    public var arrOfRSSI = [Int]()
    
    public var uuid: String!
    public var name: String?
    public var localName: String?
    public var beaconTypeName: String?
    public var standardServiceName: String?
    
    public var avgRSSI: Int!
    public var distanceInMeter: Float!
    
    public var serivces = [Service]()
    private var operationalCharacteristic: CBCharacteristic!
    
    public typealias operationComplitionBlock = ((Bool, String, Characterstic?, BLEDevice) -> Void)
    private var readComplition: operationComplitionBlock?
    
    private var writeComplition: operationComplitionBlock?
    
    public typealias updateCharComplitionBlock = ((Characterstic, BLEDevice) -> Void)
    private var updateCharComplition: updateCharComplitionBlock?
    
    //MARK:-  Functions
    init(peripheral: CBPeripheral, advertisementdata: [String: Any], Rssi: Int) {
        super.init()
        
        let exactRSSI = Rssi > -1 ? -100: Rssi < -100 ? -100: Rssi
        self.peripheral = peripheral
        AdvData = advertisementdata
        avgRSSI = exactRSSI
        arrOfRSSI = [exactRSSI]
        
        inRange = true
        count = 0
        uuid = peripheral.identifier.uuidString
        
        SetDeviceType()
        
        var beaconTypeName: String?
        if Eddystone != nil {
            if Eddystone!.isEddystoneEID() {
               beaconTypeName = "Eddystone (EID)"
            } else if Eddystone!.isEddystoneTLM() {
               beaconTypeName = "Eddystone (TLM)"
            } else if Eddystone!.isEddystoneUID() {
                beaconTypeName = "Eddystone (UID)"
            } else if Eddystone!.isEddystoneURL() {
                beaconTypeName = "Eddystone (URI)"
            }
        }
        
        name = peripheral.name
        localName = advertisementdata.peripheralname()
        self.beaconTypeName = beaconTypeName
        standardServiceName = advertisementdata.standardServiceName()
        
        
    }
    //set device type of beacon
    private func SetDeviceType() {
        
        if EddystoneManager.isEddystone(advertisementData: AdvData)  {
            
            deviceType = DeviceType.Eddystone
            Eddystone = EddystoneManager(advertisementData: AdvData)
            
        } else {
            
            deviceType = DeviceType.iBeacon
        }
    }
    //check is eddystone beacon or not
    public func isEddystone() -> Bool {
        return deviceType == .some(.Eddystone)
    }
    //check is Other beacon or not
    public func isiBeacon() -> Bool {
        return deviceType == .some(.iBeacon)
    }
    public func isConnected() -> Bool {
        return peripheral.state == .connected
    }
    public func retriveDevice(complition: @escaping ((BLEDevice) -> Void)) {
        
        blemanager.retrivePeripheral(device: self) { (d) in
            complition(d)
        }
    }
    public func connect(complition: @escaping ((Bool, BLEDevice, Error?) -> Void)) {
        
        blemanager.connect(peripheral: peripheral) { (isSuccess, peri, err)  in
            peri.delegate = self
            self.peripheral = peri
            peri.discoverServices(nil)
            DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 2, execute: {
                print("Complete Connected")
                complition(isSuccess, self, err)
            })
        }
    }
    func disConnect(myComplition: @escaping ((_ isDisconnected: Bool) -> Void)) {
        
        blemanager.disConnect(device: self.peripheral) { (isDisconnected) in
            myComplition(isDisconnected)
        }
    }
    public func writeValueIn(characteristicUUID: String, value: Data, complition: @escaping operationComplitionBlock) {
    
        writeComplition = complition
        if peripheral.Isconnected() {
            
            if let c = getCharsticFrom(UUID: characteristicUUID) {
                operationalCharacteristic = c
                
                peripheral.maximumWriteValueLength(for: .withoutResponse)
                
                peripheral.writeValue(value, for: c, type: .withoutResponse)
                
//                peripheral.writeValue(value, for: c, type: .withResponse)
                writeComplition?(true, "Command Fire Successfully", Characterstic(characterstic: c), self)
            } else {
                writeComplition?(false, "Characteristic Not Found", nil, self)
            }
        } else {
            writeComplition?(false, "Device is not Connected", nil, self)
        }
    }
    public func readValue(characteristicUUID: String, complition: @escaping operationComplitionBlock) {
        
        readComplition = complition
        
        if peripheral.Isconnected() {
            if let c = getCharsticFrom(UUID: characteristicUUID) {
                operationalCharacteristic = c
                peripheral.readValue(for: c)
            } else {
                readComplition?(false, "Characteristic Not Found", nil, self)
            }
        } else {
            writeComplition?(false, "Device is not Connected", nil, self)
        }
    }
    public func getUpdatedCharacteristic(_ updateCompletion: @escaping updateCharComplitionBlock) {
        self.updateCharComplition = updateCompletion
    }
    private func getCharsticFrom(UUID: String) -> CBCharacteristic? {
        
        var charStic: CBCharacteristic?
        for service in serivces {
            for char in service.characteristics {
                if char.uuid == UUID {
                    charStic = char.characteristic
                    break
                }
            }
        }
        return charStic
    }
}

public class Service: NSObject {
    
    public var name: String?
    public var uuid: String?
    public var characteristics = [Characterstic]()
    
    public init(service: CBService) {
        super.init()
        
        name = DictOfservices[service.uuid.uuidString]
        uuid = service.uuid.uuidString
    }
}

public class Characterstic: NSObject {
    
    public var characteristic: CBCharacteristic!
    public var name: String?
    public var uuid: String?
    public var stringValue: String?
    public var base64EncodedStringValue: String?
    public var hexValue: String?
    public var byteArray: [UInt8]?
    public var dataValue: Data?
    
    init(characterstic: CBCharacteristic) {
        super.init()
        
        characteristic = characterstic
        uuid = characterstic.uuid.uuidString
        stringValue = getStringValue()
        hexValue = getHexValue()
        base64EncodedStringValue = characteristic.value?.base64EncodedString()
        dataValue = characteristic.value
        
        if let newData = dataValue {
            byteArray = Array<UInt8>(newData)
        }
    }
    public func isNotifiable() -> Bool {
        return characteristic.properties.contains(.notify)
    }
    public func isReadable() -> Bool {
        return characteristic.properties.contains(.read)
    }
    public func isWriteable() -> Bool {
        return characteristic.properties.contains(.write)
    }
    private func getStringValue() -> String {
        
        var strvalue = ""
        if let value = characteristic.value {
            for character in value {
                strvalue = strvalue.appending("\(UnicodeScalar(character))")
            }
        }
        return strvalue
    }
    private func getHexValue() -> String {
        
        if let value = characteristic.value {
            return (value.hexEncodedString())
        }
        return ""
    }
}

//MARK:- 
extension BLEDevice: CBPeripheralDelegate {
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
        guard let serviceArray = peripheral.services else {
            return
        }
        for oneService in serviceArray{
            print("Service UUID: \(oneService.uuid)")
            print("Service UUID String: \(oneService.getUUIDString())")
            serivces.append(Service(service: oneService))
            peripheral.discoverCharacteristics(nil, for: oneService)
        }
    }
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        
        let ser = self.serivces.filter({$0.uuid == service.uuid.uuidString})
        for chr in service.characteristics! {
            
            print("chr UUID: \(chr.uuid)")
            print("chr UUID String: \(chr.getUUIDString())")
            
            if chr.properties.contains(.notify) {
                peripheral.setNotifyValue(true, for: chr)
            }
            ser[0].characteristics.append(Characterstic(characterstic: chr))
        }
        
    }
    public func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        
        if error == nil {
            
            if ((operationalCharacteristic != nil) && operationalCharacteristic.uuid == characteristic.uuid) {
                
                updateCharactersticInBLEDevice(charstic: characteristic)
                writeComplition?(true, "Command Fire Successfully", Characterstic(characterstic: characteristic), self)
            }
        }
    }
    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        
        if error == nil {
            
            updateCharactersticInBLEDevice(charstic: characteristic)
            if ((operationalCharacteristic != nil) && operationalCharacteristic.uuid == characteristic.uuid) {
                readComplition?(true, "Command Fire Successfully", Characterstic(characterstic: characteristic), self)
            }
            self.updateCharComplition?(Characterstic(characterstic: characteristic), self)
        }
    }
    func updateCharactersticInBLEDevice(charstic: CBCharacteristic) {
        
        let serviceIndex = self.serivces.firstIndex { (ser) -> Bool in
            return ser.uuid == charstic.service?.uuid.uuidString
        }
        if let index = serviceIndex {
            let charindex = self.serivces[index].characteristics.firstIndex { (char) -> Bool in
                return char.uuid == charstic.uuid.uuidString
            }
            if let i = charindex {
                self.serivces[index].characteristics[i] = Characterstic(characterstic: charstic)
            }
        }
    }
}



