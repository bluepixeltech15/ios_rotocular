//
//  AlbumManager.swift
//  AlbumManager
//
//  Created by Apple on 08/08/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Photos


class AlbumManager: NSObject {

    static let instance = AlbumManager()
    private var phLibrary = PHPhotoLibrary.shared()
    
    struct fileAsset {
        let localIdentifier: String
        let imgOrVideo: Any
    }
    
    override init() {
        super.init()
        
    }

    func checkAuthorization(complition: @escaping ((_ isAuthorized: Bool) -> Void)) {
        
        if PHPhotoLibrary.authorizationStatus() != .authorized {
            PHPhotoLibrary.requestAuthorization({ (status: PHAuthorizationStatus) -> Void in
                complition(status == .authorized)
            })
        } else {
            complition(true)
        }
        
    }
    /// Creates a folder with the specified name
    func createFolderWithName(_ name: String) {
        
        if fetchAssetCollectionFromAlbum(name: name) != nil {
            return
        }
        
        phLibrary.performChanges({
            PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: name)
        }) { success, error in
            if success {
                print("Crate folder successfully")
            } else {
                print("Crate folder error \(error?.localizedDescription ?? "")")
            }
        }
    }
    private func fetchAssetCollectionFromAlbum(name: String) -> PHAssetCollection? {

        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", name)
        let collection = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)

        if let firstObject = collection.firstObject {
            return firstObject
        }

        return nil
    }

    /// Saves the image to a new album with the specified name
    func saveImageIn(albumName: String, image: UIImage) {

        guard let assetCollection = fetchAssetCollectionFromAlbum(name: albumName) else {
            return   // If there was an error upstream, skip the save.
        }

        phLibrary.performChanges({
            
            let assetChangeRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
            let assetPlaceholder = assetChangeRequest.placeholderForCreatedAsset
            
            if let albumChangeRequest = PHAssetCollectionChangeRequest(for: assetCollection) {
                let enumeration: NSArray = [assetPlaceholder!]
                albumChangeRequest.addAssets(enumeration)
            }
            
        }, completionHandler: { (isSuccess, err) in
            if isSuccess {
                print("Image saved successfully in \(albumName)")
            } else {
                print("Image saved error : \(err?.localizedDescription ?? "") in Album:\(albumName)")
            }
        })
    }
    func moveImagesIn(albumName: String, imageURLs: [URL], completion: @escaping (() -> Void)) {
        
        func moveImagesIn(albumName: String, imageURLs: [URL]) {
            
            guard let assetCollection = fetchAssetCollectionFromAlbum(name: albumName) else {
                
                completion()
                return   // If there was an error upstream, skip the save.
            }
            
            phLibrary.performChanges({
                
                let assetChangeRequest = PHAssetChangeRequest.creationRequestForAssetFromImage(atFileURL: imageURLs.first!)
                let assetPlaceholder = assetChangeRequest?.placeholderForCreatedAsset
                
                if let albumChangeRequest = PHAssetCollectionChangeRequest(for: assetCollection) {
                    let enumeration: NSArray = [assetPlaceholder!]
                    albumChangeRequest.addAssets(enumeration)
                }
                
            }, completionHandler: { (isSuccess, err) in
                if isSuccess {
                    print("Image saved successfully in \(albumName)")
                } else {
                    print("Image saved error : \(err?.localizedDescription ?? "") in Album:\(albumName)")
                }
                
                if let removeFileURL = imageURLs.first {
                    storeManager.removeFileFrom(url: removeFileURL)
                }
                
                var images = imageURLs
                images.removeFirst()
                if images.count > 0 {
                    moveImagesIn(albumName: albumName, imageURLs: images)
                } else {
                    completion()
                }
            })
        }
        
        if imageURLs.count > 0 {
            moveImagesIn(albumName: albumName, imageURLs: imageURLs)
        } else {
            completion()
        }
    }
    func saveVideoIn(albumName: String, videoURL: URL) {
        
        guard let assetCollection = fetchAssetCollectionFromAlbum(name: albumName) else {
            return   // If there was an error upstream, skip the save.
        }
        
        phLibrary.performChanges({
            
            let assetChangeRequest = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: videoURL)
            let assetPlaceholder = assetChangeRequest?.placeholderForCreatedAsset
            
            if let albumChangeRequest = PHAssetCollectionChangeRequest(for: assetCollection) {
                let enumeration: NSArray = [assetPlaceholder!]
                albumChangeRequest.addAssets(enumeration)
            }
            
        }, completionHandler: { (isSuccess, err) in
            if isSuccess {
                print("Video saved successfully in \(albumName)")
            } else {
                print("Video saved error : \(err?.localizedDescription ?? "") in Album:\(albumName)")
            }
        })
    }
    
    func moveVideoIn(albumName: String, videoURLs: [URL], completion: @escaping (() -> Void)) {
        
        func moveVideoIn(albumName: String, videoURLs: [URL]) {
            
            guard let assetCollection = fetchAssetCollectionFromAlbum(name: albumName) else {
                completion()
                return   // If there was an error upstream, skip the save.
            }
            
            phLibrary.performChanges({
                
                let assetChangeRequest = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: videoURLs.first!)
                let assetPlaceholder = assetChangeRequest?.placeholderForCreatedAsset
                
                if let albumChangeRequest = PHAssetCollectionChangeRequest(for: assetCollection) {
                    let enumeration: NSArray = [assetPlaceholder!]
                    albumChangeRequest.addAssets(enumeration)
                }
                
            }, completionHandler: { (isSuccess, err) in
                
                if isSuccess {
                    print("Video saved successfully in \(albumName)")
                } else {
                    print("Video saved error : \(err?.localizedDescription ?? "") in Album:\(albumName)")
                }
                
                if let removeFileURL = videoURLs.first {
                    storeManager.removeFileFrom(url: removeFileURL)
                }
                var videos = videoURLs
                videos.removeFirst()
                if videos.count > 0 {
                    moveVideoIn(albumName: albumName, videoURLs: videos)
                } else {
                    completion()
                }
            })
        }
        
        if videoURLs.count > 0 {
            moveVideoIn(albumName: albumName, videoURLs: videoURLs)
        } else {
            completion()
        }
    }
    func getFilesFrom(album: String, fileType: CaptureType, completion: @escaping (([fileAsset]?) -> Void)) {
        
        var files = [fileAsset]()
        if fileType == .Photo {
            
            if let collection = fetchAssetCollectionFromAlbum(name: album) {
                
                let photoAssets = PHAsset.fetchAssets(in: collection, options: nil)
                let imageManager = PHCachingImageManager()
    
                if photoAssets.count > 0 {
                    photoAssets.enumerateObjects { (asset, count, stop) in
                        
                        let imgSize = CGSize(width: asset.pixelWidth, height: asset.pixelHeight)
                        let option = PHImageRequestOptions()
                        option.deliveryMode = .highQualityFormat
                        option.isSynchronous = true
                        
                        imageManager.requestImage(for: asset, targetSize: imgSize, contentMode: .aspectFill, options: option) { (image, info) in
                            
                            if let img = image {
                                files.append(fileAsset(localIdentifier: asset.localIdentifier, imgOrVideo: img))
                            }
                            if (photoAssets.count - 1) == count {
                                completion(files)
                            }
                        }
                    }
                } else {
                    completion(nil)
                }
            }
        } else {
            
            if let collection = fetchAssetCollectionFromAlbum(name: album) {
                
                let photoAssets = PHAsset.fetchAssets(in: collection, options: nil)
                let imageManager = PHCachingImageManager()
                
                if photoAssets.count > 0 {
                    photoAssets.enumerateObjects { (asset, count, stop) in
                        
                        let option = PHVideoRequestOptions()
                        option.deliveryMode = .highQualityFormat
                        
                        imageManager.requestAVAsset(forVideo: asset, options: option) { (videoAssest, videoMix, info) in
                            
                            if let video = videoAssest as? AVURLAsset {
                                files.append(fileAsset(localIdentifier: asset.localIdentifier, imgOrVideo: video.url))
                                if (photoAssets.count - 1) == count {
                                    completion(files)
                                }
                            }
                        }
                    }
                } else {
                    completion(nil)
                }
            }
        }
    }
    func removeFileFrom(identifier: [String], completion: @escaping ((_ isSuccess: Bool, _ message: String) -> Void)) {

        phLibrary.performChanges({
            
            let deleteAssets = PHAsset.fetchAssets(withLocalIdentifiers: identifier, options: nil)
            PHAssetChangeRequest.deleteAssets(deleteAssets)
            
        }) { (isSuccess, error) in
            
            if isSuccess {
                print("File delete successfully from Photos")
                completion(true, "File delete successfully from Photos")
            } else {
                print("File deleting error: \(error?.localizedDescription ?? "")")
                completion(false, error?.localizedDescription ?? "")
            }
        }
    }
}
