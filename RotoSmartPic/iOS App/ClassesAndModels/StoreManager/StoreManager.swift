//
//  storeManager.swift
//  CamTester
//
//  Created by Apple on 16/04/20.
//  Copyright © 2020 Gopika Rathod. All rights reserved.
//

import UIKit

class storeManager: NSObject {
    
    private static var fileManager = FileManager.default
    private static var documentDirectoryPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    
    class func makeFolder(directoryName: String, folderName: String, subfolder: String? = nil) {
        
        var filePath = documentDirectoryPath.appendingPathComponent(directoryName)
        filePath = filePath.appendingPathComponent(folderName)
        
        if let s = subfolder {
            filePath = filePath.appendingPathComponent(s)
        }

        if !fileManager.fileExists(atPath: filePath.path) {
            do {
                try fileManager.createDirectory(atPath: filePath.path, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print("FolderAlready Exist")
            }
        }
    }
    
    class func saveJpg(directoryName: String, folderName: String, subfolder: String? = nil, imageName: String, image: UIImage) {
        
        var filePath = documentDirectoryPath.appendingPathComponent(directoryName)
        filePath = filePath.appendingPathComponent(folderName)
        
        if let s = subfolder {
            filePath = filePath.appendingPathComponent(s)
        }

    
        // create the destination file url to save image
        let fileURL = filePath.appendingPathComponent(imageName + ".jpg")
        
        // get your UIImage jpeg data representation and check if the destination file url already exists
        if let data = image.jpegData(compressionQuality:  1.0), !fileManager.fileExists(atPath: fileURL.path) {
            
            do {
                // writes the image data to disk
                try data.write(to: fileURL)
                print("file saved")
            } catch {
                print("error saving file:", error)
            }
        }
    }
    class func getVideoStorePath(directoryName: String, folderName: String, subfolder: String? = nil, videoName: String) -> URL {
        
        var filePath = documentDirectoryPath.appendingPathComponent(directoryName)
        filePath = filePath.appendingPathComponent(folderName)
        
        if let s = subfolder {
            filePath = filePath.appendingPathComponent(s)
        }
        // create the destination file url to save image
        return filePath.appendingPathComponent(videoName + ".mp4")
    }
    class func moveVideoTo(directoryName: String, folderName: String, subfolder: String? = nil, oldURL: URL) {
        
        var filePath = documentDirectoryPath.appendingPathComponent(directoryName)
        filePath = filePath.appendingPathComponent(folderName)
        
        if let s = subfolder {
            filePath = filePath.appendingPathComponent(s)
        }
        
        do {
            
            let oldURLLastName = oldURL.lastPathComponent
            let newFileURL = filePath.appendingPathComponent(oldURLLastName)
        
            try fileManager.moveItem(at: oldURL, to: newFileURL)
            print("Video Move Successfully")
            
        } catch {
            print("error")
        }
    }

    class func getAllFiles(directoryName: String, folderName: String, subfolder: String? = nil) -> [URL]? {
        
        var filePath = documentDirectoryPath.appendingPathComponent(directoryName)
        filePath = filePath.appendingPathComponent(folderName)
        
        if let s = subfolder {
            filePath = filePath.appendingPathComponent(s)
        }
        do {
            let fileURLs = try fileManager.contentsOfDirectory(at: filePath, includingPropertiesForKeys: nil)
            return fileURLs
        } catch {
            print("Error while enumerating files \(filePath.path): \(error.localizedDescription)")
        }
        return nil
    }
    class func removeFileFrom(url: URL) {
        
        if fileManager.fileExists(atPath: url.path) {
            do {
                try fileManager.removeItem(at: url)
                print("file remove from url \(url)")
            } catch {
                print("Something went wrong")
            }
        } else {
            print("File not exist at url : \(url)")
        }
    }
}

