//
//  LogFile.swift
//  CamTester
//
//  Created by Apple on 16/04/20.
//  Copyright © 2020 Gopika Rathod. All rights reserved.
//

import UIKit

let logfile = LogFile()
class LogFile: NSObject {
    
    var logFileName = "Logs.txt"
    
    func create(fileName: String, text: String) {
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            logFileName = fileName
            
            let fileURL = dir.appendingPathComponent(fileName)
            
            if !(FileManager.default.fileExists(atPath: fileURL.path)){
                //writing
                do {
                    try text.write(to: fileURL, atomically: false, encoding: .utf8)
                }
                catch {/* error handling here */}
            }
//            else {
//                do {
//                    try FileManager.default.removeItem(at: fileURL)
//                }
//                catch {/* error handling here */}
//            }
        }
    }
    func appendNewText(text: String) {
        
        print(text)
        
        do {
            let dir: URL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last! as URL
            let url = dir.appendingPathComponent(logFileName)
            try "\n\(Date()) ==> \(text)\n".appendLineToURL(fileURL: url as URL)
            _ = try String(contentsOf: url as URL, encoding: String.Encoding.utf8)
        }
        catch {
            print("Could not write to file")
        }
    }
    func getLogFile() -> Any? {
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let logFileURL = dir.appendingPathComponent(logFileName)
            
            if FileManager.default.fileExists(atPath: logFileURL.path) {
                return logFileURL
            }
        }
        return nil
    }
}
extension String {
    func appendLineToURL(fileURL: URL) throws {
        try (self).appendToURL(fileURL: fileURL)
    }
    
    func appendToURL(fileURL: URL) throws {
        let data = self.data(using: String.Encoding.utf8)!
        try data.append(fileURL: fileURL)
    }
}
extension Data {
    
    func append(fileURL: URL) throws {
        if let fileHandle = FileHandle(forWritingAtPath: fileURL.path) {
            defer {
                fileHandle.closeFile()
            }
            fileHandle.seekToEndOfFile()
            fileHandle.write(self)
        }
        else {
            try write(to: fileURL, options: .atomic)
        }
    }
}
