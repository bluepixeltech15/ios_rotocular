//
//  Global.swift
//  RotoSmartPic
//
//  Created by Mac on 14/05/1942 Saka.
//  Copyright © 1942 Bluepixel Technologies. All rights reserved.
//

import UIKit

let userdef = UserDefaults.standard
let klaunchedBefore = "launchedBefore"

struct Command {
    
    //For RotoSmart Device
    public static let SERVICE_UUID  = "0000FFE0-0000-1000-8000-00805F9B34FB"
    public static let CHAR_UUID     = "FFE1"
    
    public static let defaultBytes: [UInt8] = [0x0D, 0x0A]
    
    public static let forword: UInt8        = 0x11
    public static let revese: UInt8         = 0x01
    public static let Custome: UInt8        = 0x03
    
    public static let defaultAngle: UInt8   = 0x00
    public static let Angle45p: UInt8       = 0x2D
    public static let Angle90p: UInt8       = 0x5A
    
    public static let stopForword: UInt8        = 0x10
    public static let stopRevese: UInt8         = 0x00
    
    //For LED Device
    public static let CHAR_LED_WRITE_UUID   = "FFE1"
    public static let CHAR_LED_READ_NOTIFY_UUID   = "FFE1"
    
    public static let LEDsOn: UInt8         = 0x0f
    public static let LEDsOff: UInt8        = 0x10
    public static let allLEDsValues: UInt8  = 0x02
    public static let responseSuccess: UInt8  = 0x01

    public static func getCommand(commandType: CommandType, _ bytes: [UInt8]) -> Data {
        
        var b = bytes
        b.insert(commandType.rawValue, at: 0)
        
        switch commandType {
            case .readdatafromuC:
                break
                
            case .customAngel:
                
                b.insert(UInt8(bytes.count / 2), at: 1) //bytes.count / 2 cause here we need to pass angles [Int] array count and here bytes.count is every angle are converted into two bytes so.
                b.append(getCheckSum(bytes: b))
                b += defaultBytes
                
                break
            case .SchedulingAngel:
                b = bytes
//                b.insert(UInt8(bytes.count / 2), at: 1) //bytes.count / 2 cause here we need to pass angles [Int] array count and here bytes.count is every angle are converted into two bytes so.
                b.insert(CommandType.SchedulingAngels.rawValue, at: 0)
                
                b.append(getCheckSum(bytes: b))
                b += defaultBytes

//                b.insert(CommandType.start.rawValue, at: 0)
//                b.append(getCheckSum(bytes: b))
//                b += defaultBytes
               
                break
            case .cmdVidoeAngle45P, .cmdVidoeAngle90P:
                
                let anlge = commandType == .cmdVidoeAngle45P ? Angle45p:Angle90p
                
                b.append(anlge)
                b.append(defaultAngle)
                b.append(getCheckSum(bytes: b))
                b += defaultBytes
                
                break
                
            default:
                
                b.append(getCheckSum(bytes: b))
                b += defaultBytes
                break
        }
        return Data(b)
    }
    public static func getCustomCommand(bytes: [UInt8]) -> Data {
        
        var b = bytes
        b.insert(Custome, at: 1)
        b.append(getCheckSum(bytes: b))
        b += defaultBytes
        return Data(b)
    }
    private static func getCheckSum(bytes: [UInt8]) -> UInt8 {
    
        let checkSum = bytes.map{Int($0)}.reduce(0, +) & 0xff
        return UInt8(256 - checkSum)
    }
}

struct Constant {
    
    public static let kIsCompletedIntroduction = "IsCompletedIntroduction"
    public static let kRotoPic = "RotoPic"
    
    public static let kPhoto_Single = "Photo_Single"
    public static let kPhoto_45     = "Photo_45"
    public static let kPhoto_90     = "Photo_90"
    public static let kPhoto_180    = "Photo_180"
    public static let kPhoto_360    = "Photo_360"
    
    public static let kVideo_45     = "Video_45"
    public static let kVideo_90     = "Video_90"
    public static let kVideo_180    = "Video_180"
    public static let kVideo_360    = "Video_360"
    
    public static let kgroupText    = "groupText"
    public static let kgroupValue   = "groupValue"
    
    public static let kgroupindex   = "groupindex"
    public static let kgroupName    = "groupName"
    public static let kgroupValues  = "groupValues"
    public static let kindividualValues  = "idividualValues"

}



struct USERDEFAULT {
    
    public static let kDelay           = "Delay"
    public static let kSpeed           = "Speed"
    public static let kCustomAngles    = "CustomAngles"
    public static let kVideoDelay      = "VideoDelay"
    public static let kContinueDelay   = "ContinueDelay"
    
    //For Settings
    public static let kMicroStepping    = "MicroStepping"
    public static let kGearRatio        = "GearRatio"
    
    public static let kCameraSettingMaxResolution   = "CameraSettingMaxResolution"
    public static let kCameraSettingEnablePro       = "CameraSettingEnablePro"
    public static let kCameraSettingSaveToSDCard    = "CameraSettingSaveToSDCard"
    
    public static let kGallerySettingMaxResolution  = "GallerySettingMaxResolution"
    public static let kGallerySettingEnablePro      = "GallerySettingEnablePro"
    public static let kGallerySettingSaveToSDCard   = "GallerySettingSaveToSDCard"
    
    public static let kAdjust1SettingBrightness     = "Adjust1SettingBrightness"
    public static let kAdjust1SettingContrass       = "Adjust1SettingContrass"
    public static let kAdjust1SettingPurity         = "Adjust1SettingPurity"
    
    public static let kAdjust2SettingBrightness     = "Adjust2SettingBrightness"
    public static let kAdjust2SettingContrass       = "Adjust2SettingContrass"
    public static let kAdjust2SettingPurity         = "Adjust2SettingPurity "
    
    public static let kAdjust3SettingBrightness     = "Adjust3SettingBrightness"
    public static let kAdjust3SettingContrass       = "Adjust3SettingContrass"
    public static let kAdjust3SettingPurity         = "Adjust3SettingPurity"
    
    public static let kAdjustCustomSettingBrightness     = "AdjustCustomSettingBrightness"
    public static let kAdjustCustomSettingContrass       = "AdjustCustomSettingContrass"
    public static let kAdjustCustomSettingPurity         = "AdjustCustomSettingPurity"
   
    
    //For LEDControll Setting
    public static let kGroups               = "Groups"
    public static let kIndividualSwitch     = "IndividualSwitch"
    
    public static let klastselectedGroupIndex   = "lastselectedGroupIndex"
    
    public static let kSelectedProfile   = "selectedProfile"
    
    public static let kScheduleProfile = "ScheduleProfile"
}
