//
//  Enums.swift
//  RotoSmartPic
//
//  Created by BAPS on 12/11/20.
//  Copyright © 2020 Bluepixel Technologies. All rights reserved.
//

import Foundation
import UIKit

enum CaptureType: String {
    case Photo = "Photo"
    case Video = "Video"
}

enum VideoFPS: Int {
    case Thirty = 30
    case Sixty = 60
}

enum AngleTypes: String {
    
    case P45 = "45P"
    case P90 = "90P"
    case P180 = "180"
    case P360 = "360"
    case Single = "Single"
    case c1 = "1c"
    case c0 = "0w"
    
//    case p45 = "45"
//    case p90 = "90"
    
    func getIntAngle() -> Int {
        
        var angle = 0
        
        if self == .P45 || self == .P90 || self == .c1 || self == .c0 {
            
            var a = rawValue
            a.removeLast() // Remove P from 45P and 90P angle
            angle = Int(a)!
            
        } else if self != .Single {
            angle = Int(rawValue)!
        }
        return angle
    }
}

public enum Mode: UInt8 {
    
    case EachingMode        = 0x01
    case ManualMode         = 0x02
    case SingleDelayMode    = 0x03
    case ContinuosDelayMode = 0x04
    case SchedulingMode     = 0x05
    case CustomMode         = 0x86
    case None               = 0x00
}

enum CommandType: UInt8 {
    
    case MicroStepping      =  0x24
    case speed              =  0x25
    case direction          =  0x26
    case delay              =  0x27
    case angle              =  0x28
    case noofstop           =  0x29
    case start              =  0x11
    case stop               =  0x12
    case readdatafromuC     =  0x40
    case gearratio          =  0x31
    case scanningtype       =  0x80
    case delayincontmode    =  0x81
    case customAngel        =  0x85
    case SchedulingAngel    =  0x05
    case SchedulingAngels   =  0x82
    
    case cmdVidoeAngle45P   =  0x51
    case cmdVidoeAngle90P   =  0x52
}

enum GridType: String {
    
    case None
    case Grid2x2
    case Grid3x3
    case CircleGrid
}

enum cameraMode: Int {
    
    case Eaching = 0, Manual, Home, Continue, None
}

enum PlayType: Int {
    
    case Start = 0, Stop, Preview, Save, None
}

enum GallaryOpenType: String {
    
    case Preview
    case Gallery
    case Selection
}

enum ObserveType {
    
    case first
    case second
    case last
    case None
}

struct image {
    
    var img: UIImage!
    var isSelected: Bool!
    let url: URL!
    let folder: CaptureType!
    let subfolder: AngleTypes!
    var photoidentifier: String?
}

public struct LEDIndexAndValue {
    let index: Int
    let value: Int
}
