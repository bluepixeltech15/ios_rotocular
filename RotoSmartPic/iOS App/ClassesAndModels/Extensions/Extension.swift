//
//  Extension.swift
//  Prev Spiro
//
//  Created by Nikhil Jobanputra on 01/07/20.
//  Copyright © 2020 Nikhil Jobanputra. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import AVKit
import AVFoundation
import Photos

let kAppname = "RotoSmartPic"

extension String {

    func trime() -> String {
        
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    mutating func toData() -> Data {
        return Data(self.utf8)
    }
    mutating func toBytes() -> [UInt8] {
        return [UInt8](toData())
    }
}


//MARK:-  Extension UIViewController
extension UIViewController {
    
    func isExistUserDefaultKey(_ key : String) -> Bool{
        
        if  (userdef.value(forKey: key) != nil){
            
            return true;
        }
        return false;
    }
    
    func showHUD(_ text: String? = nil, dim: Bool = false) {
        
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        hud.isUserInteractionEnabled = false
        hud.animationType = MBProgressHUDAnimation.zoomIn
        UIApplication.shared.beginIgnoringInteractionEvents()
        hud.alpha = 0.9
        if let text = text {
            hud.label.text = text
        }
    }
    func hideHUD() {
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: self.view, animated: true)
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
    
    func showAlertWithOKButton(message: String) {
        let alert = UIAlertController(title: kAppname, message: message, preferredStyle: .alert)
        
        let okaction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(okaction)
        
        present(VC: alert)
    }
    func showAlertWithOKButton(message: String, complition: @escaping (() -> Void)) {
        
        let alert = UIAlertController(title: kAppname, message: message, preferredStyle: .alert)
        
        let okaction = UIAlertAction(title: "OK", style: .default) { (okAction) in
            complition()
        }
        
        alert.addAction(okaction)
        present(VC: alert)
    }
    func showAlertWith2Buttons(message:String, btnOneName:String, btnTwoName:String, complition:@escaping  (_ btnAction:Int) -> Void ) {
        
        let alert = UIAlertController(title: kAppname, message: message, preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: btnOneName, style: .default) { (action) in
            complition(1)
        }
        alert.addAction(action1)
        
        let action2 = UIAlertAction(title: btnTwoName, style: .default) { (UIAlertActionaction2) in
            complition(2)
        }
        alert.addAction(action2)
        present(VC: alert)
    }
    func present(VC: UIViewController) {
        
        if let presentedVC = self.navigationController?.presentedViewController {
            presentedVC.present(VC, animated: false, completion: nil)
        } else {
            self.present(VC, animated: false, completion: nil)
        }
    }
    
    func getBuildVersion() -> String? {
        
        return Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
    }
    func getCurrentDateWithMilisecond() -> String {
        
        let df = DateFormatter()
        df.dateFormat = "dd_MMM_yyyy@HH_mm_ss.SSSS"
        return df.string(from: Date())
    }
    func showAlertWithButton(message: String, btn1name: String, complition: @escaping (Int) -> Void) {
        
        let alert = UIAlertController(title: kAppname, message: message, preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: btn1name, style: .default) { (one) in
            complition(1)
        }
        alert.addAction(action1)
        self.present(alert, animated: true, completion: nil)
    }
    func writeMicroStepping(value: Int, completion: @escaping (() -> Void)) {
        
        connectedBLEDevice?.writeValueIn(characteristicUUID: Command.CHAR_UUID, value: Command.getCommand(commandType: .MicroStepping, value.intToByteArray(length: 2)), complition: { (isSuccess, message, char, device) in
            
            if isSuccess {
                connectedBLEDevice = device
                print("Microsteping Write Successfull")
                completion()
            } else {
                print("Microsteping Write failed with error: \(message)")
                completion()
            }
            
        })
    }
    func writeSpeed(value: Int, completion: @escaping (() -> Void)) {
        
        let dataValue = Command.getCommand(commandType: .speed, value.intToByteArray(length: 2))
        print("Mode = \(CommandType.speed) , dataValue = \(dataValue.hexEncodedString())")
        
        connectedBLEDevice?.writeValueIn(characteristicUUID: Command.CHAR_UUID, value: dataValue , complition: { (isSuccess, message, char, device) in
            
            if isSuccess {
                connectedBLEDevice = device
                print("Speed Write Successfull")
                completion()
            } else {
                print("Speed Write failed with error: \(message)")
                completion()
            }
        })
    }
    func writeDirection(value: Int, completion: @escaping (() -> Void)) {
        
        connectedBLEDevice?.writeValueIn(characteristicUUID: Command.CHAR_UUID, value: Command.getCommand(commandType: .direction, value.intToByteArray(length: 2)), complition: { (isSuccess, message, char, device) in
            
            if isSuccess {
                connectedBLEDevice = device
                print("Direction Write Successfull")
                completion()
            } else {
                print("Direction Write failed with error: \(message)")
                completion()
            }
        })
    }
    func writeDelay(value: Int , commandType : CommandType = .delay , completion: @escaping (() -> Void)) {
        
        connectedBLEDevice?.writeValueIn(characteristicUUID: Command.CHAR_UUID, value: Command.getCommand(commandType: commandType, value.intToByteArray(length: 2)), complition: { (isSuccess, message, char, device) in
            
            if isSuccess {
                connectedBLEDevice = device
                print("Delay Write Successfull")
                completion()
            } else {
                print("Delay Write failed with error: \(message)")
                completion()
            }
        })
    }
    
    func writeAngel(value: Int , commandtype : CommandType = .angle, valuearr : [UInt8] = [], completion: @escaping (() -> Void)) {
        
        if isschedulestart{
            if commandtype != .SchedulingAngel{
                return
            }
        }
        
        
//        var setdata = [UInt8]()
//        for item in valuearr{
//            setdata.append(contentsOf: item.intToByteArray(length: 2))
//        }
                
        //var setdata =  valuearr.map{$0.intToByteArray(length: 2)}
        //(commandtype == .SchedulingAngel ? setdata : value.intToByteArray(length: 2))
        let dataValue = Command.getCommand(commandType: commandtype, commandtype == .SchedulingAngel ? valuearr : value.intToByteArray(length: 2))
        print("Mode = \(commandtype) , dataValue = \(dataValue.hexEncodedString())")

//        connectedBLEDevice?.
        
        connectedBLEDevice?.writeValueIn(characteristicUUID: Command.CHAR_UUID, value: dataValue, complition: { (isSuccess, message, char, device) in
            
            if isSuccess {
                connectedBLEDevice = device
                print("Angel Write Successfull")
                completion()
            } else {
                print("Angel Write failed with error: \(message)")
                completion()
            }
        })
    }
    
    func writeAngelSchedule(value: Int , commandtype : CommandType = .angle, valuearr : [UInt8] = [], completion: @escaping (() -> Void)) {
//                var setdata = [UInt8]()
//                for item in valuearr{
//                    setdata.append(contentsOf: item.intToByteArray(length: 2))
//                }
//
//        var setdata =  valuearr.map{$0.intToByteArray(length: 2)}
//        (commandtype == .SchedulingAngel ? setdata : value.intToByteArray(length: 2))
        if isschedulestart{
            if commandtype != .SchedulingAngel{
                return
            }
        }
        
        let dataValue = Command.getCommand(commandType: commandtype, commandtype == .SchedulingAngel ? valuearr : value.intToByteArray(length: 2))
        print("Mode = \(commandtype) , dataValue = \(dataValue.hexEncodedString())")
        
        //        connectedBLEDevice?.
        
        connectedBLEDevice?.writeValueIn(characteristicUUID: Command.CHAR_UUID, value: dataValue, complition: { (isSuccess, message, char, device) in
            
            if isSuccess {
                connectedBLEDevice = device
                print("Angel Write Successfull")
                completion()
            } else {
                print("Angel Write failed with error: \(message)")
                completion()
            }
        })
    }
    
    public func writeStart(mode: Mode, isForword: Bool, completion: @escaping (() -> Void)) {
        
        var dataValue = Data()
        let b1 = mode.rawValue
        let b2 = isForword ? Command.forword:Command.revese
    
        if mode == .CustomMode {
            dataValue = Command.getCustomCommand(bytes: [b1, b2])
        } else {
            dataValue = Command.getCommand(commandType: .start, [b1, b2])
        }
       
        print("Mode = \(mode) , dataValue = \(dataValue.hexEncodedString())")
        connectedBLEDevice?.writeValueIn(characteristicUUID: Command.CHAR_UUID, value: dataValue, complition: { (isSuccess, message, char, device) in
            
            if isSuccess {
                connectedBLEDevice = device
                print("Write Start Successfull")
                completion()
            } else {
                print("Write Start failed with error: \(message)")
                completion()
            }
        })
    }
    
    func writeStop(mode: Mode, isForword: Bool, completion: @escaping (() -> Void)) {
        
        var dataValue = Data()
        let b1 = mode.rawValue
        let b2 = isForword ? Command.stopForword:Command.stopRevese
        
        if mode == .CustomMode {
            dataValue = Command.getCustomCommand(bytes: [b1, b2])
        } else {
            dataValue = Command.getCommand(commandType: .stop, [b1, b2])
        }
        
        connectedBLEDevice?.writeValueIn(characteristicUUID: Command.CHAR_UUID, value: dataValue, complition: { (isSuccess, message, char, device) in
            
            if isSuccess {
                
                connectedBLEDevice = device
                print("Stop Write Successfull")
                completion()
            } else {
                print("Stop Write failed with error: \(message)")
                completion()
            }
        })
    }
    func writeNoOfStop(value: Int, completion: @escaping (() -> Void)) {
        
        connectedBLEDevice?.writeValueIn(characteristicUUID: Command.CHAR_UUID, value: Command.getCommand(commandType: .noofstop, value.intToByteArray(length: 2)), complition: { (isSuccess, message, char, device) in
            
            if isSuccess {
                connectedBLEDevice = device
                print("NoOfStop Write Successfull")
                completion()
            } else {
                print("NoOfStop Write failed with error: \(message)")
                completion()
            }
        })
    }
    func writeGearRatio(value: Int, completion: @escaping (() -> Void)) {
        
        connectedBLEDevice?.writeValueIn(characteristicUUID: Command.CHAR_UUID, value: Command.getCommand(commandType: .gearratio, value.intToByteArray(length: 2)), complition: { (isSuccess, message, char, device) in
            
            if isSuccess {
                connectedBLEDevice = device
                print("GearRatio Write Successfull")
                completion()
            } else {
                print("GearRatio Write failed with error: \(message)")
                completion()
            }
        })
    }
    func writeScanningType(value: Int, completion: @escaping (() -> Void)) {
        
        connectedBLEDevice?.writeValueIn(characteristicUUID: Command.CHAR_UUID, value: Command.getCommand(commandType: .scanningtype, value.intToByteArray(length: 2)), complition: { (isSuccess, message, char, device) in
            
            if isSuccess {
                connectedBLEDevice = device
                print("ScanningType Write Successfull")
                completion()
            } else {
                print("ScanningType Write failed with error: \(message)")
                completion()
            }
        })
    }
    func writeCustomAngel(values: [Int], completion: @escaping (() -> Void)) {
        
        var bytes = [UInt8]()
        for intval in values {
            bytes += intval.intToByteArray(length: 2)
        }
        
        connectedBLEDevice?.writeValueIn(characteristicUUID: Command.CHAR_UUID, value: Command.getCommand(commandType: .customAngel, bytes), complition: { (isSuccess, message, char, device) in

            if isSuccess {
                connectedBLEDevice = device
                print("customAngel Write Successfull")
                completion()
            } else {
                print("customAngel Write failed with error: \(message)")
                completion()
            }
        })
    }
    func writeStartVideoP(angle: AngleTypes, completion: @escaping (() -> Void)) {
        
        let commnadType = angle == .P45 ? CommandType.cmdVidoeAngle45P:CommandType.cmdVidoeAngle90P
        
        connectedBLEDevice?.writeValueIn(characteristicUUID: Command.CHAR_UUID, value: Command.getCommand(commandType: commnadType, [UInt8]()), complition: { (isSuccess, message, char, device) in

            if isSuccess {
                connectedBLEDevice = device
                print("customAngel Write Successfull")
                completion()
            } else {
                print("customAngel Write failed with error: \(message)")
                completion()
            }
        })
    }
    func makedelay() {
        
//        sleep(UInt32(0.2))
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {

        }
    }
    func playVideo(url: URL) {
        
        let player = AVPlayer(url: url)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    func getFolderNameOfPhotos(folder: CaptureType, subFolder: AngleTypes) -> String {
        
        if folder == .Photo {
            
            switch subFolder {
            case .Single:
                return Constant.kPhoto_Single
                
            case .P45:
                return Constant.kPhoto_45
                
            case .P90:
                return Constant.kPhoto_90
                
            case .P180:
                return Constant.kPhoto_180
                
            case .P360:
                return Constant.kPhoto_360
                
            default:
                break
            }
            
        } else {
            
            switch subFolder {
            case .P45:
                return Constant.kVideo_45
                
            case .P90:
                return Constant.kVideo_90
                
            case .P180:
                return Constant.kVideo_180
                
            case .P360:
                return Constant.kVideo_360
                
            default:
                break
            }
        }
        return ""
    }
    
    func share(items: [Any]) {
        
        let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        activityViewController.completionWithItemsHandler = {(activityType: UIActivity.ActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
            if !completed {
                // User canceled
                return
            }
        }
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func showToast(message: String) {
        
        DispatchQueue.main.async {
            
            
            let fakeLabel = UILabel()
            fakeLabel.text = "      \(message)      "
            fakeLabel.sizeToFit()
            
            let toastLabel = UILabel(frame: CGRect(x: 0, y: 0, width: fakeLabel.frame.width + 16, height: 60))
            toastLabel.backgroundColor = UIColor(red: 237.0/255.0, green: 237.0/255.0, blue: 237.0/255.0, alpha: 1.0)
            toastLabel.text = "      \(message)      "
            toastLabel.center.y = (self.view.frame.size.height * 85) / 100
            toastLabel.center.x = self.view.center.x
           
            toastLabel.layer.cornerRadius = toastLabel.frame.height / 2
            toastLabel.clipsToBounds = true
    
            toastLabel.tag = 111111
            
            self.view.addSubview(toastLabel)
            
            DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 2) {
                DispatchQueue.main.async {
                    toastLabel.removeFromSuperview()
                }
            }
        }
    }
}

extension Int {
    func toMS() -> (minute: Int, second: Int) {
        return ((self / 60), (self % 60))
    }
    func twoDigit() -> String {
        return String(format: "%02d", self)
    }
    mutating func toData() -> Data {
        return Data(bytes: &self, count: MemoryLayout.size(ofValue: self))
    }
    
    func intToByteArray(length: Int = 0) -> [UInt8] {
        
        var number = self
        let myIntData = Data(bytes: &number,
                             count: MemoryLayout.size(ofValue: 2))
        var bytes: [UInt8] = []
        var detectedInt = false
        for i in myIntData.indices {
            let byte = myIntData[myIntData.count-i-1]
            if byte == 0 && !detectedInt {
                continue
            }
            detectedInt = true
            bytes.append(byte)
        }
        if length > 0 && length != bytes.count {
            for _ in 1...length-bytes.count {
                bytes.insert(UInt8(0), at: 0)
            }
        }
        return bytes
    }
}

extension Array where Element == UInt8  {
    
    func toData() -> Data {
        return Data(self)
    }
}

extension UInt8  {
    var toInt: Int {
        return Int(self)
    }
}

enum AttachmentType: String {
    
    case camera, photoLibrary
    case video
}

let kSelectPhoto = "Select photo from gallery"
let kCapturePhoto = "Capture photo from camera"

extension UIViewController {
    
    func showImageOption() {
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        let messageFont = [kCTFontAttributeName: UIFont(name: "Avenir-Roman", size: 18.0)!]
        let messageAttrString = NSMutableAttributedString(string: "Select photo", attributes: messageFont as [NSAttributedString.Key: Any])

        alert.setValue(messageAttrString, forKey: "attributedMessage")

        let gallery = UIAlertAction(title: kSelectPhoto, style: .default) { (action) in

            self.authorisationStatus(attachmentTypeEnum: .photoLibrary) { access in
                if access {

                    if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {

                        DispatchQueue.main.async {
                            imagePicker.sourceType = .photoLibrary
                            imagePicker.allowsEditing = false
                            self.present(imagePicker, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
        alert.addAction(gallery)
        
        let camera = UIAlertAction(title: kCapturePhoto , style: .default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.authorisationStatus(attachmentTypeEnum: .camera) { access in
                    
                    if access {
                        DispatchQueue.main.async {
                            imagePicker.sourceType = .camera
                            imagePicker.cameraCaptureMode = .photo
                            imagePicker.allowsEditing = false
                            self.present(imagePicker, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
        alert.addAction(camera)
        
        let cancel = UIAlertAction(title: "Cancel" , style: .cancel, handler: nil)
        alert.addAction(cancel)

        if let popoverController = alert.popoverPresentationController {

            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.maxY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func askForPermision(forType: AttachmentType, complition: @escaping (_ success: Bool) -> Void) {
        switch forType {
            case .camera, .video:
                AVCaptureDevice.requestAccess(for: .video, completionHandler: { granted in
                    if granted {
                        complition(true)
                        
                    } else {
                        complition(false)
                        print("restriced manually")
                    }
                })
                
            case .photoLibrary:
                PHPhotoLibrary.requestAuthorization({ (status) in
                    if status == PHAuthorizationStatus.authorized{
                        // photo library access given
                        print("access given")
                        complition(true)
                        
                    }else{
                        print("restriced manually")
                        complition(false)
                    }
                })
        }
    }
    
    func authorisationStatus(attachmentTypeEnum: AttachmentType, complition: @escaping (_ success: Bool) -> Void) {
        switch attachmentTypeEnum {
            case .camera, .video:
                let authStatus = AVCaptureDevice.authorizationStatus(for: .video)
                switch authStatus {
                    case .authorized:
                        complition(true)
                        
                    case .notDetermined:
                        print("Permission Not Determined")
                        askForPermision(forType: attachmentTypeEnum) { authorized in
                            if authorized {
                                complition(true)
                            } else {
                                self.addAlertForSettings(attachmentTypeEnum)
                                complition(false)
                            }
                        }
                        
                    case .denied:
                        print("permission denied")
                        self.addAlertForSettings(attachmentTypeEnum)
                        complition(false)
                        
                    case .restricted:
                        print("permission restricted")
                        self.addAlertForSettings(attachmentTypeEnum)
                        complition(false)
                    default:
                        break
                }
                
            case .photoLibrary:
                let status = PHPhotoLibrary.authorizationStatus()
                switch status {
                    case .authorized:
                        complition(true)
                    
                    case .notDetermined:
                        print("Permission Not Determined")
                        self.askForPermision(forType: attachmentTypeEnum) { authorized in
                            if authorized {
                                complition(true)
                            } else {
                                complition(false)
                            }
                        }
                        complition(false)
                        
                    case .denied:
                        print("permission denied")
                        self.addAlertForSettings(attachmentTypeEnum)
                        complition(false)
                        
                    case .restricted:
                        print("permission restricted")
                        self.addAlertForSettings(attachmentTypeEnum)
                        complition(false)
                    default:
                        complition(false)
                }
        }
    }
    
    func addAlertForSettings(_ attachmentTypeEnum: AttachmentType){
        
        let alertForPhotoLibraryMessage = "App does not have access to your photos. To enable access, tap settings and turn on Photo Library Access."
        
        let alertForCameraAccessMessage = "App does not have access to your camera. To enable access, tap settings and turn on Camera."
        
        let alertForVideoLibraryMessage = "App does not have access to your video. To enable access, tap settings and turn on Video Library Access."
        
        let settingsBtnTitle = "Settings"
        let cancelBtnTitle = "Cancel"
        
        var alertTitle: String = ""
        
        if attachmentTypeEnum == AttachmentType.camera{
            alertTitle = alertForCameraAccessMessage
        }
        if attachmentTypeEnum == AttachmentType.photoLibrary{
            alertTitle = alertForPhotoLibraryMessage
        }
        if attachmentTypeEnum == AttachmentType.video{
            alertTitle = alertForVideoLibraryMessage
        }
        
        let cameraUnavailableAlertController = UIAlertController (title: alertTitle , message: nil, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: settingsBtnTitle, style: .destructive) { (_) -> Void in
            let settingsUrl = NSURL(string:UIApplication.openSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            }
        }
        let cancelAction = UIAlertAction(title: cancelBtnTitle, style: .default, handler: nil)
        //cameraUnavailableAlertController .addAction(cancelAction)
        cameraUnavailableAlertController .addAction(settingsAction)
        DispatchQueue.main.async {
            self.present(cameraUnavailableAlertController , animated: true, completion: nil)
        }
    }
}

extension AVCaptureDevice {
    func set(frameRate: Double) {
        guard let range = activeFormat.videoSupportedFrameRateRanges.first,
              range.minFrameRate...range.maxFrameRate ~= frameRate
        else {
            print("Requested FPS is not supported by the device's activeFormat !")
            return
        }
        
        do { try lockForConfiguration()
            activeVideoMinFrameDuration = CMTimeMake(value: 1, timescale: Int32(frameRate))
            activeVideoMaxFrameDuration = CMTimeMake(value: 1, timescale: Int32(frameRate))
            unlockForConfiguration()
            
        } catch {
            print("LockForConfiguration failed with error: \(error.localizedDescription)")
        }
    }
}
